﻿--Insert data into Category
--Phụ tùng thay thế cha
insert into Category
values(N'Phụ tùng thay thế',N'img/category/cate1.png',0,1);
--Lốp xe máy cha
insert into Category
values(N'Lốp xe máy',N'img/category/cate2.png',0,1);
--Nhớt xe máy cha
insert into Category
values(N'Nhớt xe máy',N'img/category/cate3.png',0,1);


--Phụ tùng thay thế con
insert into Category
values(N'Nhông sên đĩa',null,1,1);
insert into Category
values(N'Má phanh',null,1,1);
insert into Category
values(N'Phuộc xe máy',null,1,1);
insert into Category
values(N'Pô xe máy',null,1,1);
insert into Category
values(N'Mâm xe máy',null,1,1);
insert into Category
values(N'Bộ nồi xe máy',null,1,1);
insert into Category
values(N'Bugi + IC xe máy',null,1,1);
insert into Category
values(N'Bình ắc quy xe máy',null,1,1);

--Lốp xe máy con
insert into Category
values(N'Lốp xe Michelin',null,2,1);
insert into Category
values(N'Lốp xe Dunlop',null,2,1);
insert into Category
values(N'Lốp xe Maxxis',null,2,1);
insert into Category
values(N'Lốp xe Pirelli',null,2,1);
insert into Category
values(N'Lốp xe Metzeler',null,2,1);
insert into Category
values(N'Lốp xe Aspira',null,2,1);
insert into Category
values(N'Lốp xe Camel',null,2,1);
insert into Category
values(N'Lốp xe Continental',null,2,1);
--Nhớt xe máy con
insert into Category
values(N'Motul',null,3,1);
insert into Category
values(N'Repsol',null,3,1);
insert into Category
values(N'Shell Advance',null,3,1);
insert into Category
values(N'Mobil 1',null,3,1);
insert into Category
values(N'Liqui Moly',null,3,1);
insert into Category
values(N'Amsoil',null,3,1);
insert into Category
values(N'Maxima',null,3,1);
insert into Category
values(N'Gulf Western',null,3,1);


--Addition Category--
insert into Category
values(N'Heo dầu xe máy',null,1,1)
insert into Category
values(N'Lọc nhớt xe máy',null,1,1)
insert into Category
values(N'Khóa xe máy',null,1,1)
insert into Category
values(N'Lọc gió xe máy',null,1,1)
insert into Category
values(N'Phụ tùng khác',null,1,1)
insert into Category
values(N'Lốp xe IRC',null,2,1);
insert into Category
values(N'Lốp xe Champion',null,2,1);
insert into Category
values(N'Lốp xe Timsun',null,2,1);
insert into Category
values(N'Lốp xe chống dính',null,2,1);
insert into Category
values(N'Wolver',null,3,1);
insert into Category
values(N'Fuchs',null,3,1);
insert into Category
values(N'Ipone',null,3,1);
insert into Category
values(N'Bảo dưỡng xe máy',null,3,1);

--Brand
  insert into brand values('Recto')
  insert into brand values('MotoBatt')
  insert into brand values('Michelin')
  insert into brand values('Aspira Stretto')
  insert into brand values('Nitron')

--category-1
  insert into Product
  values (4, 2, N'Nhông Recto 14T chính hãng cho Raider', 65000, 0.1, N'Nhông Recto 14T chính hãng dành cho Raider với chất liệu hộp kim siêu bền, vận hành ổn định sẽ giúp cho xe các bạn luôn hoạt động tốt nhất. Nhông Recto 14T gắn cho xe Raider, Satria...', 'img/product/product1.png', 80, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (5, 1, N'Bố thắng đĩa sau Elig Satria, Raider', 100000, 0.12, N'Bố thắng đĩa sau Elig cho Raider, Satria luôn an toàn với công nghệ EFT làm cho má phanh tăng sức mạnh độ bám khi sử dụng lực thắng ít hơn so với sản phẩm thông thường. Bố thắng Elig được hoàn thiện đẹp và thẩm mĩ, có độ mài mòn thấp làm tăng tuổi thọ sản phẩm gấp 2 lần sản phẩm thông thường.', 'img/product/product2.png', 70, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (6, 3, N'Phuộc Nitron chính hãng Việt Nam cho Exciter 135', 2500000, 0.1, N'Phuộc Nitron chính hãng Việt Nam, thương hiệu phuộc mới tại Việt Nam đang rất được ưa chuộng trên thị trường đồ chơi xe máy với ưu điểm thiết kế đẹp mắt,  tỉ mỉ từng chi tiết đồng thời độ nhún rất mượt mà, êm ái và đặc biệt giá cả vô cùng hợp lí.', 'img/product/product3.png', 60, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (7, 2, N'Pô 4road 2008 chính hãng cho SH350i', 13750000, 0.41, N'Pô 4road 2008 chính hãng cho SH350i gồm fullset đầy đủ phụ kiện: cổ, pát, lon pô...tất cả chính hãng gắn lên vừa khít SH350i mà không cần chế cháo thêm, sản phẩm đã quá nổi tiếng với thị trường Việt Nam.', 'img/product/product4.png', 100, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (8, 4, N'Mâm RCB 8 cây chính hãng cho Exciter 150-155', 750000, 0.13, N'Mâm Rapido chính hãng cho Honda Winner, mâm gắn như zin không chế cháo, mâm thiết kế rất hiện tại, khi gắn lên xe nhìn rất cứng cáp, chất lượng bền bỉ đã được nhiều Biker tin dùng.', 'img/product/product5.png', 230, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (9, 4, N'Bộ nồi BBS cho Vario, AB', 1800000, 0.17, N'Bộ nồi BBS dành cho Vario, Air Blade gồm: đế bi (chén đựng bi), bi nồi 12g, nắp chụp, cao su kẹp, đế nhôm cánh quạt, bộ 3 càng sau + chuông nồi.', 'img/product/product6.png', 206, 10, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (10, 3, N'Bugi NGK Laser Iridium cho SHVN 2020, SH350i', 12800000, 0.15, N'Bugi laser iridium cho SH2020, SH350i mới, chân siêu dài. Dòng laser này cao cấp hơn dòng iridium power bình thường , đầu kim đánh lửa được khắc Laser chuẩn hơn, giúp lửa chính xác hơn loại Iridium loại thường.', 'img/product/product7.png', 103, 10, NULL, NULL, NULL, NULL, 1)
  insert into Product
  values (11, 4, N'Bình ắc quy khô Motobatt Gel MTZ5S', 1005000, 0.3, N'Bình ắc quy GEL Motobatt MTZ5S. Dung lượng: 12V - 4,2Ah. Công nghệ: 100% GEL, Miễn bảo dưỡng (MF), Van điều áp (VRLA), Hàn máy (SW) Kích thước: 113 x 70 x 85 (mm)', 'img/product/product8.png', 22, 4, NULL, NULL, NULL, NULL, 1)


--Category-2 
  insert into Product
  values (12, 3, N'Lốp Michelin Pilot Moto GP (90/80-14 - 100/80-14)', 702000, 0.1, N'Lốp Michelin Pilot Moto GP 90/80-14 - 100/80-14 dành cho xe Air Blade, Vario, Click Thái...các đời.', 'img/product/product9.png', 42, 5, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (13, 4, N'Lốp Dunlop D115 (80/90-14 - 90/90-14)', 590000, 0.1, N'Lốp Dunlop D115 80/90-14 - 90/90-14 dành cho xe Air Blade, Vision, Vario, Click Thái, Genio, Beat...các đời.', 'img/product/product10.png', 65, 2, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (14, 4, N'Lốp Maxxis 3D gai kim cương (80/90-14 - 90/90-14)', 450000, 0.1, N'Lốp Maxxis 3D gai kim cương (80/90-14 - 90/90-14) Lốp Maxxis 3D gai kim cương 80/90-14 - 90/90-14 dành cho xe Air Blade, Vision, Vario, Click Thái, Genio, Beat...các đời.', 'img/product/product11.png', 125, 2, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (15, 3, N'Lốp Pirelli Angel Scooter (90/80-14 - 100/80-14)', 530000, 0.1, N'Lốp Pirelli Angel Scooter 90/80-14 - 100/80-14 dành cho xe Air Blade, Vario, Click Thái...các đời.', 'img/product/product12.png', 43, 2, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (16, 5, N'Lốp Metzeler RoadTec 120/80-16', 530000, 0.1, N'Vỏ Metzeler RoadTec 120/80-16 gắn bánh sau cho SH với thiết kế mới kiểu gai rất ngầu, độ bám đường tốt đặc biệt trên đường ẩm ướt, chất lượng thương hiệu Metzeler đã được khẳng định ở nhiều nơi.', 'img/product/product13.png', 99, 45, NULL, NULL, NULL, NULL, 1)
  
  insert into Product
  values (17, NULL, N'Lốp Aspira Stretto (90/80-14 - 100/80-14)', 600000, 0.1, N'Lốp Aspira Stretto 90/80-14 - 100/80-14 dành cho xe Air Blade, Vario, Click Thái...các đời. Lốp Aspira Stretto với gai vỏ hoàn toàn mới, nhìn rất hiện đại, được sản xuất trên cùng dây chuyền trong cùng nhà máy sản xuất nên 2 thương hiệu lốp xe lớn của thế giới là Pirelli của Ý và Metzeller của Đức.', 'img/product/product14.png', 43, 32, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (18, 2, N'Lốp xe Camel 635 80/90-14', 395000, 0.1, N'Vỏ xe Camel 635 80/90-14 có gai tương tự như vỏ zin các dòng xe Vario của Indo, với nhiều rãnh gai công dụng thoát nước rất nhanh.', 'img/product/product15.png', 56, 2, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (19, 5, N'Lốp Conti Scoot (100/80-16 - 120/80-16)', 1190000, 0.1, N'Lốp Conti Scoot (100/80-16 - 120/80-16) dành cho xe Honda SH 125i , 150i , 160i các đời. ContiScoot - Dòng vỏ xe tay ga cao cấp ưu việt mới của thương hiệu lốp Continental nổi tiếng.', 'img/product/product16.png', 55, 12, NULL, NULL, NULL, NULL, 1)


  --category-3
  insert into Product
  values (20, 2, N'Nhớt Motul 300V Factory Line 10W40 1L', 435000, 0.25, N'Motul 300V Factory Line 10W40 1L nhớt chất lượng cao dành cho xe mô tô phân khối lớn', 'img/product/product17.png', 65, 12, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (21, 2, N'Nhớt Repsol Smarter Scooter 4T 5W-40 0,8L', 180000, 0.25, N'Nhớt Repsol Smarter Scooter 4T 5W-40 0,8 lít, sản phẩm nhớt cho xe tay ga lý tưởng của tập đoàn nhớt Repsol.', 'img/product/product18.png', 65, 12, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (22, 4, N'Shell Advance Xe công nghệ Scooter 10W40 0,8L', 130000, 0.25, N'Shell Advance Xe công nghệ Scooter 10W40 0,8L là dầu nhớt cao cấp công nghệ tổng hợp cho động cơ xe tay ga của tập đoàn nhớt Shell dành riêng cho tài xế xe công nghệ.', 'img/product/product19.png', 165, 142, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (23, 1, N'Nhớt Mobil 1 Gold 5W30', 310000, 0.25, N'Mobil 1 Gold 5W30 nhập khẩu U.S.A .Dầu tổng hợp 100% Đáp ứng yêu cầu khắt khe nhất của các nhà sản xuất.Phù hợp với các dòng xe tay ga hiện đại.', 'img/product/product20.png', 95, 32, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (24, 5, N'Nhớt Liqui Molygen Scooter 10W40', 300000, 0.25, N'Liqui Moly Molygen Scooter 10W40 mang những công thức đặc biệt đúng như tên gọi của nó', 'img/product/product21.png', 55, 32, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (25, 4, N'Nhớt Amsoil 10W40 Scooter 4 Stroke 946ml', 225000, 0.25, N'Nhớt Amsoil 10W40 Scooter 4 Stroke chuyên dùng cho xe tay ga cao cấp.', 'img/product/product22.png', 89, 41, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (26, 2, N'Nhớt Maxima Full Syn 10W40', 640000, 0.25, N'Nhớt Maxima Full Syn 10W40 là dòng nhớt hoàn toàn mới của Maxima, với nhiều công nghệ như chống mài mòn các chi tiết máy cực tốt', 'img/product/product23.png', 12, 6, NULL, NULL, NULL, NULL, 1)

  insert into Product
  values (27, 4, N'Nhớt Gulf Western Oil SYN-M 4T Premium Scooter 10W40 0,8L', 265000, 0.25, N'Nhớt Gulf Western Oil SYN-M 4T Premium Scooter là loại dầu tổng hợp được pha chế đặc biệt để đáp ứng yêu cầu của những động cơ hoạt động lâu dài', 'img/product/product24.png', 52, 21, NULL, NULL, NULL, NULL, 1)

  
  --Product list image--
  insert into [Pro list image]
  values('img/product/product1_1.png',1)
  insert into [Pro list image]
  values('img/product/product1_2.png',1)
  insert into [Pro list image]
  values('img/product/product2_1.png',2)
  insert into [Pro list image]
  values('img/product/product2_2.png',2)
  insert into [Pro list image]
  values('img/product/product3_1.png',3)
  insert into [Pro list image]
  values('img/product/product3_2.png',3)
  insert into [Pro list image]
  values('img/product/product4_1.png',4)
  insert into [Pro list image]
  values('img/product/product4_2.png',4)
  insert into [Pro list image]
  values('img/product/product5_1.png',5)
  insert into [Pro list image]
  values('img/product/product5_2.png',5)
  insert into [Pro list image]
  values('img/product/product6_1.png',6)
  insert into [Pro list image]
  values('img/product/product6_2.png',6)
  insert into [Pro list image]
  values('img/product/product7_1.png',7)
  insert into [Pro list image]
  values('img/product/product7_2.png',7)
  insert into [Pro list image]
  values('img/product/product8_1.png',8)
  insert into [Pro list image]
  values('img/product/product8_2.png',8)
  insert into [Pro list image]
  values('img/product/product9_1.png',9)
  insert into [Pro list image]
  values('img/product/product9_2.png',9)
  insert into [Pro list image]
  values('img/product/product10_1.png',10)
  insert into [Pro list image]
  values('img/product/product10_2.png',10)
  insert into [Pro list image]
  values('img/product/product11_1.png',11)
  insert into [Pro list image]
  values('img/product/product11_2.png',11)
  insert into [Pro list image]
  values('img/product/product12_1.png',12)
  insert into [Pro list image]
  values('img/product/product12_2.png',12)
  insert into [Pro list image]
  values('img/product/product13_1.png',13)
  insert into [Pro list image]
  values('img/product/product13_2.png',13)
  insert into [Pro list image]
  values('img/product/product14_1.png',14)
  insert into [Pro list image]
  values('img/product/product14_2.png',14)
  insert into [Pro list image]
  values('img/product/product15_1.png',15)
  insert into [Pro list image]
  values('img/product/product15_2.png',15)
  insert into [Pro list image]
  values('img/product/product16_1.png',16)
  insert into [Pro list image]
  values('img/product/product16_2.png',16)
  insert into [Pro list image]
  values('img/product/product17_1.png',17)
  insert into [Pro list image]
  values('img/product/product17_2.png',17)
  insert into [Pro list image]
  values('img/product/product18_1.png',18)
  insert into [Pro list image]
  values('img/product/product18_2.png',18)
  insert into [Pro list image]
  values('img/product/product19_1.png',19)
  insert into [Pro list image]
  values('img/product/product19_2.png',19)
  insert into [Pro list image]
  values('img/product/product20_1.png',20)
  insert into [Pro list image]
  values('img/product/product20_2.png',20)
  insert into [Pro list image]
  values('img/product/product21_1.png',21)
  insert into [Pro list image]
  values('img/product/product21_2.png',21)
  insert into [Pro list image]
  values('img/product/product22_1.png',22)
  insert into [Pro list image]
  values('img/product/product22_2.png',22)
  insert into [Pro list image]
  values('img/product/product23_1.png',23)
  insert into [Pro list image]
  values('img/product/product23_2.png',23)
  insert into [Pro list image]
  values('img/product/product24_1.png',24)
  insert into [Pro list image]
  values('img/product/product24_2.png',24)

  insert into advertiser
  values('img/banner/b1.jpg', null)
  insert into advertiser
  values('img/banner/b2.jpg', null)
  insert into advertiser
  values('img/banner/b3.jpg', null)
  insert into advertiser
  values('img/banner/b4.jpg', null)
  insert into advertiser
  values('img/banner/b5.jpg', null)

  insert into Banner
  values(N'Phụ Tùng Xe Máy', N'Cung cấp đầy đủ phụ tùng thay thế cho xe máy, 
  từ các bộ phận nhỏ như bóng đèn, chổi than, ắc quy đến những bộ phận lớn như thùng xe, yên xe và các phụ kiện khác.',
  'img/banner/phutung.png', 'Phu-tung-thay-the-1')
  insert into Banner
  values(N'Lốp Xe Máy', N'Chuyên cung cấp các loại lốp xe chất lượng cao và đa dạng cho khách hàng. 
  Tại đây, bạn có thể tìm thấy các loại lốp xe từ các thương hiệu nổi tiếng và chất lượng như Michelin, Bridgestone, Continental, Pirelli và nhiều hãng khác.',
  'img/banner/lopxe.png', 'Lop-xe-may-2')
  insert into Banner
  values(N'Dầu Nhớt Xe Máy', N'Các loại dầu xe máy chất lượng cao và đa dạng luôn được bầy bán trên kệ cho khách hàng. Tại đây, bạn có thể tìm thấy các loại dầu động cơ, dầu nhớt,
  dầu bánh răng và nhiều sản phẩm khác từ các thương hiệu nổi tiếng như Mobil, Shell, Castrol, Motul và nhiều hãng khác.',
  'img/banner/daunhot.png', 'Nhot-xe-may-3')


