/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Role;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import model.RoleModel;
import model.UserModel;

/**
 *
 * @author black
 */
@MultipartConfig
@WebServlet(name = "UserManagement", urlPatterns = {"/userManagement"})
public class UserManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserModel dao = new UserModel();
        List<User> UserList = dao.getAllUser();
        request.setAttribute("UserList", UserList);
        request.getRequestDispatcher("dashboard/userManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }

        if (action.equalsIgnoreCase("editUser")) {
            doPost_EditUser(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_Update(request, response);
        } else if (action.equalsIgnoreCase("addUser")) {
            doPost_AddUser(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            doPost_Add(request, response);
        }
    }

    public String createImage(Part file, String path) {
        String imageFileName;
        String imageFile = file.getSubmittedFileName();
        if (imageFile != null && !imageFile.isEmpty()) {
            imageFileName = path.concat(imageFile);
            String uploadPath = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/user/avatar/" + imageFile;
            try {
                FileOutputStream fos = new FileOutputStream(uploadPath);
                InputStream is = file.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    protected void doPost_AddUser(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("dashboard/addUser.jsp").forward(request, response);
    }

    protected void doPost_EditUser(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = 0;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
            System.out.println(e);
        }
        RoleModel roleDB = new RoleModel();
        List<Role> roleList = roleDB.getRoleUser();
        UserModel userDB = new UserModel();
        User user = userDB.getUserById(id);
        request.setAttribute("id", id);
        request.setAttribute("fullname", user.getFullName());
        request.setAttribute("username", user.getUsername());
        request.setAttribute("email", user.getEmail());
        request.setAttribute("phone", user.getPhoneNumber());
        request.setAttribute("roleId", user.getRoleId());
        request.setAttribute("status", user.getStatus());
        request.setAttribute("avartar", user.getAvatar());
        request.setAttribute("roleList", roleList);
        request.getRequestDispatcher("dashboard/editUser.jsp").forward(request, response);
    }

    protected void doPost_Update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserModel userDB = new UserModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int id = Integer.parseInt(request.getParameter("id"));
            User u = userDB.getUserById(id);
            Part file = request.getPart("image");
            String username = request.getParameter("username");
            String email = request.getParameter("email");
//            int roleId = Integer.parseInt(request.getParameter("role"));
            int roleId = 1;
            String fullname = request.getParameter("fullname");
            String phone = request.getParameter("phone");
            int status = Integer.parseInt(request.getParameter("status"));
            String image = createImage(file, "img/user/avatar/");
            if (image == null) {
                image = u.getAvatar();
            }
            User user = new User();
            user.setRoleId(roleId);
            user.setUsername(username);
            user.setEmail(email);
            user.setPhoneNumber(phone);
            user.setAddress(u.getAddress());
            user.setFullName(fullname);
            user.setCity(u.getCity());
            user.setDistrict(u.getDistrict());
            user.setWard(u.getWard());
            user.setAvatar(image);
            user.setStatus(status);
            userDB.update(user);
            response.sendRedirect("userManagement");
        } else {
            response.sendRedirect("userManagement");
        }
    }

    protected void doPost_Add(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserModel userDB = new UserModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int roleId = Integer.parseInt(request.getParameter("role"));
            String username = request.getParameter("username");
            String pass = request.getParameter("password");
            String email = request.getParameter("email");
            int status = Integer.parseInt(request.getParameter("status"));
            String phone = request.getParameter("phone");
            String address = request.getParameter("address");
            String fullname = request.getParameter("fullname");
            String city = request.getParameter("province1");
            String district = request.getParameter("district1");
            String ward = request.getParameter("ward1");
            Part file = request.getPart("image");
            String image = createImage(file, "img/user/avatar/");
            userDB.createUser(roleId, username, pass, email, status, phone, address, fullname, city, district, ward, image);
            response.sendRedirect("userManagement");
        } else {
            response.sendRedirect("userManagement");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
