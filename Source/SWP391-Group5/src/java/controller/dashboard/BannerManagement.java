/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Advertiser;
import entity.Banner;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import model.BannerModel;
import model.CategoryModel;

/**
 *
 * @author ADMIN
 */
@MultipartConfig
@WebServlet(name = "BannerManagement", urlPatterns = {"/bannerManagement"})
public class BannerManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CategoryManagament</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CategoryManagament at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BannerModel bannerDB = new BannerModel();
        CategoryModel cateDB = new CategoryModel();
        List<Advertiser> listAd = bannerDB.getAllAdver();
        List<Banner> listB = bannerDB.getAllBanner();
        request.setAttribute("listB", listB);
        request.setAttribute("A1", listAd.get(0).getImg());
        request.setAttribute("A2", listAd.get(1).getImg());
        request.setAttribute("A3", listAd.get(2).getImg());
        request.setAttribute("A4", listAd.get(3).getImg());
        request.setAttribute("A5", listAd.get(4).getImg());
        request.getRequestDispatcher("dashboard/bannerManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("addBanner")) {
            doPost_addBanner(request, response);
        } else if (action.equalsIgnoreCase("editBanner")) {
            doPost_editBanner(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_update(request, response);
        } else if (action.equalsIgnoreCase("create")) {
            doPost_create(request, response);
        } else if (action.equalsIgnoreCase("editAdver")) {
            doPost_editAdver(request, response);
        }
    }

    protected void doPost_addBanner(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("dashboard/addBanner.jsp").forward(request, response);
    }

    protected void doPost_editBanner(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        BannerModel bannerDB = new BannerModel();
        Banner banner = bannerDB.getBannerById(id);
        request.setAttribute("banner", banner);
        request.getRequestDispatcher("dashboard/editBanner.jsp").forward(request, response);
    }

    protected void doPost_update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BannerModel bannerModel = new BannerModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int id = Integer.parseInt(request.getParameter("id"));
            String title = request.getParameter("title");
            Part file = request.getPart("image");
            String path = "img/banner/";
            String image = createImage(file, path);
            String des = request.getParameter("des");
            String link = request.getParameter("link");
            if (image == null) {
                image = bannerModel.getBannerById(id).getImage();
            }
            bannerModel.updateBanner(id, title, des, image, link);
            response.sendRedirect("bannerManagement");
        } else {
            response.sendRedirect("bannerManagement");
        }
    }

    protected void doPost_create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BannerModel bannerModel = new BannerModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            String title = request.getParameter("title");
            Part file = request.getPart("image");
            String path = "img/banner/";
            String image = createImage(file, path);
            String des = request.getParameter("des");
            String link = request.getParameter("link");
            bannerModel.createBanner(title, des, image, link);
            response.sendRedirect("bannerManagement");
        } else {
            response.sendRedirect("bannerManagement");
        }
    }

    protected void doPost_editAdver(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BannerModel bannerModel = new BannerModel();
        Part file1 = request.getPart("image1");
        Part file2 = request.getPart("image2");
        Part file3 = request.getPart("image3");
        Part file4 = request.getPart("image4");
        Part file5 = request.getPart("image5");
        String path = "img/banner/";
        if (!file1.getSubmittedFileName().isEmpty()) {
            String imageList = createImage(file1, path);
            bannerModel.updateImageBanner(1, imageList);
        }
        if (!file2.getSubmittedFileName().isEmpty()) {
            String imageList = createImage(file2, path);
            bannerModel.updateImageBanner(2, imageList);
        }
        if (!file3.getSubmittedFileName().isEmpty()) {
            String imageList = createImage(file3, path);
            bannerModel.updateImageBanner(3, imageList);
        }
        if (!file4.getSubmittedFileName().isEmpty()) {
            String imageList = createImage(file4, path);
            bannerModel.updateImageBanner(4, imageList);
        }
        if (!file5.getSubmittedFileName().isEmpty()) {
            String imageList = createImage(file5, path);
            bannerModel.updateImageBanner(5, imageList);
        }
        response.sendRedirect("bannerManagement");
    }

    public String createImage(Part file, String path) {
        String imageFileName;
        String imageFile = file.getSubmittedFileName();
        if (imageFile != null && !imageFile.isEmpty()) {
            imageFileName = path.concat(imageFile);
            String uploadPath = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/banner/" + imageFile;
            try {
                FileOutputStream fos = new FileOutputStream(uploadPath);
                InputStream is = file.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
