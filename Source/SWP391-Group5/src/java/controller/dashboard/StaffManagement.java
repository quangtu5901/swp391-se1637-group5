/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Admin;
import entity.Role;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.AdminModel;
import model.RoleModel;

/**
 *
 * @author black
 */
@WebServlet(name = "StaffManagement", urlPatterns = {"/staffManagement"})
public class StaffManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StaffManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StaffManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AdminModel dao = new AdminModel();
        List<Admin> list = dao.getAllStaff();
        request.setAttribute("StaffList", list);
        request.getRequestDispatcher("dashboard/staffManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }

        if (action.equalsIgnoreCase("EditStaff")) {
            doPost_EditStaff(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_Update(request, response);
        }
    }

    protected void doPost_EditStaff(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = 0;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
        }

        AdminModel adDB = new AdminModel();
        RoleModel roleDB = new RoleModel();
        Admin ad = adDB.getAdminByID(id);
        List<Role> roleList = roleDB.getALlRole();
        request.setAttribute("id", id);
        request.setAttribute("fullname", ad.getFullname());
        request.setAttribute("username", ad.getUsername());
        request.setAttribute("email", ad.getEmail());
        request.setAttribute("phone", ad.getPhone());
        request.setAttribute("roleId", ad.getRoleId());
        request.setAttribute("status", ad.getStatus());
        request.setAttribute("avartar", ad.getAvartar());
        request.setAttribute("roleList", roleList);
        request.getRequestDispatcher("dashboard/editStaff.jsp").forward(request, response);

    }

    protected void doPost_Update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AdminModel adminDB = new AdminModel();
        Admin ad = new Admin();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int id = Integer.parseInt(request.getParameter("id"));
            ad.setId(id);
            ad.setUsername(request.getParameter("username"));
            ad.setFullname(request.getParameter("fullname"));
            ad.setEmail(request.getParameter("email"));
            ad.setPhone(request.getParameter("phone"));
            ad.setStatus(Integer.parseInt(request.getParameter("status")));
            ad.setAvartar(request.getParameter("image"));
            ad.setRoleId(Integer.parseInt(request.getParameter("role")));
            adminDB.UpdateAdmin(ad);
            response.sendRedirect("staffManagement");
        } else {
            response.sendRedirect("staffManagement");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
