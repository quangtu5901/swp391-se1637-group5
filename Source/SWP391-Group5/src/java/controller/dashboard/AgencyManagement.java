/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.dashboard;

import entity.Role;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.RoleModel;
import model.UserModel;

/**
 *
 * @author black
 */
@WebServlet(name="AgencyManagement", urlPatterns={"/agencyManagement"})
public class AgencyManagement extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AgencyManagement</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AgencyManagement at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        UserModel dao = new UserModel();
        List<User> UserList = dao.getAgency();
        request.setAttribute("UserList", UserList);
        request.getRequestDispatcher("dashboard/agencyManagement.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }

        if (action.equalsIgnoreCase("editUser")) {
            doPost_EditUser(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_Update(request, response);
        }
    }
    
    
    
    protected void doPost_EditUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         int id = 0;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
        }
        
        RoleModel roleDB = new RoleModel();
        List<Role> roleList = roleDB.getRoleUser();
        UserModel userDB = new UserModel();
        User user = userDB.getUserById(id);
        request.setAttribute("id", id);
        request.setAttribute("fullname", user.getFullName());
        request.setAttribute("username", user.getUsername());
        request.setAttribute("email", user.getEmail());
        request.setAttribute("phone", user.getPhoneNumber());
        request.setAttribute("roleId", user.getRoleId());
        request.setAttribute("status", user.getStatus());
        request.setAttribute("avartar", user.getAvatar());
        request.setAttribute("roleList", roleList);
        request.getRequestDispatcher("dashboard/editUser.jsp").forward(request, response);
    }
    
    
    protected void doPost_Update (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserModel userDB = new UserModel();
        User ad = new User();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int id = Integer.parseInt(request.getParameter("id"));
            ad.setId(id);
            ad.setUsername(request.getParameter("username"));
            ad.setFullName(request.getParameter("fullname"));
            ad.setEmail(request.getParameter("email"));
            ad.setPhoneNumber(request.getParameter("phone"));
            ad.setStatus(Integer.parseInt(request.getParameter("status")));
            ad.setAvatar(request.getParameter("image"));
            ad.setRoleId(Integer.parseInt(request.getParameter("role")));
            userDB.update(ad);
            response.sendRedirect("userManagement");
        } else {
            response.sendRedirect("userManagement");
        }
    }


    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
