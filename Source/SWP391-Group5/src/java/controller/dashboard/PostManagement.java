/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Post;
import entity.ShowOrderDetail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.OrderDetailModel;
import model.PostModel;

/**
 *
 * @author asus
 */
@WebServlet(name = "PostManagement", urlPatterns = {"/postmanagementadmin"})
public class PostManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostModel p = new PostModel();
        List<Post> listPost = p.getAllPostForAdmin();
        request.setAttribute("list", listPost);

        request.getRequestDispatcher("dashboard/postManagementAdmin.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
        }

        if (action.equalsIgnoreCase("update")) {
            doPost_update(request, response);
        } else if (action.equalsIgnoreCase("search")) {
            doPost_search(request, response);
        }
    }

    protected void doPost_update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int postId = 0;
        String status = "";
        int StatusNum = -1;
        try {
            postId = Integer.parseInt(request.getParameter("id"));
            status = (request.getParameter("submit"));
            if (status.equalsIgnoreCase("Approve")) {
                StatusNum = 1;
            } else {
                StatusNum = 2;
            }
        } catch (Exception e) {
        }
        PostModel p = new PostModel();
        
        p.UpdateStatusPost(postId, StatusNum);
        
        
        //out.print(postId+","+ StatusNum);
        response.sendRedirect("postmanagementadmin");
    }

    protected void doPost_search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderDetailModel orderDB = new OrderDetailModel();
        int orderId = -1;
        try {
            orderId = Integer.parseInt(request.getParameter("txt"));
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(orderId);
        List<ShowOrderDetail> list = orderDB.getListShowOrderDetail(orderId);
        System.out.println(list.get(0).getOrderId());
        PrintWriter out = response.getWriter();
        out.print("<div class=\"modal-dialog\" id=\"orderDetail\">\n"
                + "                <div class=\"modal-content\" style=\"min-width: 610px;\">"
                + "                    <div class=\"modal-header\">						\n"
                + "                        <h4 class=\"modal-title\">Order Detail</h4>\n"
                + "                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-body\">\n"
                + "                        <div class=\"form-group\">\n"
                + "                            <label>User Name: </label><span> " + list.get(0).getUserName() + " </span>\n"
                + "                        </div>\n"
                + "                        <div class=\"form-group\">\n"
                + "                            <label>User Email: </label><span> " + list.get(0).getEmail() + " </span>\n"
                + "                        </div>\n"
                + "                        <div class=\"form-group\">\n"
                + "                            <label>User Phone Number: </label><span> " + list.get(0).getPhoneNumber() + " </span>\n"
                + "                        </div>\n"
                + "                        <table class=\"table align-items-center mb-0\">\n"
                + "                            <thead>\n"
                + "                                <tr>\n"
                + "                                    <th class=\"text-uppercase text-secondary text-xxs font-weight-bolder opacity-7\">Product name</th>\n"
                + "                                    <th class=\"text-uppercase text-secondary text-xxs font-weight-bolder opacity-7\">Price</th>\n"
                + "                                    <th class=\"text-uppercase text-secondary text-xxs font-weight-bolder opacity-7\">Quantity</th>\n"
                + "                                </tr>\n"
                + "                            </thead>\n"
                + "                            <tbody>\n");
        for (ShowOrderDetail orderDetail : list) {
            out.print("                                <tr>    \n"
                    + "                                    <td>\n"
                    + "                                        <div class=\"d-flex px-2 py-1\">\n"
                    + "                                            <div class=\"d-flex flex-column \" name=\"orderId\">\n"
                    + "                                                <h6 class=\"mb-0 text-sm\">" + orderDetail.getProductName() + "</h6>\n"
                    + "                                            </div>       \n"
                    + "                                        </div>\n"
                    + "                                    </td>\n"
                    + "                                    <td>\n"
                    + "                                        <div class=\"d-flex px-2 py-1\">\n"
                    + "                                            <div class=\"d-flex flex-column\">\n"
                    + "                                                <h6 class=\"mb-0 text-sm\">" + orderDetail.getPrice() + "</h6>\n"
                    + "                                            </div>       \n"
                    + "                                        </div>\n"
                    + "                                    </td>\n"
                    + "                                    <td class=\"align-middle text-center text-sm\">\n"
                    + "                                        <div class=\"d-flex px-2 py-1\">\n"
                    + "                                            <div class=\"d-flex flex-column\">\n"
                    + "                                                <h6 class=\"mb-0 text-sm\">" + orderDetail.getQuantity() + "</h6>\n"
                    + "                                            </div>       \n"
                    + "                                        </div>\n"
                    + "                                    </td>\n"
                    + "                                </tr>\n");
        }
        out.print("                            </tbody>\n"
                + "                        </table>\n"
                + "                        <div class=\"modal-footer\">\n"
                + "                            <div class=\"d-flex px-2 py-1\">\n"
                + "                                <div class=\"d-flex flex-column\">\n"
                + "                                </div>       \n"
                + "                            </div>\n"
                + "                        </div>\n"
                + "                    </div>\n"
                + "                </div>\n"
                + "            </div>"
        );
//        request.getRequestDispatcher("orderManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
