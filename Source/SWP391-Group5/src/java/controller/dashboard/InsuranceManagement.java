/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Advertiser;
import entity.Banner;
import entity.Category;
import entity.CategoryProduct;
import entity.Insurance;
import entity.Order;
import entity.Product;
import entity.ShowOrderDetail;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BannerModel;
import model.CategoryModel;
import model.InsuranceModel;
import model.OrderDetailModel;
import model.OrderModel;
import model.ProductModel;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "InsuranceManagement", urlPatterns = {"/insurManagement"})
public class InsuranceManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        InsuranceModel insurDB = new InsuranceModel();
        List<Insurance> list = insurDB.getListInsurance();
        request.setAttribute("list", list);
        request.getRequestDispatcher("dashboard/insuranceManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
        }
        if (action.equalsIgnoreCase("search")) {
            doPost_search(request, response);
        } else if (action.equalsIgnoreCase("create")) {
            doPost_create(request, response);
        }
    }

    protected void doPost_search(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        InsuranceModel inDB = new InsuranceModel();
        int id = -1;
        try {
            id = Integer.parseInt(request.getParameter("txt"));
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(id);
        Insurance insur = inDB.getInsuranceById(id);
        PrintWriter out = response.getWriter();
        out.print(
                "            <div class=\"modal-dialog\" id=\"orderDetail\">\n"
                + "                <div class=\"modal-content\" style=\"min-width: 800px;\">\n"
                + "                    <div class=\"modal-header\">						\n"
                + "                        <h4 class=\"modal-title\">Insurance Detail</h4>\n"
                + "                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-body\">\n"
                + "                        <div class=\"col-md-12\">\n"
                + "                            <div class=\"des-order col-md-12\">\n"
                + "                                <h5>Vehicle Insurance</h5>\n"
                + "                            </div>\n"
                + "                            <!-- End .header-order -->\n"
                + "                            <div class=\"content-order\">\n"
                + "                                <p><i class=\"fa fa-taxi\" aria-hidden=\"true\"></i><span>Plate Number: <span>" + insur.getPlateNumber() + "</span> </span></p>\n"
                + "                                <p>\n"
                + "                                    <i class=\"fa fa-flickr\" aria-hidden=\"true\"></i> <span style=\"font-size: 14.5px\">Insurance Date: <span>" + insur.getDateFrom() + "</span> - <span>" + insur.getDateTo() + "</span></span>\n"
                + "                                </p>\n"
                + "                                <p class=\"pe-ta\"><i class=\"fa fa-cog\" aria-hidden=\"true\"></i><span>Owner Name: <span>" + insur.getOwnerName() + "</span> </span></p>\n"
                + "                                <p class=\"pe-ta\"><i class=\"fa fa-cog\" aria-hidden=\"true\"></i><span>Vehicle Type: <span>" + insur.getVehicleType() + "</span> </span></p>\n"
                + "                                <p class=\"sum-su\">\n"
                + "                                    <i class=\"fa fa-usd\" aria-hidden=\"true\"></i><span>Total price: <span id=\"total\"> " + insur.formatPrice(insur.getTotalPrice()) + "</span></span>\n"
                + "                                </p>\n"
                + "                            </div>\n"
                + "                            <!-- End .content-order -->\n"
                + "                            <!-- End .info-order -->\n"
                + "                        </div>\n"
                + "                    </div>\n"
                + "                    <div class=\"modal-footer\">\n"
                + "                    </div>\n"
                + "                </div>\n"
                + "            </div>"
        );
//        request.getRequestDispatcher("orderManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void doPost_create(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InsuranceModel inDB = new InsuranceModel();
        int year = Integer.parseInt(request.getParameter("year"));
        int vehicleType = Integer.parseInt(request.getParameter("loaixe"));
        String baohiem2 = "";
        if (request.getParameter("baohiem2") != null) {
            baohiem2 = request.getParameter("baohiem2");
        }

        String strVehicleType = "";
        boolean boolbh = false;
        Date dateFrom = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateFrom = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("thoihan"));
        } catch (ParseException ex) {
        }
        String strDateFrom = dateFormat.format(dateFrom);
        Calendar c = Calendar.getInstance();
        c.setTime(dateFrom);
        c.add(Calendar.YEAR, year);
        Date dateTo = c.getTime();
        String strDateTo = dateFormat.format(dateTo);
        double fee1 = 0;
        double fee2 = 0;
        double total;
        if (!baohiem2.equals("")) {
            fee2 = 20000;
            boolbh = true;
        }
        switch (vehicleType) {
            case 1:
                fee1 = 60500;
                strVehicleType = "Xe Mô tô 2 bánh dung tích từ 50cc trở xuống";
                break;
            case 2:
                fee1 = 66000;
                strVehicleType = "Xe Mô tô 2 bánh dung tích trên 50cc";
                break;
            case 3:
                fee1 = 319000;
                strVehicleType = "Mô tô 3 bánh";
                break;
            case 4:
                fee1 = 60500;
                strVehicleType = "Xe máy điện";
                break;
            case 5:
                fee1 = 319000;
                strVehicleType = "Các loại xe còn lại";
                break;
        }
        switch (year) {
            case 1:
                fee1 *= 1;
                if (baohiem2 != "") {
                    fee2 *= 1;
                }
                break;
            case 2:
                fee1 *= 2;
                if (baohiem2 != "") {
                    fee2 *= 2;
                }
                break;
            case 3:
                fee1 *= 3;
                if (baohiem2 != "") {
                    fee2 *= 3;
                }
                break;
        }
        total = fee1 + fee2;
        String ownerName = request.getParameter("ownerName");
        String plateNumber = request.getParameter("plateNumber");
        inDB.insertInsurance(year, strVehicleType, ownerName, plateNumber, strDateFrom, strDateTo, true, boolbh, total, 1);
        ProductModel productModel = new ProductModel();
        CategoryModel categoryModel = new CategoryModel();
        BannerModel bannerDB = new BannerModel();
        List<Advertiser> listAd = bannerDB.getAllAdver();
        List<Banner> listB = bannerDB.getAllBanner();
        List<CategoryProduct> listP = categoryModel.CategoryFollowProduct();
        List<Category> ListC = categoryModel.getAllCategory();
        List<Product> listP1 = productModel.getViewNumber(10);
        request.setAttribute("listP", listP);
        request.setAttribute("listC", ListC);
        request.setAttribute("listB", listB);
        request.setAttribute("listP1", listP1);
        request.setAttribute("A1", listAd.get(0).getImg());
        request.setAttribute("A2", listAd.get(1).getImg());
        request.setAttribute("A3", listAd.get(2).getImg());
        request.setAttribute("A4", listAd.get(3).getImg());
        request.setAttribute("A5", listAd.get(4).getImg());
        request.getRequestDispatcher("home.jsp").forward(request, response);

    }

}
