/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import com.google.gson.Gson;
import entity.Admin;
import entity.Brand;
import entity.Category;
import entity.ProListImage;
import entity.Product;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.BrandModel;
import model.CategoryModel;
import model.ProListImageModel;
import model.ProductModel;

/**
 *
 * @author ADMIN
 */
@MultipartConfig(
        location = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/product",
        fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)
@WebServlet(name = "ProductManagement", urlPatterns = {"/productManagement"})
public class ProductManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Dashboard</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Dashboard at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }

        ProductModel proDB = new ProductModel();
        CategoryModel cateDB = new CategoryModel();
        List<Product> list = proDB.getAllProduct(0, 0);
        List<Category> cList = cateDB.getAllCategory();
        request.setAttribute("cList", cList);
        request.setAttribute("Prolist", list);
        if (action.equalsIgnoreCase("find")) {
            doGet_Find(request, response);
        }
        request.getRequestDispatcher("dashboard/productManagement.jsp").forward(request, response);
    }

    protected void doGet_Find(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        int id = Integer.parseInt(request.getParameter("id"));
        ProductModel ProDB = new ProductModel();
        Product p = ProDB.getProductDetail(id);
        Gson gson = new Gson();
        PrintWriter writer = response.getWriter();
        writer.print(gson.toJson(p));
        writer.flush();
        writer.close();

    }

    protected void doPost_Update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Admin a = (Admin) session.getAttribute("management");
        ProductModel proDB = new ProductModel();
        ProListImageModel proListImageModel = new ProListImageModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            int id = Integer.parseInt(request.getParameter("id"));
            int cateId = Integer.parseInt(request.getParameter("subcategory"));
            String name = request.getParameter("name");
            double price = Double.parseDouble(request.getParameter("price"));
            double discount = Double.parseDouble(request.getParameter("discount"));
            String description = request.getParameter("description");
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            int status = Integer.parseInt(request.getParameter("status"));
            int brand = Integer.parseInt(request.getParameter("brandId"));
            String dateModified = LocalDateTime.parse(now.format(formatDate), formatDate).toString();
            String path = "img/product/";
            ArrayList<String> listImage = new ArrayList<>();
            try {
                Collection<Part> parts = request.getParts();
                for (Part filePart : parts) {
                    String fileName = getFileName(filePart);
                    if (fileName != null) {
                        filePart.write(fileName);
                        listImage.add(path.concat(fileName));
                    }
                }
            } catch (IOException e) {
                listImage.add(proDB.getProductDetail(id).getImage());
                List<ProListImage> listImagePro = proListImageModel.getProListImage(id);
                for (ProListImage proListImage : listImagePro) {
                    listImage.add(proListImage.getImage());
                }
            }
            proDB.updateProduct(id, cateId, name, price, discount, description, quantity, a.getId(), dateModified, status, listImage, brand);
            response.sendRedirect("productManagement");
        } else {
            response.sendRedirect("productManagement");
        }

    }

    protected void doPost_Create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Admin a = (Admin) session.getAttribute("management");
        ProductModel proDB = new ProductModel();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        int cateId = Integer.parseInt(request.getParameter("subcategory"));
        String name = request.getParameter("name");
        double price = Double.parseDouble(request.getParameter("price"));
        double discount = Double.parseDouble(request.getParameter("discount"));
        String description = request.getParameter("description");
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        int status = Integer.parseInt(request.getParameter("status"));
        int brand = Integer.parseInt(request.getParameter("brandId"));
        String dateCreated = LocalDateTime.parse(now.format(formatDate), formatDate).toString();
        String path = "img/product/";
        ArrayList<String> listImage = new ArrayList<>();
        try {
            Collection<Part> parts = request.getParts();
            for (Part filePart : parts) {
                String fileName = getFileName(filePart);
                if (fileName != null) {
                    filePart.write(fileName);
                    listImage.add(path.concat(fileName));
                }
            }
        } catch (IOException e) {
            listImage = null;
        }
        proDB.insertProduct(cateId, name, price, discount, description, quantity, a.getId(), dateCreated, null, status, listImage, brand);
        response.sendRedirect("productManagement");

    }

    protected void doPost_addProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandBD = new BrandModel();
        CategoryModel cateDB = new CategoryModel();
        List<Brand> bList = brandBD.getAllBrand();
        List<Category> cList = cateDB.getAllCategory();
        request.setAttribute("cList", cList);
        request.setAttribute("bList", bList);
        request.getRequestDispatcher("dashboard/addProduct.jsp").forward(request, response);
    }

    protected void doPost_editProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = 0;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
            System.out.println(e);
        }
        ProductModel proDB = new ProductModel();
        BrandModel brandBD = new BrandModel();
        CategoryModel cateDB = new CategoryModel();
        ProListImageModel proListImage = new ProListImageModel();
        List<Category> cList = cateDB.getAllCategory();
        List<Brand> bList = brandBD.getAllBrand();
        Product p = proDB.getProductDetail(id);
        List<ProListImage> listPLI = proListImage.getProListImage(id);
        request.setAttribute("id", id);
        request.setAttribute("name", p.getProductName());
        request.setAttribute("price", p.getPrice());
        request.setAttribute("discount", p.getDiscount());
        request.setAttribute("des", p.getDescription());
        request.setAttribute("quantity", p.getQuantity());
        request.setAttribute("image", p.getImage());
        request.setAttribute("status", p.getProductStatus());
        request.setAttribute("parentId", p.getCategory().getParentId());
        request.setAttribute("cateId", p.getCategory().getCateId());
        request.setAttribute("brandId", p.getBrandId());
        request.setAttribute("cList", cList);
        request.setAttribute("cList", cList);
        request.setAttribute("listPLI", listPLI);
        request.setAttribute("bList", bList);
        request.getRequestDispatcher("dashboard/editProduct.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("addProduct")) {
            doPost_addProduct(request, response);
        } else if (action.equalsIgnoreCase("editProduct")) {
            doPost_editProduct(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_Update(request, response);
        } else if (action.equalsIgnoreCase("create")) {
            doPost_Create(request, response);
        }

    }

    private String getFileName(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        if (!contentDisposition.contains("filename=")) {
            return null;
        }
        int beginIndex = contentDisposition.indexOf("filename=") + 10;
        int endIndex = contentDisposition.length() - 1;
        return contentDisposition.substring(beginIndex, endIndex);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
