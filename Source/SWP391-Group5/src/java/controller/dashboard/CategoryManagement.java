/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Category;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import model.CategoryModel;
import model.ProductModel;

/**
 *
 * @author ADMIN
 */
@MultipartConfig
@WebServlet(name = "CategoryManagement", urlPatterns = {"/categoryManagement"})
public class CategoryManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CategoryManagament</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CategoryManagament at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        CategoryModel cateDB = new CategoryModel();
        List<Category> cList = cateDB.getAllCategory();
        request.setAttribute("cList", cList);
        request.getRequestDispatcher("dashboard/categoryManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("addCategory")) {
            doPost_addCategory(request, response);
        } else if (action.equalsIgnoreCase("editCategory")) {
            doPost_editCategory(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_update(request, response);
        } else if (action.equalsIgnoreCase("create")) {
            doPost_create(request, response);
        }
    }

    public String createImage(Part file, String path) {
        String imageFileName;
        String imageFile = file.getSubmittedFileName();
        if (imageFile != null && !imageFile.isEmpty()) {
            imageFileName = path.concat(imageFile);
            String uploadPath = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/category/" + imageFile;
            try {
                FileOutputStream fos = new FileOutputStream(uploadPath);
                InputStream is = file.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    protected void doPost_addCategory(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryModel cateDB = new CategoryModel();
        List<Category> cList = cateDB.getAllCategory();
        request.setAttribute("cList", cList);
        request.getRequestDispatcher("dashboard/addCategory.jsp").forward(request, response);
    }

    protected void doPost_editCategory(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = 0;
        try {
            id = Integer.parseInt(request.getParameter("id"));
        } catch (Exception e) {
        }
        CategoryModel cateDB = new CategoryModel();
        Category cate = cateDB.getCategoryById(id);
        List<Category> cList = cateDB.getAllCategoryExcept(id);
        request.setAttribute("id", id);
        request.setAttribute("cateName", cate.getCateName());
        request.setAttribute("image", cate.getImage());
        request.setAttribute("parentId", cate.getParentId());
        request.setAttribute("cateStatus", cate.getCateStatus());
        request.setAttribute("cList", cList);
        request.getRequestDispatcher("dashboard/editCategory.jsp").forward(request, response);
    }

    protected void doPost_update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryModel cateDB = new CategoryModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            int parentId;
            String parentId_raw = request.getParameter("parentId");
            if (parentId_raw != null) {
                parentId = Integer.parseInt(parentId_raw);
            } else {
                parentId = 0;
            }
            int status = Integer.parseInt(request.getParameter("status"));
            String imageFileName = null;
            if (parentId == 0) {
                Part file = request.getPart("image");
                String path = "img/category/";
                imageFileName = createImage(file, path);
            }
            cateDB.updateCategory(id, name, imageFileName, parentId, status);
            response.sendRedirect("categoryManagement");
        } else {
            response.sendRedirect("categoryManagement");
        }
    }

    protected void doPost_create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryModel cateDB = new CategoryModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            String name = request.getParameter("name");
            int parentId = Integer.parseInt(request.getParameter("parentId"));
            int status = Integer.parseInt(request.getParameter("status"));
            String imageFileName = null;
            if (parentId == 0) {
                Part file = request.getPart("image");
                String path = "img/category/";
                imageFileName = createImage(file, path);
            }
            cateDB.insertCategory(name, imageFileName, parentId, status);
            response.sendRedirect("categoryManagement");
        } else {
            response.sendRedirect("categoryManagement");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
