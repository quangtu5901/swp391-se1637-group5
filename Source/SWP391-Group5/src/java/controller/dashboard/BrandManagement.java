/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.dashboard;

import entity.Brand;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import model.BrandModel;

/**
 *
 * @author Saka289
 */
@MultipartConfig
@WebServlet(name = "BrandManagement", urlPatterns = {"/brandManagement"})
public class BrandManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BrandManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BrandManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandModel = new BrandModel();
        List<Brand> listB = brandModel.getAllBrand();
        request.setAttribute("listB", listB);
        request.getRequestDispatcher("dashboard/brandManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("addBrand")) {
            doPost_addBrand(request, response);
        } else if (action.equalsIgnoreCase("editBrand")) {
            doPost_editBrand(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            doPost_update(request, response);
        } else if (action.equalsIgnoreCase("create")) {
            doPost_create(request, response);
        }
    }

    public String createImage(Part file, String path) {
        String imageFileName;
        String imageFile = file.getSubmittedFileName();
        if (imageFile != null && !imageFile.isEmpty()) {
            imageFileName = path.concat(imageFile);
            String uploadPath = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/brand/" + imageFile;
            try {
                FileOutputStream fos = new FileOutputStream(uploadPath);
                InputStream is = file.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        } else {
            imageFileName = null;
        }
        return imageFileName;
    }

    protected void doPost_addBrand(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("dashboard/addBrand.jsp").forward(request, response);
    }

    protected void doPost_editBrand(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandModel = new BrandModel();
        int brandId = Integer.parseInt(request.getParameter("id"));
        Brand brand = brandModel.getBrandDetail(brandId);
        request.setAttribute("brand", brand);
        request.getRequestDispatcher("dashboard/editBrand.jsp").forward(request, response);
    }

    protected void doPost_update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandModel = new BrandModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            int brandID = Integer.parseInt(request.getParameter("brandId"));
            Brand brand = brandModel.getBrandDetail(brandID);
            Part file = request.getPart("image");
            String path = "img/brand/";
            String name = request.getParameter("name");
            String image = createImage(file, path);
            if (image == null) {
                image = brand.getImage();
            }
            int status = Integer.parseInt(request.getParameter("status"));
            brandModel.updateBrand(brandID, name, image, status);
            response.sendRedirect("brandManagement");
        } else {
            response.sendRedirect("brandManagement");
        }

    }

    protected void doPost_create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandModel = new BrandModel();
        if (request.getParameter("submit").equalsIgnoreCase("Save")) {
            Part file = request.getPart("image");
            String path = "img/brand/";
            String name = request.getParameter("name");
            String image = createImage(file, path);
            int status = Integer.parseInt(request.getParameter("status"));
            brandModel.insertBrand(name, image, status);
            response.sendRedirect("brandManagement");
        } else {
            response.sendRedirect("brandManagement");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
