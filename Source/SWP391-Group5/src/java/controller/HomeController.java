/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import entity.Advertiser;
import entity.Banner;
import entity.Brand;
import entity.Category;
import entity.CategoryProduct;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.BannerModel;
import model.BrandModel;
import model.CategoryModel;
import model.ProductModel;

/**
 *
 * @author black
 */
@WebServlet(name = "HomeController", urlPatterns = {"/home"})
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BrandModel brandModel = new BrandModel();
        ProductModel productModel = new ProductModel();
        CategoryModel categoryModel = new CategoryModel();
        BannerModel bannerDB = new BannerModel();
        List<Advertiser> listAd = bannerDB.getAllAdver();
        List<Banner> listB = bannerDB.getAllBanner();
        List<CategoryProduct> listP = categoryModel.CategoryFollowProduct();
        List<Category> ListC = categoryModel.getAllCategory();
        List<Product> listP1 = productModel.getViewNumber(10);
        List<Brand> listBrand = brandModel.getAllBrand();
        request.setAttribute("listP", listP);
        request.setAttribute("listC", ListC);
        request.setAttribute("listB", listB);
        request.setAttribute("listP1", listP1);
        request.setAttribute("listB1", listBrand);
        request.setAttribute("A1", listAd.get(0).getImg());
        request.setAttribute("A2", listAd.get(1).getImg());
        request.setAttribute("A3", listAd.get(2).getImg());
        request.setAttribute("A4", listAd.get(3).getImg());
        request.setAttribute("A5", listAd.get(4).getImg());
        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = "";
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("searchAllWeb")) {
            doPost_searchProduct(request, response);
        }
    }

    protected void doPost_searchProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        ProductModel productModel = new ProductModel();
        String textSearch = request.getParameter("textSearch");
        List<Product> listSW = productModel.getSearchProductAllWeb(textSearch);
        if (textSearch != null && !textSearch.isEmpty()) {
            out.print("                <div class=\"container searchWeb\">\n"
                    + "                    <ul class=\"searchWeb1\">");
            for (Product product : listSW) {
                out.print("                        <li class=\"searchWeb2\">\n"
                        + "                            <div class=\"searchWeb-content\">\n"
                        + "                                <div>\n"
                        + "                                    <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                        <img style=\"width: 80px; margin-right: 20px\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                    </a>\n"
                        + "                                </div>\n"
                        + "                                <div style=\"display: flex;flex-direction: column;justify-content: space-between;\">\n"
                        + "                                    <div>\n"
                        + "                                        <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h1 style=\"font-size: 20px; font-family: monospace;height: 30px\"\">" + product.getProductName() + "</h1></a>\n"
                        + "                                    </div>\n"
                        + "                                    <div name=\"category\">Category : " + product.getCategory().getCateName() + "</div>\n"
                        + "                                </div>\n"
                        + "                            </div>\n"
                        + "                            <div class=\"price\">\n"
                        + "                                <h6 style=\"color: #ffba00\">" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                            </div> \n"
                        + "                        </li>");
            }
            out.print("                    </ul>\n"
                    + "                </div>");
            out.close();
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
