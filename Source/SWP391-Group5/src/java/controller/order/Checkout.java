/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import entity.Cart;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.OrderModel;

/**
 *
 * @author black
 */
@WebServlet(name = "Checkout", urlPatterns = {"/checkout"})
public class Checkout extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Checkout</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Checkout at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("order/checkout.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("account");
        Cart cart = (Cart) session.getAttribute("cart");
        OrderModel ord = new OrderModel();
        String ordername = request.getParameter("name");
        String selector_new_address = request.getParameter("selectorAddress");
        String phoneNumber = request.getParameter("phoneNumber");
        String note = request.getParameter("note");
        String old_address = request.getParameter("old_add");
        int paymentMethod = Integer.parseInt(request.getParameter("selectorpayment"));
        String lastAddress = "";

        if (cart == null) {
            request.getRequestDispatcher("checkout").forward(request, response);
            request.setAttribute("nothavecart", "You not have any product in cart");
            return;
        }
        if (selector_new_address != null) {
            String city = request.getParameter("province1");
            String district = request.getParameter("district1");
            String ward = request.getParameter("ward1");
            String address = request.getParameter("add_new");

            if (city != null && district != null && ward != null && address != null) {
                lastAddress = city + ", " + district + ", " + ward + ", " + address;
            } else {
                request.setAttribute("errorAddress", "Address is not update, you should update address for shipping");
                response.sendRedirect("checkout");
            }
        }
        if (selector_new_address == null && old_address == null) {
            request.setAttribute("errorAddress", "Address is not update, you should update address for shipping");
            response.sendRedirect("checkout");

        } else {

            lastAddress = old_address;
        }

        if (user == null) {
            response.sendRedirect("singin");
        } else {
            int orderId = ord.addOrder(user.getId(), cart, ordername, note, lastAddress, phoneNumber);

            if (paymentMethod == 1) {
                session.removeAttribute("cart");
                session.removeAttribute("size");
                session.removeAttribute("total");
                response.sendRedirect("OrderSuccess");
            } else {
//                if (orderId != -1) {
                    session.setAttribute("paymentBanking", String.valueOf(orderId));
                    response.sendRedirect("paymentQR");
//                } else {
//                    response.sendRedirect("home");
//                }
            }
        }
    }

    public void createOrder() {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
