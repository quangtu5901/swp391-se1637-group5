package controller.order;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import Utilities.SMS;
import com.google.gson.Gson;
import entity.ListPayment;
import entity.Order;
import entity.Payment;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.LogModel;
import model.OrderModel;
import model.UserModel;

/**
 *
 * @author black
 */
@WebServlet(urlPatterns = {"/webhookHandle"})
public class payment extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet payment</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet payment at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserModel userdao = new UserModel();
        OrderModel orderdao = new OrderModel();

        BufferedReader reader = request.getReader();
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        String json = sb.toString();

        Gson gson = new Gson();
        LogModel log = new LogModel();
        ListPayment listPayment = gson.fromJson(json, ListPayment.class);
        List<Payment> data = listPayment.data;
        for (int i = 0; i < data.size(); i++) {
            int indexIdOrder = -1;
            String[] destrip = data.get(i).getDescription().split(" ");
            log.insertLog(data.get(i).toString());
            for (int j = 0; j < destrip.length; j++) {
                if (destrip[i].equalsIgnoreCase("SHOP2BANH")) {
                    indexIdOrder = j;
                }
            }
//            String[] motalist = destrip[1].trim().split("-");
            int id = getInt(destrip[indexIdOrder + 1]);
            String username = destrip[indexIdOrder + 2];
            User user = userdao.getUserByUsername(username);
            int userId = -1;
            if (user != null) {
                userId = user.getId();
            }
            if (id == -1) {
                break;
            }
            Order order = orderdao.selectOrder(id);
            if (order.getTotalPrice() == (int) data.get(i).getAmount()) {
                orderdao.updatePaymentState(id);
                try {
                    SMS.call("0943510905", "Your order have been payment successful: " + id + username + order.getTotalPrice());
                } catch (Exception ex) {
                    Logger.getLogger(payment.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (order.getTotalPrice() < data.get(i).getAmount()) {
                if (orderdao.checkRefund(user.getId()) != -1) {
                    double total = orderdao.checkRefund(user.getId()) - order.getTotalPrice() + data.get(i).getAmount();
                    if (userId != -1) {
                        orderdao.updateRefund(userId, total);
                    }
                    orderdao.updatePaymentState(id);
                }
            } else {
                if (orderdao.checkRefund(user.getId()) != -1) {
                    double total = orderdao.checkRefund(user.getId()) + data.get(i).getAmount();
                    if (userId != -1) {
                        orderdao.updateRefund(userId, total);
                    }
                }
            }
        }
    }

    private static int getInt(String so) {
        try {
            int number = Integer.parseInt(so);
            return number;
        } catch (Exception e) {
            return -1;
        }
    }

//    public static void main(String[] args) {
//        String s = "asdasd.sdas.sdsadasd";
//        String[] arr = s.split("[.]");
//        for(int i = 0; i < arr.length; i++) {
//            System.out.println(arr[i]);
//        }
//    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
