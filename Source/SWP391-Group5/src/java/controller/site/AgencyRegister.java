/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.site;

import Utilities.ENV;
import Utilities.SMTP;
import Utilities.TokenGenerator;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author black
 */
@WebServlet(name = "AgencyRegister", urlPatterns = {"/agencyRegister"})
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
        maxFileSize = 1024 * 1024 * 10, // 10 MB
        maxRequestSize = 1024 * 1024 * 100 // 100 MB
)
public class AgencyRegister extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AgencyRegister</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AgencyRegister at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("site/agencyRegister.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        HttpSession session = request.getSession();
//        User u = (User) session.getAttribute("account");
        String shopName = request.getParameter("Shopname");
        String subject = request.getParameter("subject");
        String email = request.getParameter("email");
        Part filePart = request.getPart("file");
        upload(filePart, shopName, email);
        String fileName = filePart.getSubmittedFileName();
        InputStream fileContent = filePart.getInputStream();

        // Create the email message
        Properties props = new Properties();
        props.put("mail.smtp.host", ENV.SMTP_HOST);
        props.put("mail.smtp.port", ENV.SMTP_PORT);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ENV.SMTP_ACCOUNT_EMAIL, ENV.SMTP_ACCOUNT_PASSWORD);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(ENV.SMTP_ACCOUNT_EMAIL));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ENV.SMTP_HOST_OFFICCAL));
            message.setSubject("Register to become Agency");
            message.setSentDate(new Date());

            // Attach the file to the email message
            Multipart multipart = new MimeMultipart();
            BodyPart fileBodyPart = new MimeBodyPart();
            fileBodyPart.setText("Hi, " + shopName + "! Thank you for your interest in becoming an agency. Please find attached the necessary files for your registration.");

            DataSource source = new ByteArrayDataSource(fileContent, "application/octet-stream");
            fileBodyPart.setDataHandler(new DataHandler(source));
            fileBodyPart.setFileName(fileName);
            multipart.addBodyPart(fileBodyPart);
            message.setContent(multipart);

            Transport.send(message);

            response.getWriter().println("File uploaded and sent to email successfully!");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
        response.sendRedirect("/");
    }

    protected void upload(Part filePart, String name, String email) throws IOException {
        String uploadDir = "C:/upload"; // specify your upload directory here

        File fileUploadDir = new File(uploadDir);
        if (!fileUploadDir.exists()) {
            fileUploadDir.mkdir();
        }

        String fileName = filePart.getSubmittedFileName() + "-" + name + "-" + email;
        InputStream fileContent = filePart.getInputStream();

        // save the uploaded file to the server
        File file = new File(fileUploadDir, fileName);
        OutputStream outputStream = new FileOutputStream(file);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = fileContent.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.close();
        fileContent.close();
    }
//    protected void 

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
