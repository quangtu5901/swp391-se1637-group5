/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.post;

import entity.Post;
import entity.PostComment;
import entity.PostListImage;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import model.PostModel;

/**
 *
 * @author asus
 */
@WebServlet(name = "CommentPostServlet", urlPatterns = {"/comment"})
public class CommentPostServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CommentPostServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CommentPostServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String postId_raw = request.getParameter("postId");
        int postId = Integer.parseInt(postId_raw);
        String comment = request.getParameter("txt");
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = dateFormat.format(currentDate);
        PostModel pm = new PostModel();
        HttpSession session = request.getSession();
        Post post = pm.getPostByID(postId);
        PrintWriter out = response.getWriter();
        
        User u = (User) session.getAttribute("account");
        pm.insertPostComment(1, postId, comment, formattedDate);
        List<PostComment> listComment = pm.getAllPostComment(postId);
        out.print("<h4>"+listComment.size()+"</h4>\n");
        for (PostComment postComment : listComment) {
            out.print("\n"
                    + "\n"
                    +                                    " <div class=\"comment-list left-padding\">\n"
                    + "                                    <div class=\"single-comment justify-content-between d-flex\">\n"
                    + "                                        <div class=\"user justify-content-between d-flex\">\n"
                    + "                                            <div class=\"thumb\">\n"
                    + "                                                <img src=\""+postComment.getUserId().getAvatar()+"\" alt=\"\">\n"
                    + "                                            </div>\n"
                    + "                                            <div class=\"desc\">\n"
                    + "                                                <h5><a href=\"#\">"+postComment.getUserId().getFullName()+"</a></h5>\n"
                    + "                                                <p class=\"date\">"+postComment.getDatePosted()+" </p>\n"
                    + "                                                <p class=\"comment\">\n"
                    + "                                                    "+postComment.getContent()+"\n"
                    + "                                                </p>\n"
                    + "                                            </div>\n"
                    + "                                        </div>\n"
                    + "                                        <div class=\"reply-btn\">\n"
                    + "                                            <a href=\"\" class=\"btn-reply text-uppercase\">reply</a>\n"
                    + "                                        </div>\n"
                    + "                                    </div>\n"
                    + "                                </div>\n");
        }
        
        User userCreate = post.getUserCreated();
        List<PostListImage> listImage = pm.getAllPostListImage(postId);
        
        request.setAttribute("msg", "add comment success");
        request.setAttribute("user", userCreate);
        request.setAttribute("post", post);
        request.setAttribute("listPostImage", listImage);
        request.setAttribute("listPostComment", listComment);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String postId_raw = request.getParameter("id");
        int postId = Integer.parseInt(postId_raw);
        String comment = request.getParameter("message");
        Date currentDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
        String formattedDate = dateFormat.format(currentDate);
        PostModel pm = new PostModel();
        HttpSession session = request.getSession();
        Post post = pm.getPostByID(postId);
        
        User u = (User) session.getAttribute("account");
        pm.insertPostComment(1, postId, comment, formattedDate);
        
        User userCreate = post.getUserCreated();
        List<PostListImage> listImage = pm.getAllPostListImage(postId);
        List<PostComment> listComment = pm.getAllPostComment(postId);
        
        request.setAttribute("msg", "add comment success");
        request.setAttribute("user", userCreate);
        request.setAttribute("post", post);
        request.setAttribute("listPostImage", listImage);
        request.setAttribute("listPostComment", listComment);
        
        request.getRequestDispatcher("post/single-blog.jsp").forward(request, response);
        //request.getRequestDispatcher("post/single-blog.jsp").forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
