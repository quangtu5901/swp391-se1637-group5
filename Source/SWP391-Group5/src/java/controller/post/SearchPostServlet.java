/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.post;

import entity.Post;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.PostModel;

/**
 *
 * @author asus
 */
@WebServlet(name="SearchPostServlet", urlPatterns={"/searchPost"})
public class SearchPostServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchPostServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchPostServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String search  = request.getParameter("txt");
        
        PostModel p = new PostModel();
        List<Post>  list  =  p.getPostBySearchTitle(search);
        PrintWriter out = response.getWriter();
        for (Post post : list) {
            
        
        out.println("<article class=\"row blog_item\">\n" +
"                                    <div class=\"col-md-3\">\n" +
"                                        <div class=\"blog_info text-right\">\n" +
"                                            <div class=\"post_tag\">\n" +
"                                                <a href=\"#\">Motorcycle insurance</a>\n" +
"                                                <a href=\"#\">Motobike accessories</a>\n" +
"\n" +
"\n" +
"\n" +
"                                            </div>\n" +
"                                            <ul class=\"blog_meta list\">\n" +
"\n" +
"                                                <li><a href=\"#\">"+post.getDateCreated()+"<i class=\"lnr lnr-calendar-full\"></i></a></li>\n" +
"                                                <li><a href=\"#\">"+post.getViewNumber()+"<i class=\"lnr lnr-eye\"></i></a></li>\n" +
"                                                        <jsp:useBean id=\"postModel\" class=\"model.PostModel\"/>\n" +
"                                                <li><a href=\"#\">"+p.getAllPostComment(post.getPostId()).size()+"<i class=\"lnr lnr-bubble\"></i></a></li>\n" +
"                                            </ul>\n" +
"                                        </div>\n" +
"                                    </div>\n" +
"                                    <div class=\"col-md-9\">\n" +
"                                        <div class=\"blog_post\">\n" +
"                                            <img src=\""+post.getImageBanner()+"\" alt=\"\" style=\"width:100%;\">\n" +
"                                            <div class=\"blog_details\">\n" +
"\n" +
"                                                <a href=\"" + request.getContextPath() + "/single-blog/" + post.deAccent() + "\">\n" +
"\n" +
"\n" +
"                                                    <h2>"+post.getPostTitle()+"</h2>\n" +
"                                                </a>\n" +
"                                                <p>"
        +post.getSeoContent()+ "</p>\n" +
"\n" +
"                                                <a href=\"" + request.getContextPath() + "/single-blog/" + post.deAccent() + "\" class=\"white_bg_btn\">View More</a>\n" +
"\n" +
"\n" +
"\n" +
"                                            </div>\n" +
"                                        </div>\n" +
"                                    </div>\n" +
"                                </article>\n");
        }
        out.close();
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String search =request.getParameter("search");
        PostModel postModel = new PostModel();
        List<Post>  list = postModel.getPostBySearchTitle(search);
        request.setAttribute("list_search", list);
        request.getRequestDispatcher("post/searchpost.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
