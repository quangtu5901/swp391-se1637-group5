/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.account;

import Utilities.*;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import model.UserModel;

/**
 *
 * @author black
 */
@WebServlet(name="forgot_pass", urlPatterns={"/forgot_pass"})
public class forgot_pass extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet forgot_pass</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet forgot_pass at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.getRequestDispatcher("account/forgot_pass.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            String username = request.getParameter("username");
            String captcha = request.getParameter("g-recaptcha-response");

            GoogleReCaptcha grecaptcha = new GoogleReCaptcha(ENV.GOOGLE_RECAPTCHA_SECRET_KEY);

//            if (!grecaptcha.checkCaptcha(captcha)) {
//                throw null;
//            }

            SMTP smtp = new SMTP(ENV.SMTP_HOST, ENV.SMTP_PORT, ENV.SMTP_ACCOUNT_EMAIL, ENV.SMTP_ACCOUNT_PASSWORD);
            smtp.connect();

            UserModel user = new UserModel();

            User u = user.getUserByUsername(username);

            if (u == null) {
                throw null;
            }

            String oldPassword = u.getPassword();

            HashMap<String, Object> data = new HashMap<>();
            data.put("username", u.getUsername());
            data.put("expiry", new Date().getTime() + 1000 * 60 * 5); // 5 minutes

            String text = "Vui lòng truy cập đường dẫn sau để cài đặt mật khẩu mới (hiệu lực trong 5 phút): \nhttp://" + ENV.HOST + "/reset_password?token=" + TokenGenerator.generate(data, oldPassword);

            smtp.sendMimeMessageWithThread("Group5 (No-Reply)", u.getEmail(), "[Group5] Password Recovery", text);
        } catch (Exception e) {
            doGet(request, response);
            return;
        }
        response.sendRedirect("home");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
