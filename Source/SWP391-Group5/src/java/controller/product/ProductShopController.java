/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import entity.Brand;
import entity.Category;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.BrandModel;
import model.CategoryModel;
import model.ProductModel;

/**
 *
 * @author Saka289
 */
@WebServlet(name = "ProductShopController", urlPatterns = {"/productshop/*"})
public class ProductShopController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductShopController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductShopController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CategoryModel categoryModel = new CategoryModel();
        ProductModel productModel = new ProductModel();
        BrandModel brandModel = new BrandModel();
        String path = request.getPathInfo();
        String cateId_raw = null;
        String listCateId_raw = null;
        if (path != null) {
            String[] pathName = path.split("/");
            String[] pathCategory = pathName[2].split("-");
            if (pathName[1].equals("category")) {
                listCateId_raw = pathCategory[pathCategory.length - 1];
            } else {
                cateId_raw = pathCategory[pathCategory.length - 1];
            }
        }
        String indexPage = request.getParameter("index");
        int pageTotal = 9;
        if (listCateId_raw != null) {
            int listCateId = Integer.parseInt(listCateId_raw);
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getProductByAllCategory(listCateId, 0, 15000000, null, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            List<Product> listP = productModel.getProductByAllCategory(listCateId, 0, 15000000, null, index, pageTotal);
            request.setAttribute("endPage", endPage);
            request.setAttribute("tag", index);
            request.setAttribute("listP", listP);
            session.setAttribute("listCateId", listCateId_raw);
        } else if (cateId_raw != null) {
            int cateId = Integer.parseInt(cateId_raw);
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getProductByCategory(cateId, 0, 15000000, null, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            List<Product> listP = productModel.getProductByCategory(cateId, 0, 15000000, null, index, pageTotal);
            request.setAttribute("endPage", endPage);
            request.setAttribute("tag", index);
            request.setAttribute("listP", listP);
            session.setAttribute("cateId", cateId_raw);
        } else {
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getAllProduct(0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            List<Product> listP = productModel.getAllProduct(index, pageTotal);
            request.setAttribute("endPage", endPage);
            request.setAttribute("tag", index);
            request.setAttribute("listP", listP);
        }
        List<Category> listC = categoryModel.getAllCategory();
        List<Brand> listB = brandModel.getAllBrand();
        List<Product> listP1 = productModel.getViewNumber(10);
        request.setAttribute("listP1", listP1);
        request.setAttribute("listC", listC);
        request.setAttribute("listB", listB);
        request.getRequestDispatcher("/product&category/category.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = null;
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("search")) {
            doPost_searchProduct(request, response);
        }
    }

    protected void doPost_searchProduct(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductModel productModel = new ProductModel();
        BrandModel brandModel = new BrandModel();
        CategoryModel categoryModel = new CategoryModel();
        String searchAll = request.getParameter("searchAll");
        List<Product> listSA = productModel.getSearchProductAllWeb(searchAll);
        List<Category> listC = categoryModel.getAllCategory();
        List<Brand> listB = brandModel.getAllBrand();
        request.setAttribute("listC", listC);
        request.setAttribute("listB", listB);
        request.setAttribute("listP", listSA);
        request.getRequestDispatcher("/product&category/category.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
