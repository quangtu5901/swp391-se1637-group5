/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import entity.ProListImage;
import entity.Product;
import entity.ProductComment;
import entity.ProductCommentListImage;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import static java.lang.String.format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.CategoryModel;
import model.ProListImageModel;
import model.ProductCommentModel;
import model.ProductModel;

/**
 *
 * @author Saka289
 */
@MultipartConfig(
        location = "D:/Study Materials/Code_Java_Web/SWP391_Project/swp391-se1637-group5/Source/SWP391-Group5/web/public/img/product",
        fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10, // 10MB
        maxRequestSize = 1024 * 1024 * 50)
@WebServlet(name = "ProductDetailController", urlPatterns = {"/productDetail/*"})
public class ProductDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductModel productModel = new ProductModel();
        ProListImageModel proListImageModel = new ProListImageModel();
        ProductCommentModel productCommentModel = new ProductCommentModel();
        CategoryModel categoryModel = new CategoryModel();
        String id_raw = request.getPathInfo();
        String[] stringID = null;
        if (id_raw != null) {
            stringID = id_raw.split("-");
        }
        String indexPage = request.getParameter("index");
        int productId;
        int pageTotal = 3;
        try {
            productId = Integer.parseInt(stringID[stringID.length - 1]);
            Product p = productModel.getProductDetail(productId);
            List<ProListImage> listPLI = proListImageModel.getProListImage(productId);
            List<ProductComment> listPC = productCommentModel.getProductCommentByProductId(productId, 0, 0);
            int count = 0;
            int temp = 0;
            int rating1 = 0, rating2 = 0, rating3 = 0, rating4 = 0, rating5 = 0;
            String rating = null;
            for (ProductComment pc1 : listPC) {
                temp += pc1.getRating();
                count++;
                if (pc1.getRating() == 1) {
                    rating1++;
                } else if (pc1.getRating() == 2) {
                    rating2++;
                } else if (pc1.getRating() == 3) {
                    rating3++;
                } else if (pc1.getRating() == 4) {
                    rating4++;
                } else if (pc1.getRating() == 5) {
                    rating5++;
                }
            }
            ArrayList<Integer> listRating = new ArrayList<>();
            listRating.add(rating1);
            listRating.add(rating2);
            listRating.add(rating3);
            listRating.add(rating4);
            listRating.add(rating5);
            double rating_raw = (double) temp / count;
            if (Double.isNaN(rating_raw)) {
                rating_raw = 0;
            } else {
                rating = format("%,.1f", rating_raw);
            }
            //pagination
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productCommentModel.getTotalProductComment(productId);
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            List<ProductComment> listPCPage = productCommentModel.getProductCommentByProductId(productId, index, pageTotal);
            //
            // view number
            long viewNumber = productModel.getProductDetail(productId).getBuyNumber();
            productModel.updateViewNumber(viewNumber + 1, productId);
            //related product 
            int relatedId = categoryModel.getCategoryById(productModel.getProductDetail(productId).getCategory().getCateId()).getParentId();
            List<Product> listR = productModel.getRealatedProduct(relatedId);
            request.setAttribute("totalRating", rating);
            request.setAttribute("listRating", listRating);
            request.setAttribute("numberReview", count);
            request.setAttribute("listPC", listPCPage);
            request.setAttribute("listR", listR);
            request.setAttribute("ListPLI", listPLI);
            request.setAttribute("detailP", p);
            request.setAttribute("endPage", endPage);
            request.setAttribute("tag", index);
        } catch (Exception e) {
            System.out.println(e);
        }
        request.getRequestDispatcher("/product&category/single-product.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = null;
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("insertimage")) {
            ProductCommentModel productCommentModel = new ProductCommentModel();
            ArrayList<String> listImage = new ArrayList<String>();
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("account");
            String rating_raw = request.getParameter("rating");
            String review = request.getParameter("review");
            String productId_raw = request.getParameter("productId");
            int rating, productId = 0;
            String path = "img/product/";
            rating = Integer.parseInt(rating_raw);
            productId = Integer.parseInt(productId_raw);
            try {
                Collection<Part> parts = request.getParts();
                for (Part filePart : parts) {
                    String fileName = getFileName(filePart);
                    if (fileName != null) {
                        filePart.write(fileName);
                        listImage.add(path.concat(fileName));
                    }
                }
            } catch (IOException e) {
                listImage = null;
            }
            productCommentModel.insertProductComment(user, productId, review, rating, listImage);
            doPost_ProductComment(request, response);
        } else if (action.equalsIgnoreCase("pagecomment")) {
            doPost_ProductComment(request, response);
        }
    }

    protected void doPost_ProductComment(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductCommentModel productCommentModel = new ProductCommentModel();
        String indexPage = request.getParameter("indexPage");
        String id_raw = request.getParameter("productId");
        int pageTotal = 3;
        int productId;
        productId = Integer.parseInt(id_raw);
        List<ProductComment> listPC = productCommentModel.getProductCommentByProductId(productId, 0, 0);
        int count = 0;
        int temp = 0;
        int rating1 = 0, rating2 = 0, rating3 = 0, rating4 = 0, rating5 = 0;
        String rating = null;
        for (ProductComment pc1 : listPC) {
            temp += pc1.getRating();
            count++;
            if (pc1.getRating() == 1) {
                rating1++;
            } else if (pc1.getRating() == 2) {
                rating2++;
            } else if (pc1.getRating() == 3) {
                rating3++;
            } else if (pc1.getRating() == 4) {
                rating4++;
            } else if (pc1.getRating() == 5) {
                rating5++;
            }
        }
        ArrayList<Integer> listRating = new ArrayList<>();
        listRating.add(rating1);
        listRating.add(rating2);
        listRating.add(rating3);
        listRating.add(rating4);
        listRating.add(rating5);
        double rating_raw = (double) temp / count;
        if (Double.isNaN(rating_raw)) {
            rating_raw = 0;
        } else {
            rating = format("%,.1f", rating_raw);
        }
        //pagination
        if (indexPage == null) {
            indexPage = "1";
        }
        int index = Integer.parseInt(indexPage);
        int countPage = productCommentModel.getTotalProductComment(productId);
        int endPage = countPage / pageTotal;
        if ((countPage % pageTotal) != 0) {
            endPage++;
        }
        List<ProductComment> listPCPage = productCommentModel.getProductCommentByProductId(productId, index, pageTotal);
        //pagination
        PrintWriter out = response.getWriter();
        out.print("                            <div class=\"row total_rate\">\n"
                + "                                <div class=\"col-6\">\n"
                + "                                    <div class=\"box_total\">\n"
                + "                                        <h5>Overall</h5>");
        if (rating == null) {
            out.print("<h4>0.0</h4>");
        }
        if (rating != null) {
            out.print("<h4>" + rating + "</h4>");
        }
        if (count != 0) {
            out.print("<h6>(0" + count + " Reviews)</h6>");
        }
        if (count == 0) {
            out.print("<h6>(0 Reviews)</h6>");
        }
        out.print("                                    </div>\n"
                + "                                </div>\n"
                + "                                <div class=\"col-6\">\n"
                + "                                    <div class=\"rating_list\">\n"
                + "                                        <h3>Based on " + count + " Reviews</h3>");
        for (int i = 0; i < listRating.size(); i++) {
            out.print("                                            <ul class=\"list\">\n"
                    + "                                                <li><a>" + (i + 1) + " Star <i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i><i\n"
                    + "                                                            class=\"fa fa-star\"></i><i class=\"fa fa-star\"></i> " + (listRating.get(i)) + "</a></li>\n"
                    + "                                            </ul>");
        }
        out.print("                                    </div>\n"
                + "                                </div>\n"
                + "                            </div>\n"
                + "                            <div class=\"review_list\" style=\"min-height: 720px\">");
        for (ProductComment pc : listPCPage) {
            out.print("                                    <div class=\"review_item\">\n"
                    + "                                        <div class=\"media\">\n"
                    + "                                            <div class=\"d-flex\">\n"
                    + "                                                <img src=\"" + pc.getUser().getAvatar() + "\" alt=\"\">\n"
                    + "                                            </div>\n"
                    + "                                            <div class=\"media-body\">\n"
                    + "                                                <h4>" + pc.getUser().getFullName() + "</h4>");
            for (int i = 1; i <= pc.getRating(); i++) {
                out.print("<i class=\"fa fa-star\"></i>");
            }
            for (int i = pc.getRating(); i < 5; i++) {
                out.print("<i style=\"font-family: FontAwesome;\" class=\"fa fa-star-o\"></i>");
            }
            out.print("                                            </div>\n"
                    + "                                        </div>\n"
                    + "                                        <p>" + pc.getContent() + "</p>");
            if (pc.getListImage() != null) {
                out.print("                                            <div class=\"review-comment\">\n"
                        + "                                                <div class=\"row-review\">");
                for (ProductCommentListImage pc1 : pc.getListImage()) {
                    out.print("                                                        <div class=\"column-review\">\n"
                            + "                                                            <img src=\"" + pc1.getImage() + "\" alt=\"\"  onclick=\"myFunction(this, '" + pc1.getId() + "');\">\n"
                            + "                                                        </div>");
                }
                out.print("               </div>");
                for (ProductCommentListImage pc2 : pc.getListImage()) {
                    out.print("                                                    <div class=\"container-review\">\n"
                            + "                                                        <span onclick=\"this.parentElement.style.display = 'none'\" class=\"closebtn-review\">&times;</span>\n"
                            + "                                                        <img id=\"expandedImg" + pc2.getId() + "\" style=\"width:80%\">\n"
                            + "                                                    </div>");
                }
                out.print("                                            </div>");
            }
            out.print("                  </div>");
        }
        out.print("                            </div>");
        out.print("                            <div class=\"d-flex align-items-center justify-content-center\">\n"
                + "                                <div class=\"paginationPage pagination\">");
        if (index > 1) {
            out.print("                                        <a onclick=\"loadDataPage('" + (index - 1) + "', '" + productId + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
        }
        for (int i = 1; i <= endPage; i++) {
            out.print("                                        <a onclick=\"loadDataPage('" + i + "', '" + productId + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");

        }
        if (index < endPage) {
            out.print("                                        <a onclick=\"loadDataPage('" + (index + 1) + "', '" + productId + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
        }
        out.print("                                </div>\n"
                + "                            </div>");
        out.close();
    }

    private String getFileName(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        if (!contentDisposition.contains("filename=")) {
            return null;
        }
        int beginIndex = contentDisposition.indexOf("filename=") + 10;
        int endIndex = contentDisposition.length() - 1;
        return contentDisposition.substring(beginIndex, endIndex);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
