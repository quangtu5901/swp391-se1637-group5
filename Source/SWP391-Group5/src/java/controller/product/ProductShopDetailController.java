/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.product;

import entity.Category;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.CategoryModel;
import model.ProductModel;

/**
 *
 * @author Saka289
 */
@WebServlet(name = "ProductShopDetailController", urlPatterns = {"/productshopdetail"})
public class ProductShopDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductShopDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductShopDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        PrintWriter out = response.getWriter();
        ProductModel productModel = new ProductModel();
        String cateId_raw = request.getParameter("cateId");
        String listCateId_raw = request.getParameter("listCateId");
        String listcateIdAll_raw = request.getParameter("listcateIdAll");
        String min_raw = request.getParameter("minPrice");
        String max_raw = request.getParameter("maxPrice");
        String sort = request.getParameter("sort");
        String searchP = request.getParameter("txt");
        String[] searchBrand = request.getParameterValues("brandId[]");
        String indexPage = request.getParameter("index");
        int pageTotal = 9;
        double min = 0, max = 0;
        int cateId, listCateId;
        int listcateIdAll = -1;
        int[] branId;
        int flag = 0;
        if (searchP == "") {
            searchP = null;
        }
        if (min_raw != null && max_raw != null) {
            min = Double.parseDouble(min_raw);
            max = Double.parseDouble(max_raw);
        }
        if (listcateIdAll_raw != null) {
            listcateIdAll = Integer.parseInt(listcateIdAll_raw);
        }
        if (listCateId_raw != null) {
            session.setAttribute("listCateId", listCateId_raw);
            session.removeAttribute("cateId");
        }
        if (cateId_raw != null) {
            session.setAttribute("cateId", cateId_raw);
            session.removeAttribute("listCateId");
        }
        if (listcateIdAll == 0) {
            session.removeAttribute("cateId");
            session.removeAttribute("listCateId");
            if (searchP == null && searchBrand == null) {
                flag = 1;
            }
        }
        if (session.getAttribute("cateId") == null && session.getAttribute("listCateId") == null && searchP == null && searchBrand == null) {
            flag = 1;
        }
        if ((listcateIdAll == 0 && flag == 1) || (searchP == null && flag == 1 && searchBrand == null)) {
            //pagination
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getsearchProductByAll(min, max, sort, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            //pagination
            List<Product> listSPP = productModel.getsearchProductByAll(min, max, sort, index, pageTotal);
            out.print("<section style=\"min-height: 1615px\" class=\"lattest-product-area pb-40 category-list\">\n"
                    + "                            <div class=\"row\">");
            for (Product product : listSPP) {
                out.println("                                <div class=\"col-lg-4 col-md-6\">\n"
                        + "                                    <div class=\"single-product\">\n"
                        + "                                        <div class=\"img-product\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                                <img style=\"height: 100%\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                            </a>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"product-details\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h6 style=\"font-family: monospace;height: 40px\"\">" + product.getProductName() + "</h6></a>\n"
                        + "                                            <div class=\"price\">\n"
                        + "                                                <h6>" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                                                <!--<h6 class=\"l-through\">$210.00</h6>-->\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"prd-bottom\">\n"
                        + "\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"ti-bag\"></span>\n"
                        + "                                                    <p class=\"hover-text\">add to bag</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-heart\"></span>\n"
                        + "                                                    <p class=\"hover-text\">Wishlist</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-sync\"></span>\n"
                        + "                                                    <p class=\"hover-text\">compare</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-move\"></span>\n"
                        + "                                                    <p class=\"hover-text\">view more</p>\n"
                        + "                                                </a>\n"
                        + "                                            </div>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>");
            }
            out.print("                            </div>\n"
                    + "                        </section>");

            out.print("<div class=\"filter-bar d-flex flex-wrap align-items-center\">\n"
                    + "                            <div class=\"sorting mr-auto\"></div>\n"
                    + "                            <div class=\"pagination\" style=\"cursor: pointer\">");
            if (index > 1) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index - 1) + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
            }
            for (int i = 1; i <= endPage; i++) {
                out.print("                                        <a onclick=\"loadDataPageShop('" + i + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");
            }
            if (index < endPage) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index + 1) + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
            }
            out.print("                            </div>\n"
                    + "                        </div>");
            out.close();
        }
        if (cateId_raw != null || (session.getAttribute("cateId") != null && searchP == null)) {
            cateId = Integer.parseInt((String) session.getAttribute("cateId"));
            //pagination
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getProductByCategory(cateId, min, max, sort, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            //pagination
            List<Product> ListP = productModel.getProductByCategory(cateId, min, max, sort, index, pageTotal);
            out.print("<section style=\"min-height: 1615px\" class=\"lattest-product-area pb-40 category-list\">\n"
                    + "                            <div class=\"row\">");
            for (Product product : ListP) {
                out.println("                                <div class=\"col-lg-4 col-md-6\">\n"
                        + "                                    <div class=\"single-product\">\n"
                        + "                                        <div class=\"img-product\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                                <img style=\"height: 100%\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                            </a>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"product-details\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h6 style=\"font-family: monospace;height: 40px\"\">" + product.getProductName() + "</h6></a>\n"
                        + "                                            <div class=\"price\">\n"
                        + "                                                <h6>" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                                                <!--<h6 class=\"l-through\">$210.00</h6>-->\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"prd-bottom\">\n"
                        + "\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"ti-bag\"></span>\n"
                        + "                                                    <p class=\"hover-text\">add to bag</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-heart\"></span>\n"
                        + "                                                    <p class=\"hover-text\">Wishlist</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-sync\"></span>\n"
                        + "                                                    <p class=\"hover-text\">compare</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-move\"></span>\n"
                        + "                                                    <p class=\"hover-text\">view more</p>\n"
                        + "                                                </a>\n"
                        + "                                            </div>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>");
            }
            out.print("                            </div>\n"
                    + "                        </section>");

            out.print("<div class=\"filter-bar d-flex flex-wrap align-items-center\">\n"
                    + "                            <div class=\"sorting mr-auto\"></div>\n"
                    + "                            <div class=\"pagination\" style=\"cursor: pointer\">");
            if (index > 1) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index - 1) + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
            }
            for (int i = 1; i <= endPage; i++) {
                out.print("                                        <a onclick=\"loadDataPageShop('" + i + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");
            }
            if (index < endPage) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index + 1) + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
            }
            out.print("                            </div>\n"
                    + "                        </div>");
            out.close();
        }
        if (listCateId_raw != null || (session.getAttribute("listCateId") != null && searchP == null)) {
            listCateId = Integer.parseInt((String) session.getAttribute("listCateId"));
            //pagination
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getProductByAllCategory(listCateId, min, max, sort, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            //pagination
            List<Product> ListP1 = productModel.getProductByAllCategory(listCateId, min, max, sort, index, pageTotal);
            out.print("<section style=\"min-height: 1615px\" class=\"lattest-product-area pb-40 category-list\">\n"
                    + "                            <div class=\"row\">");
            for (Product product : ListP1) {
                out.println("                                <div class=\"col-lg-4 col-md-6\">\n"
                        + "                                    <div class=\"single-product\">\n"
                        + "                                        <div class=\"img-product\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                                <img style=\"height: 100%\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                            </a>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"product-details\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h6 style=\"font-family: monospace;height: 40px\"\">" + product.getProductName() + "</h6></a>\n"
                        + "                                            <div class=\"price\">\n"
                        + "                                                <h6>" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                                                <!--<h6 class=\"l-through\">$210.00</h6>-->\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"prd-bottom\">\n"
                        + "\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"ti-bag\"></span>\n"
                        + "                                                    <p class=\"hover-text\">add to bag</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-heart\"></span>\n"
                        + "                                                    <p class=\"hover-text\">Wishlist</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-sync\"></span>\n"
                        + "                                                    <p class=\"hover-text\">compare</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-move\"></span>\n"
                        + "                                                    <p class=\"hover-text\">view more</p>\n"
                        + "                                                </a>\n"
                        + "                                            </div>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>");
            }
            out.print("                            </div>\n"
                    + "                        </section>");

            out.print("<div class=\"filter-bar d-flex flex-wrap align-items-center\">\n"
                    + "                            <div class=\"sorting mr-auto\"></div>\n"
                    + "                            <div class=\"pagination\" style=\"cursor: pointer\">");
            if (index > 1) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index - 1) + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
            }
            for (int i = 1; i <= endPage; i++) {
                out.print("                                        <a onclick=\"loadDataPageShop('" + i + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");
            }
            if (index < endPage) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index + 1) + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
            }
            out.print("                            </div>\n"
                    + "                        </div>");
            out.close();
        }
        if (searchP != null) {
            //pagination
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getsearchProduct(searchP, searchP, min, max, sort, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            //pagination
            List<Product> listSP = productModel.getsearchProduct(searchP, searchP, min, max, sort, index, pageTotal);
            out.print("<section style=\"min-height: 1615px\" class=\"lattest-product-area pb-40 category-list\">\n"
                    + "                            <div class=\"row\">");
            for (Product product : listSP) {
                out.println("                                <div class=\"col-lg-4 col-md-6\">\n"
                        + "                                    <div class=\"single-product\">\n"
                        + "                                        <div class=\"img-product\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                                <img style=\"height: 100%\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                            </a>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"product-details\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h6 style=\"font-family: monospace;height: 40px\"\">" + product.getProductName() + "</h6></a>\n"
                        + "                                            <div class=\"price\">\n"
                        + "                                                <h6>" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                                                <!--<h6 class=\"l-through\">$210.00</h6>-->\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"prd-bottom\">\n"
                        + "\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"ti-bag\"></span>\n"
                        + "                                                    <p class=\"hover-text\">add to bag</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-heart\"></span>\n"
                        + "                                                    <p class=\"hover-text\">Wishlist</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-sync\"></span>\n"
                        + "                                                    <p class=\"hover-text\">compare</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-move\"></span>\n"
                        + "                                                    <p class=\"hover-text\">view more</p>\n"
                        + "                                                </a>\n"
                        + "                                            </div>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>");
            }
            out.print("                            </div>\n"
                    + "                        </section>");

            out.print("<div class=\"filter-bar d-flex flex-wrap align-items-center\">\n"
                    + "                            <div class=\"sorting mr-auto\"></div>\n"
                    + "                            <div class=\"pagination\" style=\"cursor: pointer\">");
            if (index > 1) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index - 1) + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
            }
            for (int i = 1; i <= endPage; i++) {
                out.print("                                        <a onclick=\"loadDataPageShop('" + i + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");
            }
            if (index < endPage) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index + 1) + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
            }
            out.print("                            </div>\n"
                    + "                        </div>");
            out.close();
        }
        if (searchBrand != null) {
            branId = new int[searchBrand.length];
            for (int i = 0; i < branId.length; i++) {
                branId[i] = Integer.parseInt(searchBrand[i]);
            }
            if (indexPage == null) {
                indexPage = "1";
            }
            int index = Integer.parseInt(indexPage);
            int countPage = productModel.getProductByBrand(branId, min, max, sort, 0, 0).size();
            int endPage = countPage / pageTotal;
            if ((countPage % pageTotal) != 0) {
                endPage++;
            }
            //pagination
            List<Product> listB = productModel.getProductByBrand(branId, min, max, sort, index, pageTotal);
            out.print("<section style=\"min-height: 1615px\" class=\"lattest-product-area pb-40 category-list\">\n"
                    + "                            <div class=\"row\">");
            for (Product product : listB) {
                out.println("                                <div class=\"col-lg-4 col-md-6\">\n"
                        + "                                    <div class=\"single-product\">\n"
                        + "                                        <div class=\"img-product\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\">\n"
                        + "                                                <img style=\"height: 100%\" class=\"img-fluid\" src=\"" + product.getImage() + "\" alt=\"Error\">\n"
                        + "                                            </a>\n"
                        + "                                        </div>\n"
                        + "                                        <div class=\"product-details\">\n"
                        + "                                            <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\"><h6 style=\"font-family: monospace;height: 40px\"\">" + product.getProductName() + "</h6></a>\n"
                        + "                                            <div class=\"price\">\n"
                        + "                                                <h6>" + product.formatPrice(product.getPrice()) + "</h6>\n"
                        + "                                                <!--<h6 class=\"l-through\">$210.00</h6>-->\n"
                        + "                                            </div>\n"
                        + "                                            <div class=\"prd-bottom\">\n"
                        + "\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"ti-bag\"></span>\n"
                        + "                                                    <p class=\"hover-text\">add to bag</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-heart\"></span>\n"
                        + "                                                    <p class=\"hover-text\">Wishlist</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-sync\"></span>\n"
                        + "                                                    <p class=\"hover-text\">compare</p>\n"
                        + "                                                </a>\n"
                        + "                                                <a href=\"" + request.getContextPath() + "/productDetail/" + product.deAccent() + "\" class=\"social-info\">\n"
                        + "                                                    <span class=\"lnr lnr-move\"></span>\n"
                        + "                                                    <p class=\"hover-text\">view more</p>\n"
                        + "                                                </a>\n"
                        + "                                            </div>\n"
                        + "                                        </div>\n"
                        + "                                    </div>\n"
                        + "                                </div>");
            }
            out.print("                            </div>\n"
                    + "                        </section>");

            out.print("<div class=\"filter-bar d-flex flex-wrap align-items-center\">\n"
                    + "                            <div class=\"sorting mr-auto\"></div>\n"
                    + "                            <div class=\"pagination\" style=\"cursor: pointer\">");
            if (index > 1) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index - 1) + "')\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>");
            }
            for (int i = 1; i <= endPage; i++) {
                out.print("                                        <a onclick=\"loadDataPageShop('" + i + "')\" " + ((index == i) ? "class=\"active\"" : "") + ">" + i + "</a>");
            }
            if (index < endPage) {
                out.print("                                    <a onclick=\"loadDataPageShop('" + (index + 1) + "')\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>");
            }
            out.print("                            </div>\n"
                    + "                        </div>");
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = null;
        if (request.getParameter("action") != null) {
            action = request.getParameter("action");
        }
        if (action.equalsIgnoreCase("searchProduct")) {
//            doPost_searchProduct(request, response);
        } else if (action.equalsIgnoreCase("searchPrice")) {
//            doPost_searchPrice(request, response);
        } else if (action.equalsIgnoreCase("update")) {
//            doPost_Update(request, response);
        } else if (action.equalsIgnoreCase("create")) {
//            doPost_Create(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
