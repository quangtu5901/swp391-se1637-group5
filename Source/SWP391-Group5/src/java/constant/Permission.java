/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package constant;

/**
 *
 * @author black
 */
public class Permission {

    public static String admin = "admin";
    public static String user = "user";
    public static String agency = "Agency";
    public static String productManagement = "Product Management"; 
    public static String postManagement = "Post Management";
    public static String orderManagement = "Order Management";
    public static String userManagement = "User Management";
    public static String staffManagement = "Staff Management";
}
