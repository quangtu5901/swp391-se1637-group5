/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.ShowOrderDetail;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class OrderDetailModel extends DBContext{
    public List<ShowOrderDetail> getListShowOrderDetail(int orderId) {
        List<ShowOrderDetail> list = new ArrayList<ShowOrderDetail>();
        String sql = " select u.fullName, u.phoneNumber, u.email, p.productName, p.price, o2.quantity from "
                + "(([Order] o join [User] u on o.userId = u.id) join [Oder Detail] o2 on o.orderID = o2.orderId) "
                + "join [Product] p on o2.productId = p.productId where o2.orderId = " + orderId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String userName = rs.getString(1);
                String phoneNumber = rs.getString(2);
                String email = rs.getString(3);
                String productString = rs.getString(4);
                double price = rs.getDouble(5);
                int quantity = rs.getInt(6);
                ShowOrderDetail orderDetail = new ShowOrderDetail(orderId, userName, phoneNumber, email, productString, price, quantity);
                list.add(orderDetail);
            }
        } catch (Exception e) {
            System.out.println("Get list orderDetail error: " + e.getMessage());
        }
        return list;
    }
    
}
