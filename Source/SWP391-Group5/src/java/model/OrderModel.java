/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import entity.Cart;
import entity.Item;
import entity.Order;
import entity.User;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpSession;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author black
 */
public class OrderModel extends DBContext {

    public int addOrder(int userId, Cart cart, String name_recive, String note, String address, String phoneNumber) {
        LocalDate curDate = LocalDate.now();
        int oid = -1;
        String date = curDate.toString();
        try {
            //add order
            String sql = "INSERT INTO [dbo].[Order]\n"
                    + "           ([orderName]\n"
                    + "           ,[userId]\n"
                    + "           ,[dateCreated]\n"
                    + "           ,[totalPrice]\n"
                    + "           ,[phoneNumber]\n"
                    + "           ,[status]\n"
                    + "           ,[payment_status]\n"
                    + "           ,[note]\n"
                    + "           ,[address])\n"
                    + "     VALUES(?, ?, ?, ?, ?, 0, 0, ?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name_recive);
            st.setInt(2, userId);
            st.setString(3, date);
            st.setDouble(4, cart.getTotalMoney());
            st.setString(5, phoneNumber);
            st.setString(6, note);
            st.setString(7, address);
            st.executeUpdate();

            //lay id cua order vua add
            String sql1 = "select top 1 orderID from [Order] order by orderID desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            //add bang OrderDetail
            if (rs.next()) {
                return rs.getInt("orderID");
//                for (Item i : cart.getItems()) {
//                    String sql2 = "insert into [Oder Detail] values(?,?,?,?)";
//                    PreparedStatement st2 = connection.prepareStatement(sql2);
//                    st2.setInt(1, oid);
//                    st2.setInt(2, i.getProduct().getProductId());
//                    st2.setDouble(3, i.getPrice());
//                    st2.setInt(4, i.getQuantity());
//                    st2.executeUpdate();
//                }
            }
            //cap nhat lai so luong san pham
            String sql3 = "update Product set quantity=quantity-? where id=?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getProductId());
                st3.executeUpdate();
            }
        } catch (SQLException e) {

        }
        return oid;
    }

    public List<Order> getListOrder() {
        List<Order> list = new ArrayList<Order>();
        String sql = "SELECT [orderID], [userId],[dateCreated],[totalPrice],[status],[note],[address], [payment_status], [orderName]"
                + "  FROM [dbo].[Order]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int orderId = rs.getInt(1);
                int userId = rs.getInt(2);
                String dateCreated = rs.getString(3);
                double totalPrice = rs.getDouble(4);
                int status = rs.getInt(5);
                String note = rs.getString(6);
                String address = rs.getString(7);
                Order order = new Order(orderId, userId, dateCreated, totalPrice, status, note, address);
                order.setPaymentStatus(rs.getInt("payment_status"));
                order.setOrderName(rs.getString("orderName"));
                list.add(order);
            }
        } catch (Exception e) {
            System.out.println("Get list category error: " + e.getMessage());
        }
        return list;
    }
    
    public static void main(String[] args) {
        OrderModel ob = new OrderModel();
        List<Order> list = ob.orderHistoryFromTo("2023-09-08", "2023/12/9");
        //List<Order> list = ob.getListOrder();
        for (Order order : list) {
            System.out.println(order.getId());
        }
    }
    public List<Order> orderHistoryFromTo(String dateFrom,String dateTo) {
        String sql = "SELECT [orderID]\n" +
"      ,[orderName]\n" +
"      ,[userId]\n" +
"      ,[dateCreated]\n" +
"      ,[totalPrice]\n" +
"      ,[phoneNumber]\n" +
"      ,[status]\n" +
"      ,[payment_status]\n" +
"      ,[note]\n" +
"      ,[address]\n" +
"  FROM [dbo].[Order] where dateCreated between ? and ?";
        List<Order> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, dateFrom);
            st.setString(2, dateTo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("orderID"));
                order.setOrderName(rs.getString("orderName"));
                order.setUser_id(rs.getInt("userId"));
                order.setDateCreated(rs.getString("dateCreated"));
                order.setTotalPrice(rs.getDouble("totalPrice"));
                order.setPhoneNumber(rs.getString("phoneNumber"));
                order.setStatus(rs.getInt("status"));
                order.setPaymentStatus(rs.getInt("payment_status"));
                order.setNote(rs.getString("note"));
                order.setAddress(rs.getString("address"));
                list.add(order);
            }
        } catch (Exception e) {
        }
        return list;

    }

    public boolean approveOrder(int orderId, int status) {
        String sql = "UPDATE [dbo].[Order]\n"
                + "   SET [status] = ?\n"
                + "     \n"
                + " WHERE orderID = " + orderId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public Order selectOrder(int orderId) {
        String sql = "select * from [Order] where orderID = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, orderId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Order order = new Order();
                order.setId(orderId);
                order.setOrderName(rs.getString("orderName"));
                order.setUser_id(rs.getInt("userId"));
                order.setDateCreated(rs.getString("dateCreated"));
                order.setTotalPrice(rs.getDouble("totalPrice"));
                order.setPhoneNumber(rs.getString("phoneNumber"));
                order.setStatus(rs.getInt("status"));
                order.setPaymentStatus(rs.getInt("payment_status"));
                order.setNote(rs.getString("note"));
                order.setAddress(rs.getString("address"));
                return order;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Order> orderHistory(int userid) {
        String sql = "select * from [Order] where userId = ?";
        List<Order> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("orderID"));
                order.setOrderName(rs.getString("orderName"));
                order.setUser_id(rs.getInt("userId"));
                order.setDateCreated(rs.getString("dateCreated"));
                order.setTotalPrice(rs.getDouble("totalPrice"));
                order.setPhoneNumber(rs.getString("phoneNumber"));
                order.setStatus(rs.getInt("status"));
                order.setPaymentStatus(rs.getInt("payment_status"));
                order.setNote(rs.getString("note"));
                order.setAddress(rs.getString("address"));
                list.add(order);
            }
        } catch (Exception e) {
        }
        return list;

    }
    public List<Order> ListOrder() {
        String sql = "select * from [Order]";
        List<Order> list = new ArrayList<>();

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setId(rs.getInt("orderID"));
                order.setOrderName(rs.getString("orderName"));
                order.setUser_id(rs.getInt("userId"));
                order.setDateCreated(rs.getString("dateCreated"));
                order.setTotalPrice(rs.getDouble("totalPrice"));
                order.setPhoneNumber(rs.getString("phoneNumber"));
                order.setStatus(rs.getInt("status"));
                order.setPaymentStatus(rs.getInt("payment_status"));
                order.setNote(rs.getString("note"));
                order.setAddress(rs.getString("address"));
                list.add(order);
            }
        } catch (Exception e) {
        }
        return list;

    }

    public void updatePaymentState(int orderID) {
        String sql = "update [Order] set payment_status = 1 where orderID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, orderID);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void insertRefund(int id, double totalAmount) {
        String sql = "INSERT INTO [dbo].[Refund]\n"
                + "           ([userId]\n"
                + "           ,[totalAmount])\n"
                + "     VALUES\n"
                + "           (?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setDouble(2, totalAmount);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateRefund(int id, double totalAmount) {
        String sql = "UPDATE [dbo].[Refund]\n"
                + "   SET [totalAmount] = ?\n"
                + " WHERE [userId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, totalAmount);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public double checkRefund(int id) {
        String sql = "select [totalAmount] from Refund where [userId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble("totalAmount");
            }
        } catch (Exception e) {
        }
        return -1;
    }
    
    
    

   
}
