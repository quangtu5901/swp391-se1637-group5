/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Advertiser;
import entity.Banner;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class BannerModel extends DBContext {

    public List<Advertiser> getAllAdver() {
        List<Advertiser> list = new ArrayList<>();
        String sql = "SELECT id, image FROM [advertiser]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String img = rs.getString(2);
                Advertiser ad = new Advertiser(id, img, "");
                list.add(ad);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Banner> getAllBanner() {
        List<Banner> list = new ArrayList<>();
        String sql = "SELECT id, title, description, image, link FROM [Banner]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String desc = rs.getString(3);
                String img = rs.getString(4);
                String link = rs.getString(5);
                Banner ad = new Banner(id, title, desc, img, link);
                list.add(ad);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Banner getBannerById(int id) {
        Banner banner = new Banner();
        String sql = "SELECT id, title, description, image, link FROM [Banner] where id =" + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
                String title = rs.getString(2);
                String desc = rs.getString(3);
                String img = rs.getString(4);
                String link = rs.getString(5);
                banner = new Banner(id, title, desc, img, link);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return banner;
    }

    public void updateImageBanner(int number, String imageBanner) {
        String sql = "UPDATE [dbo].[advertiser]\n"
                + "   SET [image] = ?\n"
                + " WHERE id = " + number;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, imageBanner);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void createBanner(String title, String des, String image, String link) {
        String sql = "insert into Banner values (?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, des);
            st.setString(3, image);
            st.setString(4, link);
            st.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateBanner(int id, String title, String des, String image, String link) {
        String sql = "UPDATE [dbo].[Banner]\n"
                + "   SET [title] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[link] = ?\n"
                + " WHERE id = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, des);
            st.setString(3, image);
            st.setString(4, link);
            st.executeQuery();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}
