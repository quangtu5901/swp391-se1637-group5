/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Insurance;
import entity.Order;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class InsuranceModel extends DBContext {

    public List<Insurance> getListInsurance() {
        List<Insurance> list = new ArrayList<Insurance>();
        String sql = "  select id,userid, ownerName, plateNumber,insurYear, dateFrom, dateTo, mandatoryInsur, optionInsur, vehicleType, totalPrice from Insurance";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int insurId = rs.getInt(1);
                int userId = rs.getInt(2);
                String ownerName = rs.getString(3);
                String plateNumber = rs.getString(4);
                int insurYear = rs.getInt(5);
                String dateFrom = rs.getString(6);
                String dateTo = rs.getString(7);
                boolean mandatoryInsur = rs.getBoolean(8);
                boolean optionInsur = rs.getBoolean(9);
                String vehicleType = rs.getString(10);
                double totalPrice = rs.getDouble(11);
                Insurance in = new Insurance(insurId, userId, ownerName, plateNumber, insurYear, dateFrom, dateTo, mandatoryInsur, optionInsur, vehicleType, totalPrice);
                list.add(in);
            }
        } catch (Exception e) {
            System.out.println("Get list insur error: " + e.getMessage());
        }
        return list;
    }

    public Insurance getInsuranceById(int id) {
        Insurance in = new Insurance();
        String sql = "  select id,userid, ownerName, plateNumber,insurYear, dateFrom, dateTo, mandatoryInsur, optionInsur, vehicleType, totalPrice from Insurance where id=" + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int insurId = rs.getInt(1);
                int userId = rs.getInt(2);
                String ownerName = rs.getString(3);
                String plateNumber = rs.getString(4);
                int insurYear = rs.getInt(5);
                String dateFrom = rs.getString(6);
                String dateTo = rs.getString(7);
                boolean mandatoryInsur = rs.getBoolean(8);
                boolean optionInsur = rs.getBoolean(9);
                String vehicleType = rs.getString(10);
                double totalPrice = rs.getDouble(11);
                in = new Insurance(insurId, userId, ownerName, plateNumber, insurYear, dateFrom, dateTo, mandatoryInsur, optionInsur, vehicleType, totalPrice);
            }
        } catch (Exception e) {
            System.out.println("Get list insur error: " + e.getMessage());
        }
        return in;
    }

    public Boolean insertInsurance(int year, String vehicleType, String ownerName, String plateNumber, String dateFrom, String dateTo, boolean mandatoryInsur, boolean optionInsur, double totalPrice, int userid) {
        String sql = "INSERT INTO [dbo].[Insurance]\n"
                + "           ([userid]\n"
                + "           ,[ownerName]\n"
                + "           ,[plateNumber]\n"
                + "           ,[insurYear]\n"
                + "           ,[dateFrom]\n"
                + "           ,[dateTo]\n"
                + "           ,[mandatoryInsur]\n"
                + "           ,[optionInsur]\n"
                + "           ,[vehicleType]\n"
                + "           ,[totalPrice])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)\n";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userid);
            st.setString(2, ownerName);
            st.setString(3, plateNumber);
            st.setInt(4, year);
            st.setString(5, dateFrom);
            st.setString(6, dateTo);
            st.setBoolean(7, mandatoryInsur);
            st.setBoolean(8, optionInsur);
            st.setString(9, vehicleType);
            st.setDouble(10, totalPrice);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }
}
