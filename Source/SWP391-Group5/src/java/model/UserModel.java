/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Utilities.Crypto;
import constant.Permission;
import entity.Admin;
import entity.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author black
 */
public class UserModel extends DBContext {

    public int checkUser(String username, String password) {
        password = Crypto.SHA256(password);

        String sql = "SELECT [id] FROM [User] WHERE [username] = ? AND [password] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }

        return -1;
    }

    public User getUser(String username, String password) {
        password = Crypto.SHA256(password);
        String sql = "SELECT * FROM [User] WHERE [username] = ? AND [password] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setRoleId(rs.getInt("roleID"));
                u.setUsername(username);
                u.setPassword(password);
                u.setEmail(rs.getString("email"));
                u.setPhoneNumber(rs.getString("phoneNumber"));
                u.setAddress(rs.getString("phoneNumber"));
                u.setFullName(rs.getString("fullName"));
                u.setCity(rs.getString("city"));
                u.setDistrict(rs.getString("district"));
                u.setWard(rs.getString("ward"));
                u.setAvatar(rs.getString("avatar"));
                u.setStatus(rs.getInt("status"));
                return u;
            }

            return null;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    public Boolean isUsernameExist(String username) {
        String sql = "SELECT [id] FROM [User] WHERE [username] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return false;
    }

    public User getUserByUsername(String username) {
        String sql = "SELECT * FROM [User] WHERE [username] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setRoleId(rs.getInt("roleID"));
                u.setUsername(username);
                u.setPassword(rs.getString("password"));
                u.setEmail(rs.getString("email"));
                u.setPhoneNumber(rs.getString("phoneNumber"));
                u.setAddress(rs.getString("phoneNumber"));
                u.setFullName(rs.getString("fullName"));
                u.setCity(rs.getString("city"));
                u.setDistrict(rs.getString("district"));
                u.setWard(rs.getString("ward"));
                u.setAvatar(rs.getString("avatar"));
                return u;
            }

            return null;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    public Boolean insertUser(String username, String password, String email, String phone) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([roleID]\n"
                + "           ,[username]\n"
                + "           ,[password]\n"
                + "           ,[email]\n"
                + "           ,[phoneNumber]\n"
                + "           ,[status]\n"
                + "           )\n"
                + "     VALUES\n"
                + "           (2, ?, ?, ?,?, 1)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, Crypto.SHA256(password));
            st.setString(3, email);
            st.setString(4, phone);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public Boolean updatePassword(String username, String password) {
        password = Crypto.SHA256(password);
        String sql = "select * from [User]\n"
                + "UPDATE [dbo].[User] SET [password] = ? WHERE username = ? ";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, password);
            st.setString(2, username);
            st.executeUpdate();
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public Boolean update(User user) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [roleID] = ?\n"
                + "      ,[username] = ?\n"
                + "      ,[email] = ?\n"
                + "      ,[phoneNumber] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[fullName] = ?\n"
                + "      ,[city] = ?\n"
                + "      ,[district] = ?\n"
                + "      ,[ward] = ?\n"
                + "      ,[avatar] = ?\n"
                + "      ,[status] = ?\n"
                + " WHERE [username] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user.getRoleId());
            st.setString(2, user.getUsername());
            st.setString(3, user.getEmail());
            st.setString(4, user.getPhoneNumber());
            st.setString(5, user.getAddress());
            st.setString(6, user.getFullName());
            st.setString(7, user.getCity());
            st.setString(8, user.getDistrict());
            st.setString(9, user.getWard());
            st.setString(10, user.getAvatar());
            st.setInt(11, user.getStatus());
            st.setString(12, user.getUsername());
            st.executeUpdate();
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean updateImage(User u, String img) {
        String sql = "UPDATE [dbo].[User]\n"
                + "   SET [avatar] = ?\n"
                + " WHERE [username] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, img);
            st.setString(2, u.getUsername());

            st.executeUpdate();
        } catch (Exception e) {
            return false;
        }

        return true;

    }

    public User getUserByEmail(String email) {
        String sql = "SELECT [username], [email] FROM [User] WHERE [email] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User(rs.getString("username"), email);
                return u;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int createUser(User u) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([roleID]\n"
                + "           ,[username]\n"
                + "           ,[email]\n"
                + "           ,[fullName]\n"
                + "           ,[avatar])\n"
                + "     VALUES (2, ?, ?, ?, ?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u.getEmail());
            st.setString(2, u.getEmail());
            st.setString(3, u.getFullName());
            st.setString(4, u.getAvatar());

            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public User getUserById(int userId) {
        String sql = "select * from [User]\n"
                + "where id = " + userId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setRoleId(rs.getInt("roleID"));
                u.setUsername(rs.getString("username"));
                u.setPassword(rs.getString("password"));
                u.setEmail(rs.getString("email"));
                u.setStatus(rs.getInt("status"));
                u.setPhoneNumber(rs.getString("phoneNumber"));
                u.setAddress(rs.getString("address"));
                u.setFullName(rs.getString("fullName"));
                u.setCity(rs.getString("city"));
                u.setDistrict(rs.getString("district"));
                u.setWard(rs.getString("ward"));
                u.setAvatar(rs.getString("avatar"));
                return u;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Admin isManager(String username, String password) {
        password = Crypto.SHA256(password);

        String sql = "SELECT [id], [fullname], [username], [email], [phone], [roleId], [status] FROM [Admin] WHERE [username] = ? AND [password] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
//                Admin ad = new Admin(rs.getInt("id"), rs.getString("fullname"), rs.getString("username"), rs.getString("email"), rs.getString("phone"), rs.getInt("roleId"));
                Admin ad = new Admin(rs.getInt("id"), rs.getString("fullname"), rs.getString("username"), rs.getString("email"), rs.getString("phone"), rs.getInt("roleId"), rs.getInt("status"));
                return ad;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public List<User> getAllUser() {
        String sql = "select id, roleID, fullName, username, email, phoneNumber, avatar, status from [User]";
        List<User> listUser = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                User u = new User();
                u.setId(rs.getInt("id"));
                u.setRoleId(rs.getInt("roleID"));
                u.setUsername(rs.getString("username"));
                u.setFullName(rs.getString("fullName"));
                u.setEmail(rs.getString("email"));
                u.setPhoneNumber(rs.getString("phoneNumber"));
                u.setAvatar(rs.getString("avatar"));
                u.setStatus(rs.getInt("status"));
                listUser.add(u);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listUser;

    }

    public void createUser(int role, String username, String pass, String email, int status, String phone, String address, String fullname, String city, String district, String ward, String avatar) {
        String sql = "INSERT INTO [dbo].[User]\n"
                + "           ([roleID]\n"
                + "           ,[username]\n"
                + "           ,[password]\n"
                + "           ,[email]\n"
                + "           ,[status]\n"
                + "           ,[phoneNumber]\n"
                + "           ,[address]\n"
                + "           ,[fullName]\n"
                + "           ,[city]\n"
                + "           ,[district]\n"
                + "           ,[ward]\n"
                + "           ,[avatar])\n"
                + "     VALUES\n"
                + "           (?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, role);
            st.setString(2, username);
            st.setString(3, pass);
            st.setString(4, email);
            st.setInt(5, status);
            st.setString(6, phone);
            st.setString(7, address);
            st.setString(8, fullname);
            st.setString(9, city);
            st.setString(10, district);
            st.setString(11, ward);
            st.setString(12, avatar);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<User> getAgency() {
        String sql = "select a.id, a.roleID, a.fullName, a.username, a.email, a.phoneNumber, a.avatar,a.status, b.roleName roleName from [User] a join [Role] b on a.roleID = b.roleID";
        List<User> listUser = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if (rs.getString("roleName").equalsIgnoreCase(Permission.agency)) {
                    User u = new User();
                    u.setId(rs.getInt("id"));
                    u.setRoleId(rs.getInt("roleID"));
                    u.setUsername(rs.getString("username"));
                    u.setFullName(rs.getString("fullName"));
                    u.setEmail(rs.getString("email"));
                    u.setPhoneNumber(rs.getString("phoneNumber"));
                    u.setAvatar(rs.getString("avatar"));
                    u.setStatus(rs.getInt("status"));
                    listUser.add(u);
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listUser;

    }

    public static void main(String[] args) {
        UserModel u = new UserModel();
        User u1 = u.getUserById(1);
        u.update(u1);
    }
}
