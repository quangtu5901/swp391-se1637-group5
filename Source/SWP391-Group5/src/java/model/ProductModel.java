/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Category;
import entity.ProListImage;
import entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Saka289
 */
public class ProductModel extends DBContext {

    public List<Product> getAllProduct(int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        if (index == 0 && nextPage == 0) {
            sql = "select * from Product";
        } else {
            sql = "select * from Product\n"
                    + "order by productId\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (index != 0 && nextPage != 0) {
                st.setInt(1, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Product getProductDetail(int productId) {
        String sql = "select * from Product\n"
                + "where productId = " + productId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                return product;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Product> getProductByCategory(int cateId, double priceMin, double priceMax, String sort, int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        String sortOption = "order by productId";
        if (sort != null) {
            if (sort.equals("sort1")) {
                sortOption = "order by price asc";
            } else if (sort.equals("sort2")) {
                sortOption = "order by price desc";
            } else if (sort.equals("sort3")) {
                sortOption = "order by productName asc";
            } else if (sort.equals("sort4")) {
                sortOption = "order by productName desc";
            }
        }
        if (index == 0 && nextPage == 0) {
            sql = "select productId,cateId,brandId,productName,price,discount,description,image,\n"
                    + "quantity,buyNumber,userCreatedId,userModifiedId,\n"
                    + "dateCreated,dateModified,productStatus from Product \n"
                    + "where cateid = ? and price between " + priceMin + " and  " + priceMax;
        } else {
            sql = "select productId,cateId,brandId,productName,price,discount,description,image,\n"
                    + "quantity,buyNumber,userCreatedId,userModifiedId,\n"
                    + "dateCreated,dateModified,productStatus from Product \n"
                    + "where cateid = ? and price between " + priceMin + " and  " + priceMax + "\n"
                    + sortOption + "\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateId);
            if (index != 0 && nextPage != 0) {
                st.setInt(2, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByBrand(int[] brandId, double priceMin, double priceMax, String sort, int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        String stringBrandID = "";
        String sortOption = "order by productId";
        if (sort != null) {
            if (sort.equals("sort1")) {
                sortOption = "order by price asc";
            } else if (sort.equals("sort2")) {
                sortOption = "order by price desc";
            } else if (sort.equals("sort3")) {
                sortOption = "order by productName asc";
            } else if (sort.equals("sort4")) {
                sortOption = "order by productName desc";
            }
        }
        if (brandId != null) {
            for (int i = 0; i < brandId.length; i++) {
                if (i < (brandId.length - 1)) {
                    stringBrandID += brandId[i] + ",";
                } else {
                    stringBrandID += brandId[i];
                }
            }
        }
        if (index == 0 && nextPage == 0) {
            sql = "select productId,cateId,brandId,productName,price,discount,description,image,quantity,buyNumber,userCreatedId,userModifiedId,\n"
                    + "dateCreated,dateModified,productStatus from Product\n"
                    + "where brandId in (" + stringBrandID + ") and price between " + priceMin + " and  " + priceMax;
        } else {
            sql = "select productId,cateId,brandId,productName,price,discount,description,image,quantity,buyNumber,userCreatedId,userModifiedId,\n"
                    + "dateCreated,dateModified,productStatus from Product\n"
                    + "where brandId in (" + stringBrandID + ") and price between " + priceMin + " and  " + priceMax + "\n"
                    + sortOption + "\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (index != 0 && nextPage != 0) {
                st.setInt(1, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByAllCategory(int parentId, double priceMin, double priceMax, String sort, int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        String sortOption = "order by p.productId";
        if (sort != null) {
            if (sort.equals("sort1")) {
                sortOption = "order by p.price asc";
            } else if (sort.equals("sort2")) {
                sortOption = "order by p.price desc";
            } else if (sort.equals("sort3")) {
                sortOption = "order by p.productName asc";
            } else if (sort.equals("sort4")) {
                sortOption = "order by p.productName desc";
            }
        }
        if (index == 0 && nextPage == 0) {
            sql = "select p.productId,p.cateId,p.brandId,p.productName,p.price,p.discount,p.description,p.image,p.quantity,p.buyNumber,\n"
                    + "p.userCreatedId,p.userModifiedId,p.dateCreated,p.dateModified,p.productStatus\n"
                    + "from Product p join Category c\n"
                    + "on p.cateId = c.cateId\n"
                    + "where c.parentId = ? and p.price between " + priceMin + " and  " + priceMax;
        } else {
            sql = "select p.productId,p.cateId,p.brandId,p.productName,p.price,p.discount,p.description,p.image,p.quantity,p.buyNumber,\n"
                    + "p.userCreatedId,p.userModifiedId,p.dateCreated,p.dateModified,p.productStatus\n"
                    + "from Product p join Category c\n"
                    + "on p.cateId = c.cateId\n"
                    + "where c.parentId = ? and p.price between " + priceMin + " and  " + priceMax + "\n"
                    + sortOption + "\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, parentId);
            if (index != 0 && nextPage != 0) {
                st.setInt(2, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getsearchProduct(String text1, String text2, double priceMin, double priceMax, String sort, int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        String sortOption = "order by productId";
        if (sort != null) {
            if (sort.equals("sort1")) {
                sortOption = "order by price asc";
            } else if (sort.equals("sort2")) {
                sortOption = "order by price desc";
            } else if (sort.equals("sort3")) {
                sortOption = "order by productName asc";
            } else if (sort.equals("sort4")) {
                sortOption = "order by productName desc";
            }
        }
        if (index == 0 && nextPage == 0) {
            sql = "select * from Product\n"
                    + "where (productName like ? or description like ?)\n"
                    + "and price between " + priceMin + " and " + priceMax;
        } else {
            sql = "select * from Product\n"
                    + "where (productName like ? or description like ?)\n"
                    + "and price between " + priceMin + " and " + priceMax + "\n"
                    + sortOption + "\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, text1 + "%");
            st.setString(2, text2 + "%");
            if (index != 0 && nextPage != 0) {
                st.setInt(3, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getsearchProductByAll(double price1, double price2, String sort, int index, int nextPage) {
        List<Product> list = new ArrayList<>();
        String sql;
        String sortOption = "order by productId";
        if (sort != null) {
            if (sort.equals("sort1")) {
                sortOption = "order by price asc";
            } else if (sort.equals("sort2")) {
                sortOption = "order by price desc";
            } else if (sort.equals("sort3")) {
                sortOption = "order by productName asc";
            } else if (sort.equals("sort4")) {
                sortOption = "order by productName desc";
            }
        }
        if (index == 0 && nextPage == 0) {
            sql = "select * from Product\n"
                    + "where price between ? and ?";
        } else {
            sql = "select * from Product\n"
                    + "where price between ? and ?\n"
                    + sortOption + "\n"
                    + "offset ? rows fetch next " + nextPage + " row only";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDouble(1, price1);
            st.setDouble(2, price2);
            if (index != 0 && nextPage != 0) {
                st.setInt(3, (index - 1) * nextPage);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getSearchProductAllWeb(String text) {
        List<Product> list = new ArrayList<>();
        String sql = "select productId,cateId,brandId,productName,price,discount,description,image,\n"
                + "quantity,buyNumber,userCreatedId,userModifiedId,\n"
                + "dateCreated,dateModified,productStatus from Product \n"
                + "where productName like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public void insertProduct(int cateId, String name, double price, double discount, String des, int quantity, int userCreated, String dateCreated, String dateModified, int status, ArrayList<String> listImage, int brandId) {
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "           ([cateId]\n"
                + "           ,[productName]\n"
                + "           ,[price]\n"
                + "           ,[discount]\n"
                + "           ,[description]\n"
                + "           ,[image]\n"
                + "           ,[quantity]\n"
                + "           ,[userCreatedId]\n"
                + "           ,[dateCreated]\n"
                + "           ,[dateModified]\n"
                + "           ,[productStatus]"
                + "           ,[brandId]) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateId);
            st.setString(2, name);
            st.setDouble(3, price);
            st.setDouble(4, discount);
            st.setString(5, des);
            if (listImage != null) {
                st.setString(6, listImage.get(0));
            } else {
                st.setString(6, null);
            }
            st.setInt(7, quantity);
            st.setInt(8, userCreated);
            st.setString(9, dateCreated);
            st.setString(10, dateModified);
            st.setInt(11, status);
            st.setInt(12, brandId);
            st.executeUpdate();
            if (listImage != null) {
                String sql1 = "select top 1 productId from Product\n"
                        + "order by productId desc";
                PreparedStatement st1 = connection.prepareStatement(sql1);
                ResultSet rs = st1.executeQuery();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    for (int i = 1; i < listImage.size(); i++) {
                        String sql2 = "insert into [Pro list image] values(?,?)";
                        PreparedStatement st2 = connection.prepareStatement(sql2);
                        st2.setString(1, listImage.get(i));
                        st2.setInt(2, id);
                        st2.executeUpdate();
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateProduct(int productId, int cateId, String name, double price, double discount, String des, int quantity, int userModified, String dateModified, int status, ArrayList<String> listImage, int brandId) {
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET [cateId] = ?\n"
                + "      ,[productName] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[discount] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[quantity] = ?\n"
                + "      ,[userModifiedId] = ?\n"
                + "      ,[dateModified] = ?\n"
                + "      ,[productStatus] = ?\n"
                + "      ,[brandId] = ?\n"
                + " WHERE productId = " + productId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateId);
            st.setString(2, name);
            st.setDouble(3, price);
            st.setDouble(4, discount);
            st.setString(5, des);
            st.setString(6, listImage.get(0));
            System.out.println(listImage.get(0));
            st.setInt(7, quantity);
            st.setInt(8, userModified);
            st.setString(9, dateModified);
            st.setInt(10, status);
            st.setInt(11, brandId);
            st.executeUpdate();
            List<ProListImage> listImageProduct = new ArrayList<>();
            String sql1 = "select * from [Pro list image]\n"
                    + "where productId = " + productId;
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs1 = st1.executeQuery();
            while (rs1.next()) {
                ProListImage proListImage = new ProListImage();
                proListImage.setId(rs1.getInt("id"));
                proListImage.setImage(rs1.getString("image"));
                proListImage.setProductId(rs1.getInt("productId"));
                listImageProduct.add(proListImage);
            }
            int count = 0;
            if (listImage.size() > 1) {
                for (int i = 1; i < listImage.size(); i++) {
                    if (i < listImageProduct.size()) {
                        String sql2 = "UPDATE [dbo].[Pro list image]\n"
                                + "   SET [image] = ?\n"
                                + " WHERE id = " + listImageProduct.get(i - 1).getId();
                        PreparedStatement st2 = connection.prepareStatement(sql2);
                        st2.setString(1, listImage.get(i));
                        System.out.println(listImage.get(i));
                        st2.executeUpdate();
                        count++;
                    } else if (i > listImageProduct.size()) {
                        String sql3 = "insert into [Pro list image] values(?,?)";
                        PreparedStatement st3 = connection.prepareStatement(sql3);
                        st3.setString(1, listImage.get(i));
                        st3.setInt(2, productId);
                        st3.executeUpdate();
                    }
                }
                if (listImage.size() - 1 < listImageProduct.size()) {
                    for (int i = count; i < listImageProduct.size(); i++) {
                        String sql4 = "Delete from [Pro list image]\n"
                                + "where id = " + listImageProduct.get(i).getId();
                        PreparedStatement st4 = connection.prepareStatement(sql4);
                        st4.executeUpdate();
                    }
                }
            } else {
                for (int i = 0; i < listImageProduct.size(); i++) {
                    String sql5 = "Delete from [Pro list image]\n"
                            + "where id = " + listImageProduct.get(i).getId();
                    PreparedStatement st5 = connection.prepareStatement(sql5);
                    st5.executeUpdate();
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void deleteProduct(int productId) {
        String sql = "delete from Product\n"
                + "where productId = " + productId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void updateViewNumber(long viewNumber, int productId) {
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET [buyNumber] = ?\n"
                + " WHERE productId = " + productId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setLong(1, viewNumber);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Product> getViewNumber(int number) {
        List<Product> list = new ArrayList<>();
        String sql = "select top 9 * from Product \n"
                + "where buyNumber > " + number + "\n"
                + "order by newid()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getRealatedProduct(int relatedId) {
        List<Product> list = new ArrayList<>();
        String sql = "select top 4 p.* from Category c\n"
                + "join Product p on c.cateId = p.cateId\n"
                + "where c.parentId = " + relatedId + "\n"
                + "order by newid()";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setProductId(rs.getInt("productId"));
                CategoryModel categoryModel = new CategoryModel();
                Category c = categoryModel.getCategoryById(rs.getInt("cateId"));
                product.setCategory(c);
                product.setBrandId(rs.getInt("brandId"));
                product.setProductName(rs.getString("productName"));
                product.setPrice(rs.getDouble("price"));
                product.setDiscount(rs.getDouble("discount"));
                product.setDescription(rs.getString("description"));
                product.setImage(rs.getString("image"));
                product.setQuantity(rs.getInt("quantity"));
                product.setBuyNumber(rs.getLong("buyNumber"));
                product.setUserCreatedId(rs.getInt("userCreatedId"));
                product.setUserModifiedId(rs.getInt("userModifiedId"));
                product.setDateCreated(rs.getString("dateCreated"));
                product.setDateModified(rs.getString("dateModified"));
                product.setProductStatus(rs.getInt("productStatus"));
                list.add(product);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        ProductModel p = new ProductModel();
        List<Product> list = p.getsearchProduct("lop", "l", 0, 999999, null, 1, 9);
        for (Product product : list) {
            System.out.println(product.toString());
        }

    }
}
