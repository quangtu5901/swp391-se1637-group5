/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author black
 */
public class PermissionModel extends DBContext {
    public String checkPermission (int roleId) {
        String sql = "select roleName from Role where roleID = ?";
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, roleId);
            ResultSet rs = st.executeQuery();
            if(rs.next()) {
                return rs.getString("roleName");
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    
    public static void main(String[] args) {
        PermissionModel a = new PermissionModel();
        System.out.println(a.checkPermission(3));
    }
}
