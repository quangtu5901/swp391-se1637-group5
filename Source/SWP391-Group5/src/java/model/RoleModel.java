/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import entity.Role;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author black
 */
public class RoleModel extends DBContext {
    public List<Role> getALlRole() {
        String sql = "select * from [Role] where roleID not in (1, 2, 7)";
        List<Role> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                Role role = new Role();
                role.setRoleId(rs.getInt("roleID"));
                role.setRoleName(rs.getString("roleName"));
                list.add(role);
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public List<Role> getRoleUser() {
        String sql = "select * from [Role] where roleID in (2, 7)";
        List<Role> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                Role role = new Role();
                role.setRoleId(rs.getInt("roleID"));
                role.setRoleName(rs.getString("roleName"));
                list.add(role);
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public static void main(String[] args) {
        RoleModel ad = new RoleModel();
        System.out.println(ad.getRoleUser().toString());
    }
}
