/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author black
 */
public class LogModel extends DBContext{
    
    public void insertLog(String result) {
        String sql = "insert into [Log] values(?)";
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, result);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
}
