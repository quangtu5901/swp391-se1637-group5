/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Utilities.Crypto;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import entity.Admin;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.apache.jasper.tagplugins.jstl.core.ForEach;

/**
 *
 * @author black
 */
public class AdminModel extends DBContext {

    public Admin getAdmin(String username, String password) {
        password = Crypto.SHA256(password);
        String sql = "SELECT * FROM [Admin] WHERE [username] = ? AND [password] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Admin a = new Admin();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRoleId(rs.getInt("roleId"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Admin> getAllStaff() {
        String sql = "Select * from [Admin] where roleId not in (1, 2, 7)";
        List<Admin> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Admin a = new Admin();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRoleId(rs.getInt("roleId"));
                a.setStatus(rs.getInt("status"));
                a.setAvartar(rs.getString("avartar"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Admin getAdminByID(int id) {
        String sql = "Select * from [Admin] where id = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Admin a = new Admin();
                a.setId(rs.getInt("id"));
                a.setFullname(rs.getString("fullname"));
                a.setUsername(rs.getString("username"));
                a.setEmail(rs.getString("email"));
                a.setPhone(rs.getString("phone"));
                a.setRoleId(rs.getInt("roleId"));
                a.setStatus(rs.getInt("status"));
                a.setAvartar(rs.getString("avartar"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void UpdateAdmin(Admin ad) {
        String sql = "UPDATE [dbo].[Admin]\n"
                + "   SET [fullname] = ?\n"
                + "      ,[email] = ?\n"
                + "      ,[phone] = ?\n"
                + "      ,[roleId] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[avartar] = ?\n"
                + " WHERE id = ?";
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, ad.getFullname());
            st.setString(2, ad.getEmail());
            st.setString(3, ad.getPhone());
            st.setInt(4, ad.getRoleId());
            st.setInt(5, ad.getStatus());
            st.setString(6, ad.getAvartar());
            st.setInt(7, ad.getId());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
}
