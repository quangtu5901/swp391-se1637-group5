/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author black
 */
public class Payment {

    private int id;
    private String when;
    private int amount;
    private String description;
    private int cusum_balance;
    private String tid;
    private String subAccId;
    private String bank_sub_acc_id;
    private String virtualAccount;
    private String virtualAccountName;
    private String corresponsiveName;
    private String corresponsiveAccount;
    private String corresponsiveBankId;
    private String corresponsiveBankName;

    public Payment() {
    }

    public Payment(int id, String when, int amount, String description, int cusum_balance, String tid, String subAccId, String bank_sub_acc_id, String virtualAccount, String virtualAccountName, String corresponsiveName, String corresponsiveAccount, String corresponsiveBankId, String corresponsiveBankName) {
        this.id = id;
        this.when = when;
        this.amount = amount;
        this.description = description;
        this.cusum_balance = cusum_balance;
        this.tid = tid;
        this.subAccId = subAccId;
        this.bank_sub_acc_id = bank_sub_acc_id;
        this.virtualAccount = virtualAccount;
        this.virtualAccountName = virtualAccountName;
        this.corresponsiveName = corresponsiveName;
        this.corresponsiveAccount = corresponsiveAccount;
        this.corresponsiveBankId = corresponsiveBankId;
        this.corresponsiveBankName = corresponsiveBankName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCusum_balance() {
        return cusum_balance;
    }

    public void setCusum_balance(int cusum_balance) {
        this.cusum_balance = cusum_balance;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getSubAccId() {
        return subAccId;
    }

    public void setSubAccId(String subAccId) {
        this.subAccId = subAccId;
    }

    public String getBank_sub_acc_id() {
        return bank_sub_acc_id;
    }

    public void setBank_sub_acc_id(String bank_sub_acc_id) {
        this.bank_sub_acc_id = bank_sub_acc_id;
    }

    public String getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(String virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public String getVirtualAccountName() {
        return virtualAccountName;
    }

    public void setVirtualAccountName(String virtualAccountName) {
        this.virtualAccountName = virtualAccountName;
    }

    public String getCorresponsiveName() {
        return corresponsiveName;
    }

    public void setCorresponsiveName(String corresponsiveName) {
        this.corresponsiveName = corresponsiveName;
    }

    public String getCorresponsiveAccount() {
        return corresponsiveAccount;
    }

    public void setCorresponsiveAccount(String corresponsiveAccount) {
        this.corresponsiveAccount = corresponsiveAccount;
    }

    public String getCorresponsiveBankId() {
        return corresponsiveBankId;
    }

    public void setCorresponsiveBankId(String corresponsiveBankId) {
        this.corresponsiveBankId = corresponsiveBankId;
    }

    public String getCorresponsiveBankName() {
        return corresponsiveBankName;
    }

    public void setCorresponsiveBankName(String corresponsiveBankName) {
        this.corresponsiveBankName = corresponsiveBankName;
    }

    @Override
    public String toString() {
        return "Payment{" + "id=" + id + ", when=" + when + ", amount=" + amount + ", description=" + description + ", cusum_balance=" + cusum_balance + ", tid=" + tid + ", subAccId=" + subAccId + ", bank_sub_acc_id=" + bank_sub_acc_id + ", virtualAccount=" + virtualAccount + ", virtualAccountName=" + virtualAccountName + ", corresponsiveName=" + corresponsiveName + ", corresponsiveAccount=" + corresponsiveAccount + ", corresponsiveBankId=" + corresponsiveBankId + ", corresponsiveBankName=" + corresponsiveBankName + '}';
    }
    
    
    
   
}
