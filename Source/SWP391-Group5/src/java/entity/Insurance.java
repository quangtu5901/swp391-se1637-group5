/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author ADMIN
 */
public class Insurance {

    private int id;
    private int userid;
    private String ownerName;
    private String plateNumber;
    private int insurYear;
    private String dateFrom;
    private String dateTo;
    private boolean mandatoryInsur;
    private boolean optionInsur;
    private String vehicleType;
    private double totalPrice;

    public Insurance() {
    }

    public Insurance(int id, int userid, String ownerName, String plateNumber, int insurYear, String dateFrom, String dateTo, boolean mandatoryInsur, boolean optionInsur, String vehicleType, double totalPrice) {
        this.id = id;
        this.userid = userid;
        this.ownerName = ownerName;
        this.plateNumber = plateNumber;
        this.insurYear = insurYear;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.mandatoryInsur = mandatoryInsur;
        this.optionInsur = optionInsur;
        this.vehicleType = vehicleType;
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public int getInsurYear() {
        return insurYear;
    }

    public void setInsurYear(int insurYear) {
        this.insurYear = insurYear;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isMandatoryInsur() {
        return mandatoryInsur;
    }

    public void setMandatoryInsur(boolean mandatoryInsur) {
        this.mandatoryInsur = mandatoryInsur;
    }

    public boolean isOptionInsur() {
        return optionInsur;
    }

    public void setOptionInsur(boolean optionInsur) {
        this.optionInsur = optionInsur;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Insurance{" + "id=" + id + ", userid=" + userid + ", ownerName=" + ownerName + ", plateNumber=" + plateNumber + ", insurYear=" + insurYear + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", mandatoryInsur=" + mandatoryInsur + ", optionInsur=" + optionInsur + ", vehicleType=" + vehicleType + ", totalPrice=" + totalPrice + '}';
    }

    public String formatPrice(double amount) {
        Locale locale = new Locale("vn", "VN");
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
        String total = currencyFormatter.format(amount);
        return total;
    }

    public int intPrice(double amount) {
        int price = (int) amount;
        return price;
    }

}
