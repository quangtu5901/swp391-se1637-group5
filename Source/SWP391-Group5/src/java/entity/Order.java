/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author black
 */
public class Order {

    private int id;
    private String orderName;
    private String phoneNumber;
    private int user_id;
    private String dateCreated;
    private double totalPrice;
    private int status;
    private int paymentStatus;
    private String note;
    private String address;

    public Order(int id, String orderName, String phoneNumber, int user_id, String dateCreated, double totalPrice, int status, int paymentStatus, String note, String address) {
        this.id = id;
        this.orderName = orderName;
        this.phoneNumber = phoneNumber;
        this.user_id = user_id;
        this.dateCreated = dateCreated;
        this.totalPrice = totalPrice;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.note = note;
        this.address = address;
    }

    public Order(int orderId, int userId, String dateCreated, double totalPrice, int status, String note, String address) {
        this.id = orderId;
        this.user_id = userId;
        this.dateCreated = dateCreated;
        this.totalPrice = totalPrice;
        this.status = status;
        this.note = note;
        this.address = address;
    }

    public Order() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(int paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", orderName=" + orderName + ", phoneNumber=" + phoneNumber + ", user_id=" + user_id + ", dateCreated=" + dateCreated + ", totalPrice=" + totalPrice + ", status=" + status + ", paymentStatus=" + paymentStatus + ", note=" + note + ", address=" + address + '}';
    }

    
}
