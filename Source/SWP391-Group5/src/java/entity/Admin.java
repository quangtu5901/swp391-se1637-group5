/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

/**
 *
 * @author black
 */
public class Admin {
    private int id;
    private String fullname;
    private String username;
    private String password;
    private String email;
    private String phone;
    private int roleId;
    private int status;
    private String avartar;

    public Admin(int id, String fullname, String username, String email, String phone, int roleId, int status, String avartar) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.roleId = roleId;
        this.status = status;
        this.avartar = avartar;
    }
    
    
    public Admin(int id, String fullname, String username, String password, String email, String phone, int roleId) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.roleId = roleId;
    }

    public Admin() {
    }

    public Admin(int id, String fullname, String username, String email, String phone, int roleId, int status) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.roleId = roleId;
        this.status = status;
    }

    
    public Admin(int id, String fullname, String username, String email, String phone, int roleId) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.roleId = roleId;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAvartar() {
        return avartar;
    }

    public void setAvartar(String avartar) {
        this.avartar = avartar;
    }

    
    @Override
    public String toString() {
        return "Admin{" + "id=" + id + ", fullname=" + fullname + ", username=" + username + ", password=" + password + ", email=" + email + ", phone=" + phone + ", roleId=" + roleId + '}';
    }

   
    
    
    
    
    
}
