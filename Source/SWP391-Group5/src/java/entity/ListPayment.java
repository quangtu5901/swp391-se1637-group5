/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.List;

/**
 *
 * @author black
 */
public class ListPayment {
    public int error;
    public List<Payment> data;

    public ListPayment() {
    }

    public ListPayment(int error, List<Payment> data) {
        this.error = error;
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public List<Payment> getData() {
        return data;
    }

    public void setData(List<Payment> data) {
        this.data = data;
    }

    
    @Override
    public String toString() {
        return "ListPayment{" + "error=" + error + ", payments=" + data + '}';
    }
    
    
    
}
