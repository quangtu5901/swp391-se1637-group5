<%-- 
    Document   : header
    Created on : Jan 16, 2023, 3:42:21 PM
    Author     : black
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.*"%>

<%
    User account = (User) session.getAttribute("account");  
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .searchWeb{
                max-width: 1198px;
                background: white;
                height: 400px;
                border: 1px solid black;
                padding: 8px;
                overflow-y: scroll;
            }
            .searchWeb1{
                padding: 0px;
                padding-right: 32px;
                padding-left: 16px;
            }
            .searchWeb2{
                display: flex;
                border-bottom: 1px dotted #ccc;
                padding: 6px;
                justify-content: space-between;
                align-items: baseline;
            }
            .searchWeb-content{
                display: flex;
            }
            .container #searchAllWeb{
                padding: 8px;
            }
        </style>
    </head>
    <body>
        <!-- Start Header Area -->
        <header class="header_area sticky-header">
            <div class="main_menu">
                <nav class="navbar navbar-expand-lg navbar-light main_box">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <a class="navbar-brand logo_h" href="${pageContext.request.contextPath}/home"><img src="img/logo.png" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                            <ul class="nav navbar-nav menu_nav ml-auto">
                                <li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/home">Home</a></li>
                                <li class="nav-item submenu dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">Shop</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/productshop">Shop Category</a></li>
                                        <li class="nav-item"><a class="nav-link" href="single-product.html">Product Details</a></li>
                                        <li class="nav-item"><a class="nav-link" href="checkout.html">Product Checkout</a></li>
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/showCart">Shopping Cart</a></li>
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/Insurance">Insurance</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item submenu dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/post">Post</a></li>
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/postmanagement">Post Management</a></li>
                                        <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/createpost">Create new post</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item submenu dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                       aria-expanded="false">Pages</a>
                                    <ul class="dropdown-menu">
                                        <c:if test="${account == null}">
                                            <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/login">Login</a></li>
                                            </c:if>
                                            <c:if test="${account != null}">
                                            <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout</a></li>
                                            </c:if>    
                                        <li class="nav-item"><a class="nav-link" href="tracking.html">Tracking</a></li>
                                        <li class="nav-item"><a class="nav-link" href="elements.html">Elements</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/contact">Contact</a></li>
                                <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/agencyRegister">Agency Register</a></li>
                                <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/aboutUs">About Us</a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="nav-item"><a href="${pageContext.request.contextPath}/showCart" class="cart"><span class="ti-bag"></span></a></li>
                                <li class="nav-item">
                                    <button id="search" onclick="hideSearch()" class="search"><span class="lnr lnr-magnifier"></span></button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div style="position: relative" class="search_input" id="search_input_box">
                <div class="container">
                    <form action="${pageContext.request.contextPath}/productshop?action=search" method="post" class="d-flex justify-content-between">
                        <input id="search_input" oninput="searchProductAllWeb(this.value)" name="searchAll" type="text" class="form-control" placeholder="Search Here">
                    </form>
                </div>
            </div>
            <div id="searchAllWeb" style="display: none"></div>
        </header>
        <!-- End Header Area -->
    </body>
    <script>
        function searchProductAllWeb(text) {
            $.ajax({
                url: "/SWP391-Group5/home?action=searchAllWeb",
                type: 'POST',
                data: {
                    textSearch: text
                },
                success: function (data) {
                    var Data = document.getElementById("searchAllWeb");
                    Data.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function hideSearch() {
            var text = document.getElementById("search_input").value = null;
            const search = document.getElementById("searchAllWeb");
            if (search.style.display === "none") {
                search.style.display = "block";
            } else {
                search.style.display = "none";
            }
            searchProductAllWeb(text);
        }
    </script>
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/640b07b14247f20fefe519b4/1gr5hc43c';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
</html>
