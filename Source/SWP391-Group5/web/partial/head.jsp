<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Google tag (gtag.js) -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-CLMR00JJEQ"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'G-CLMR00JJEQ');
        </script>

        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="SWP391-Group5">
        <!-- Meta Description -->
        <meta name="description" content="Cung c?p ??y ?? ph? t�ng thay th? cho xe m�y, 
              t? c�c b? ph?n nh? nh? b�ng ?�n, ch?i than, ?c quy ??n nh?ng b? ph?n l?n nh? th�ng xe, y�n xe v� c�c ph? ki?n kh�c.">
        <!-- Meta Keyword -->
        <meta name="keywords" content="Motor accessories">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>MotorGear</title>
        <!--
                CSS
                ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/nouislider.min.css">
        <link rel="stylesheet" href="css/ion.rangeSlider.css" />
        <link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/fotorama.css">
        <script src="js/jquery-3.6.3.min.js"></script>
        <!-- Font Awesome -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />

        <!-- MDB -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.1.0/mdb.min.css" rel="stylesheet" />
        <link href='https://fonts.googleapis.com/css?family=Be Vietnam' rel='stylesheet'>
        <script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "Article",
            "name": "MotorGear",
            "image": "public/$%7Bp1.image%7D",
            "articleBody": "Cung c?p nh?ng s?n ph?m ch?t l??ng, gi� c? ph?i ch?ng v� lu�n ?�p ?ng ???c nhu c?u c?a kh�ch h�ng."
            }
        </script>   
    </head>
    <body>

    </body>
</html> 