<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <link rel="stylesheet" href="../public/css/uploadimage.css"/>
        <style>
            #subcategory optgroup{
                display:none;
            }
            .form-control1{
                padding: 0.5rem 0.75rem;
                font-size: 0.875rem;
                font-weight: 400;
                line-height: 1.4rem;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #d2d6da;
                border-radius: 0.5rem;
                transition: box-shadow 0.15s ease, border-color 0.15s ease;
            }
        </style>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <div class="min-height-300 bg-primary position-absolute w-100"></div>
        <%@include file="shared/header.jsp" %>
        <main class="main-content position-relative border-radius-lg ">
            <!-- End Navbar -->
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <div class="card-body px-0 pt-0 pb-2">
                                <div class="table-responsive p-0" style="margin: 20px;">
                                    <form method="post" action="${pageContext.request.contextPath}/productManagement?action=update" enctype="multipart/form-data" id="my-form">
                                        <div class="modal-header">						
                                            <h4 class="modal-title">Edit Product</h4>
                                            <!--                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                                        </div>
                                        <div class="modal-body">					
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" required="required" name="name" id="name" value="${name}">
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control1" id="cate" required>
                                                    <option value="">Select Category</option>
                                                    <c:forEach items="${requestScope.cList}"  var="item1">
                                                        <option class="${item1.cateId}" ${(parentId == item1.cateId)?"selected":""} value="${item1.cateId}">${item1.cateName}</option>
                                                    </c:forEach>
                                                </select> 
                                                <select class="form-control1" id="subcategory" name="subcategory">    
                                                    <c:forEach items="${requestScope.cList}"  var="item1">
                                                        <optgroup class="${item1.cateId}" required>
                                                            <option value="${item1.cateId}">Select Sub-category</option>
                                                            <c:forEach items="${item1.subCategory}" var="sub1">
                                                                <option ${(cateId == sub1.cateId)?"selected":""} value="${sub1.cateId}">${sub1.cateName}</option>
                                                            </c:forEach>
                                                        </optgroup>
                                                    </c:forEach>
                                                </select>  
                                            </div>
                                             <div class="form-group">
                                                <select class="form-control1" name="brandId" required>
                                                    <option value="">Select Brand</option>
                                                    <c:forEach items="${requestScope.bList}"  var="item">
                                                        <option ${(brandId == item.brandId)?"selected":""} value="${item.brandId}">${item.brandName}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Price</label>
                                                <input type="text" class="form-control" required="required" name="price" id="price" value="${price}">
                                            </div>
                                            <div class="form-group">
                                                <label>Discount</label>
                                                <input type="text" class="form-control" required="required" name="discount" id="discount" value="${discount}">
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea class="form-control" required="required" name="description" id="des">${des}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Quantity</label>
                                                <input type="text" class="form-control" required="required" name="quantity" id="quantity" value="${quantity}">
                                            </div>
                                            <div class="form-group">
                                                <label>Image</label>
                                                <div style="width:40%;height: 100px;display:flex;justify-content:center" class="form-control">
                                                    <img style="margin-right: 20px;border: 1px solid silver" src="${image}" width="16%" height="100%" alt="50px"/>
                                                    <c:forEach items="${requestScope.listPLI}" var="pli">
                                                        <img style="margin-right: 20px;border: 1px solid silver" src="${pli.image}" width="16%" height="100%" alt="50px"/>
                                                    </c:forEach>
                                                </div>
                                                <div style="display: flex">
                                                    <div style="width: 40%;margin: 20px 0px" class="multiple-uploader" id="multiple-uploader">
                                                        <div class="mup-msg">
                                                            <span class="mup-main-msg">Click to upload images.</span>
                                                            <span class="mup-msg" id="max-upload-number">Upload up to 10 images</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>	
                                            <div class="form-group">
                                                <label>Status</label>
                                                <c:choose>
                                                    <c:when test="${status == 1}">
                                                        <div>
                                                            <span><input type="radio" name="status" checked="checked" value="1"> Available</span>
                                                            <span> <input type="radio" name="status" value="2"> Unavailable</span>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div>
                                                            <span><input type="radio" name="status"  value="1"> Available</span>
                                                            <span> <input type="radio" name="status" checked="checked" value="2"> Unavailable</span>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-default" value="Cancel" name="submit" style="margin-right: 20px;">
                                            <input type="submit" class="btn btn-info" value="Save" name="submit">
                                        </div>      
                                        <input type="hidden" name="id" id="id" value="${id}">
                                    </form>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>

                <footer class="footer pt-3  ">
                </footer>
            </div>
        </main>
        <!--   Core JS Files   -->
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.min.js"></script>
        <script src="js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script>
                                var win = navigator.platform.indexOf('Win') > -1;
                                if (win && document.querySelector('#sidenav-scrollbar')) {
                                    var options = {
                                        damping: '0.5'
                                    };
                                    Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
                                }
        </script>
        <script>
            var el = document.getElementById('cate');
            el.onchange = (function () {
                var category = $('#cate :selected').attr('class');
                $('#subcategory optgroup').hide();
                $('#subcategory').val("");
                $('#subcategory .' + category).show();
            });
            var e2 = document.getElementById('category1');
            e2.onchange = (function () {
                var category = $('#category1 :selected').attr('class');
                $('#subcategory1 optgroup').hide();
                $('#subcategory1').val("");
                $('#subcategory1 .' + category).show();
            });
        </script>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>

        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
        <script src="../public/js/multiple-uploader.js"></script>
        <script>
            let multipleUploader = new MultipleUploader('#multiple-uploader').init({
                maxUpload: 5, // maximum number of uploaded images
                maxSize: 5, // in size in mb
                filesInpName: 'images', // input name sent to backend
                formSelector: '#my-form', // form selector
            });
        </script>
    </body>

</html>