<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <link href="//cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="css/uploadimage.css"/>
        <style>
            .edit_btn_management{
                font-size: var(--bs-badge-font-size);
                font-weight: var(--bs-badge-font-weight);
                text-transform: uppercase;
                padding:0px;
                border:0px;
                background-image: linear-gradient(310deg, #627594 0%, #a8b8d8 100%);
                color: white;
                width: 100%;
            }
            .uploadImg1{
                background: #444242 ;
                color: #fff;
                background-size: cover;
                background-position: center;
                max-width: 450px;
                height: 200px;
                position: relative;
                margin:  10px;
            }
            .uploadImg2{
                background: #444242 ;
                color: #fff;
                background-size: cover;
                background-position: center;
                max-width: 200px;
                height: 200px;
                position: relative;
            }
            .uploadImg3{
                background: #444242 ;
                color: #fff;
                background-size: cover;
                background-position: center;
                max-width: 250px;
                height: 400px;
                position: relative;
            }
            .upload-icon{
                position: absolute;
                width: 75px;
                height: auto;
                bottom: 32%;
                left: 50%;
                transform: translateX(-50%);
                cursor: pointer;

            }
        </style>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <div class="min-height-300 bg-primary position-absolute w-100"></div>
        <%@include file="shared/header.jsp" %>
        <main class="main-content position-relative border-radius-lg ">
            <!-- End Navbar -->
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <div class="card-header pb-0">
                                <h6>Advertiser Section</h6>
                            </div>
                            <div class="card-body px-0 pt-0 pb-2">
                                <div class="p-0">
                                    <!-- Start category Area -->
                                    <form method="post" action="${pageContext.request.contextPath}/bannerManagement?action=editAdver" enctype="multipart/form-data">
                                        <section class="category-area">
                                            <div class="container">
                                                <div class="row justify-content-center">
                                                    <div class="col-lg-8 col-md-12">
                                                        <div class="row">
                                                            <div class="col-lg-8 col-md-8 uploadImg1" id="img-box1" style="background-image: url('${A1}');">
                                                                <span class="file_remove" id="remove-btn1" style="padding-left: 11px; display: none;">X</span>
                                                                <div class="form-group" id="image1">
                                                                    <input type="file" accept="image/*" name="image1" id="file1" style="display: none;" onchange="loadFile1(event)">
                                                                </div>
                                                                <label for="file1"><img src="img/upload.png" class="upload-icon"> </label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 uploadImg2" id="img-box2" style="background-image: url('${A2}');">
                                                                <span class="file_remove" id="remove-btn2" style="padding-left: 11px; display: none;">X</span>
                                                                <div class="form-group" id="image2">
                                                                    <input type="file" accept="image/*" name="image2" id="file2" style="display: none;" onchange="loadFile2(event)">
                                                                </div>
                                                                <label for="file2"><img src="img/upload.png" class="upload-icon"> </label>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 uploadImg2" id="img-box3" style="background-image: url('${A3}');">
                                                                <span class="file_remove" id="remove-btn3" style="padding-left: 11px; display: none;">X</span>
                                                                <div class="form-group" id="image3">
                                                                    <input type="file" accept="image/*" name="image3" id="file3" style="display: none;" onchange="loadFile3(event)">
                                                                </div>
                                                                <label for="file3"><img src="img/upload.png" class="upload-icon"> </label>
                                                            </div>
                                                            <div class="col-lg-8 col-md-8 uploadImg1" id="img-box4" style="background-image: url('${A4}');">
                                                                <span class="file_remove" id="remove-btn4" style="padding-left: 11px; display: none;">X</span>
                                                                <div class="form-group" id="image4">
                                                                    <input type="file" accept="image/*" name="image4" id="file4" style="display: none;" onchange="loadFile4(event)">
                                                                </div>
                                                                <label for="file4"><img src="img/upload.png" class="upload-icon"> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-6 uploadImg3" id="img-box5" style="background-image: url('${A5}');">
                                                        <span class="file_remove" id="remove-btn5" style="padding-left: 11px; display: none;">X</span>
                                                        <div class="form-group" id="image5">
                                                            <input type="file" accept="image/*" name="image5" id="file5" style="display: none;" onchange="loadFile5(event)">
                                                        </div>
                                                        <label for="file5"><img src="img/upload.png" class="upload-icon"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="submit" class="btn btn-info" value="Save" name="submit" style="margin-right: 20px;">
                                            </div>
                                        </section>

                                    </form>
                                    <!-- End category Area -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <div class="card-header pb-0">
                                <h6>Banner Table</h6>
                                <form method="post" action="${pageContext.request.contextPath}/bannerManagement?action=addBanner">
                                    <input type="submit" class="btn btn-success" value="Add new Banner">
                                </form>
                            </div>
                            <div class="card-body px-0 pt-0 pb-2">
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Title</th>
                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Link vd:(Phu-tung-thay-the-1)</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${requestScope.listB}" var="sub">   
                                                <tr>
                                                    <td>
                                                        <div class="d-flex px-2 py-1 ">                                         
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <img src="${sub.image}" class="avatar avatar-sm me-3" alt="user1">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex px-2 py-1 justify-content-center">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-sm">${sub.title}</h6>
                                                            </div>       
                                                        </div>
                                                    </td>
                                                    <td class="align-middle text-center text-sm">
                                                        <div class="d-flex px-2 py-1 justify-content-center">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-sm">${sub.link}</h6>
                                                            </div>       
                                                        </div>
                                                    </td>
                                                    <td class="align-middle">
                                                        <form method="post" action="${pageContext.request.contextPath}/bannerManagement?action=editBanner&id=${sub.id}">
                                                            <span class="badge badge-sm bg-gradient-secondary">
                                                                <input class="edit_btn_management" type="submit" value="Edit">
                                                            </span>
                                                        </form>
                                                    </td>
                                                </tr> 
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="footer pt-3  ">
                </footer>
            </div>
        </main>
        <!--   Core JS Files   -->
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.min.js"></script>
        <script src="js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script>
                                                                $(document).ready(function () {
                                                                    $('.table').DataTable();
                                                                });
        </script>
        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
        <script>
                                                                var imgBox1 = document.getElementById('img-box1');
                                                                var loadFile1 = function (event) {
                                                                    imgBox1.style.backgroundImage = "url(" + URL.createObjectURL(event.target.files[0]) + ")";
                                                                    document.getElementById('remove-btn1').style.display = "block";
                                                                };
                                                                $("#remove-btn1").on("click", function (e) {
                                                                    imgBox1.style.backgroundImage = "";
                                                                    document.getElementById('file1').value = "";
                                                                    document.getElementById('remove-btn1').style.display = "none";
                                                                });
                                                                var imgBox2 = document.getElementById('img-box2');
                                                                var loadFile2 = function (event) {
                                                                    imgBox2.style.backgroundImage = "url(" + URL.createObjectURL(event.target.files[0]) + ")";
                                                                    document.getElementById('remove-btn2').style.display = "block";
                                                                };
                                                                $("#remove-btn2").on("click", function (e) {
                                                                    imgBox2.style.backgroundImage = "";
                                                                    document.getElementById('file2').value = "";
                                                                    document.getElementById('remove-btn2').style.display = "none";
                                                                });
                                                                var imgBox3 = document.getElementById('img-box3');
                                                                var loadFile3 = function (event) {
                                                                    imgBox3.style.backgroundImage = "url(" + URL.createObjectURL(event.target.files[0]) + ")";
                                                                    document.getElementById('remove-btn3').style.display = "block";
                                                                };
                                                                $("#remove-btn3").on("click", function (e) {
                                                                    imgBox3.style.backgroundImage = "";
                                                                    document.getElementById('file3').value = "";
                                                                    document.getElementById('remove-btn3').style.display = "none";
                                                                });
                                                                var imgBox4 = document.getElementById('img-box4');
                                                                var loadFile4 = function (event) {
                                                                    imgBox4.style.backgroundImage = "url(" + URL.createObjectURL(event.target.files[0]) + ")";
                                                                    document.getElementById('remove-btn4').style.display = "block";
                                                                };
                                                                $("#remove-btn4").on("click", function (e) {
                                                                    imgBox4.style.backgroundImage = "";
                                                                    document.getElementById('file4').value = "";
                                                                    document.getElementById('remove-btn4').style.display = "none";
                                                                });
                                                                var imgBox5 = document.getElementById('img-box5');
                                                                var loadFile5 = function (event) {
                                                                    imgBox5.style.backgroundImage = "url(" + URL.createObjectURL(event.target.files[0]) + ")";
                                                                    document.getElementById('remove-btn5').style.display = "block";
                                                                };
                                                                $("#remove-btn5").on("click", function (e) {
                                                                    imgBox5.style.backgroundImage = "";
                                                                    document.getElementById('file5').value = "";
                                                                    document.getElementById('remove-btn5').style.display = "none";
                                                                });
        </script>
    </body>

</html>