<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <link rel="stylesheet" href="css/uploadimage.css"/>
        <style>
            .form-control1{
                padding: 0.5rem 0.75rem;
                font-size: 0.875rem;
                font-weight: 400;
                line-height: 1.4rem;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #d2d6da;
                border-radius: 0.5rem;
                transition: box-shadow 0.15s ease, border-color 0.15s ease;
                display: block;
                margin-bottom: 20px
            }
        </style>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <div class="min-height-300 bg-primary position-absolute w-100"></div>
        <%@include file="shared/header.jsp" %>
        <main class="main-content position-relative border-radius-lg ">
            <!-- End Navbar -->
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <div class="card-body px-0 pt-0 pb-2">
                                <div class="table-responsive p-0" style="margin: 20px;">
                                    <form method="post" action="${pageContext.request.contextPath}/categoryManagement?action=update" enctype="multipart/form-data">
                                        <div class="modal-header">						
                                            <h4 class="modal-title">Edit category</h4>
                                        </div>
                                        <div class="modal-body">					
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" required name="name" value="${cateName}">
                                            </div>
                                            <c:if test="${parentId != 0}">
                                                <div class="form-group">
                                                    <select id="cate" name="cate" required onchange="ShowHideDiv()">
                                                        <option value="">select parent category</option> 
                                                        <c:forEach items="${requestScope.cList}"  var="item">
                                                            <option class="${item.cateId}" ${(parentId == item.cateId)?"selected":""} value="${item.cateId}">${item.cateName}</option>
                                                        </c:forEach>
                                                    </select>                                              
                                                </div>
                                            </c:if>
                                            <c:if test="${parentId == 0}">
                                                <div class="form-group" id="image1">
                                                    <label>Image</label>
                                                    <div>
                                                        <img class="form-control1" src="${image}" width="20%" height="auto" alt="alt"/>
                                                        <div class="button_outer">
                                                            <div class="btn_upload">
                                                                <input type="file" id="upload_file" name="image">Upload Image
                                                            </div>
                                                            <div class="success_box"></div>
                                                        </div>
                                                    </div>
                                                    <div class="uploaded_file_view" id="uploaded_view">
                                                        <span class="file_remove">X</span>
                                                    </div>
                                                </div>	
                                            </c:if>
                                            <div style="color: red" class="error_msg"></div>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <c:choose>
                                                    <c:when test="${cateStatus == 1}">
                                                        <div>
                                                            <span><input type="radio" name="status" checked="checked" value="1"> Available</span>
                                                            <span> <input type="radio" name="status" value="2"> Unavailable</span>
                                                        </div>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <div>
                                                            <span><input type="radio" name="status"  value="1"> Available</span>
                                                            <span> <input type="radio" name="status" checked="checked" value="2"> Unavailable</span>
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-default" value="Cancel" name="submit" style="margin-right: 20px;">
                                            <input type="submit" class="btn btn-info" value="Save" name="submit">
                                            <input type="hidden" name="id" value="${id}">
                                        </div>
                                    </form>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>

                <footer class="footer pt-3  ">
                </footer>
            </div>
        </main>
        <!--   Core JS Files   -->
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.min.js"></script>
        <script src="js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script>
                                                        var win = navigator.platform.indexOf('Win') > -1;
                                                        if (win && document.querySelector('#sidenav-scrollbar')) {
                                                            var options = {
                                                                damping: '0.5'
                                                            };
                                                            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
                                                        }
        </script>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>

        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
        <script>
            var btnUpload = $("#upload_file"), btnOuter = $(".button_outer");
            btnUpload.on("change", function (e) {
                var ext = btnUpload.val().split('.').pop().toLowerCase();
                if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) === -1) {
                    $(".error_msg").text("Not an Image...");
                } else {
                    $(".error_msg").text("");
                    btnOuter.addClass("file_uploading");
                    btnOuter.addClass("file_uploaded");
                    var uploadedFile = URL.createObjectURL(e.target.files[0]);
                    $("#uploaded_view").append('<img src="' + uploadedFile + '" />').addClass("show");
                }
            });
            $(".file_remove").on("click", function (e) {
                $("#uploaded_view").removeClass("show");
                $("#uploaded_view").find("img").remove();
                btnOuter.removeClass("file_uploading");
                btnOuter.removeClass("file_uploaded");
            });
        </script>
        <script type="text/javascript">
            function ShowHideDiv() {
                var categoryId = document.getElementById("cate");
                var image = document.getElementById("image1");
                image.style.display = categoryId.value == "0" ? "block" : "none";
            }
        </script>
    </body>

</html>