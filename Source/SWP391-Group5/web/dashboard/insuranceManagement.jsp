<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <link href="//cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css" rel="stylesheet"/>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <div class="min-height-300 bg-primary position-absolute w-100"></div>
        <%@include file="shared/header.jsp" %>    
        <main class="main-content position-relative border-radius-lg ">
            <!-- End Navbar -->
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card mb-4">
                            <div class="card-header pb-0">
                                <h6>Order request table</h6>
                                <h7>
                                    <form action="${pageContext.request.contextPath}/orderManagement?action=searchFromTo" method="post">
                                        <label for="From ">From:</label>
                                        <input type="datetime-local" id="birthdaytime" name="from"/>
                                        <label for="To">To:</label>
                                        <input type="datetime-local" id="birthdaytime" name="to"/>

                                        <input type="submit" class="btn btn-success" value="Search"/>
                                    </form>
                                </h7>
                            </div>
                            <div class="card-body px-0 pt-0 pb-2">
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                            <tr>
                                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">User ID</th>
                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Plate Number</th>
                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Register Date</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <c:forEach items="${requestScope.list}" var="item">
                                                <tr>    
                                                    <td>
                                                        <div class="d-flex px-2 py-1">
                                                            <div class="d-flex flex-column justify-content-center" name="orderId">
                                                                <h6 class="mb-0 text-sm">${item.userid}</h6>
                                                            </div>       
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex px-2 py-1 justify-content-center">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-sm">${item.plateNumber}</h6>
                                                            </div>       
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="d-flex px-2 py-1 justify-content-center">
                                                            <div class="d-flex flex-column justify-content-center">
                                                                <h6 class="mb-0 text-sm">${item.dateFrom}</h6>
                                                            </div>       
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a href="#editEmployeeModal"  class="edit" data-toggle="modal">View detail</a>
                                                        <input type="hidden" value="${item.id}" name="orderId" id="orderId" >
                                                    </td>
                                                </tr>
                                            </c:forEach>                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer pt-3  ">
                </footer>
            </div>
        </main>
        <div id="editEmployeeModal" class="modal fade">
            <div class="modal-dialog" id="orderDetail">
                <div class="modal-content" style="min-width: 800px;">
                    <div class="modal-header">						
                        <h4 class="modal-title">Insurance Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="des-order col-md-12">
                                <h5>Vehicle Insurance</h5>
                            </div>
                            <!-- End .header-order -->
                            <div class="content-order">
                                <p><i class="fa fa-taxi" aria-hidden="true"></i><span>Plate Number: <span id="show-plate"></span> </span></p>
                                <p>
                                    <i class="fa fa-flickr" aria-hidden="true"></i> <span id="MainContent_thoiHan_message" style="font-size: 14.5px">Insurance Date: <span></span> - <span></span></span>
                                </p>
                                <p class="pe-ta"><i class="fa fa-cog" aria-hidden="true"></i><span>Owner Name: <span id="show-ownerName"></span> </span></p>
                                <p class="sum-su">
                                    <i class="fa fa-usd" aria-hidden="true"></i><span>Total price: <span id="total">86,000 VNĐ</span></span>
                                </p>
                            </div>
                            <!-- End .content-order -->
                            <!-- End .info-order -->
                        </div>

                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.min.js"></script>
        <script src="js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>

        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
        </script>
        <script>
            var win = navigator.platform.indexOf('Win') > -1;
            if (win && document.querySelector('#sidenav-scrollbar')) {
                var options = {
                    damping: '0.5'
                };
                Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
            }
        </script>
        <script>
            $(document).ready(function () {
                $('table .edit').on('click', function () {
                    var id = $(this).parent().find('#orderId').val();
                    $.ajax({
                        url: "/SWP391-Group5/insurManagement?action=search",
                        type: 'POST',
                        data: {
                            txt: id
                        },
                        success: function (data) {
                            var showOrder = document.getElementById("orderDetail");
                            showOrder.innerHTML = data;
                        },
                        error: function (jqXHR) {
                            console.log(jqXHR);
                        }
                    });
                });
            });
        </script>
        <style>
            .form-group span{
                font-size: 15px;
                color: #000;
            }
        </style>
        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
    </body>

</html>