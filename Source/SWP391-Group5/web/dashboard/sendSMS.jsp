<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <style>
            #subcategory optgroup{
                display:none;
            }
            #subcategory1 optgroup{
                display:none;
            }
        </style>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <%@include file="shared/header.jsp" %>
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">

                        <!--                        <div class="card-body px-0 pt-0 pb-2">
                                                    <div class="table-responsive p-0">-->
                        <section class="vh-100" style="background-color: #9A616D;">
                            <div class="container py-5 h-100">
                                <div class="row d-flex justify-content-center align-items-center h-100">
                                    <div class="col col-xl-10">
                                        <div class="card" style="border-radius: 1rem;">
                                            <div class="row g-0">
                                                <div class="col-md-6 col-lg-5 d-none d-md-block">
                                                    <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/img1.webp"
                                                         alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
                                                </div>
                                                <div class="col-md-6 col-lg-7 d-flex align-items-center">
                                                    <div class="card-body p-4 p-lg-5 text-black">

                                                        <form action="${pageContext.request.contextPath}/sendSMS" method="post">

                                                            <div>
                                                                <h1 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px; text-align: center">Send SMS Host</h1>
                                                            </div>
                                                            <div class="form-outline mb-4">
                                                                <input type="number" id="form2Example17" class="form-control form-control-lg" name="phonenumber"/>
                                                                <label class="form-label" for="form2Example17">Phone Number</label>
                                                            </div>

                                                            <div class="form-outline mb-4">
                                                                <textarea  id="form2Example27" class="form-control form-control-lg" name="message"></textarea>
                                                                <label class="form-label" for="form2Example27">Message</label>
                                                            </div>

                                                            <div class="pt-1 mb-4">
                                                                <button class="btn btn-dark btn-lg btn-block" type="submit">Send</button>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--                            </div>
                                                </div>-->
                    </div>
                </div>
            </div>


        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="js/core/popper.min.js"></script>
    <script src="js/core/bootstrap.min.js"></script>
    <script src="js/plugins/perfect-scrollbar.min.js"></script>
    <script src="js/plugins/smooth-scrollbar.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            };
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
    <script>
        var el = document.getElementById('category');
        el.onchange = (function () {
            var category = $('#category :selected').attr('class');
            $('#subcategory optgroup').hide();
            $('#subcategory').val("");
            $('#subcategory .' + category).show();
        });
        var e2 = document.getElementById('category1');
        e2.onchange = (function () {
            var category = $('#category1 :selected').attr('class');
            $('#subcategory1 optgroup').hide();
            $('#subcategory1').val("");
            $('#subcategory1 .' + category).show();
        });
    </script>
    <script>
        $(document).ready(function () {
            // Activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Select/Deselect checkboxes
            var checkbox = $('table tbody input[type="checkbox"]');
            $("#selectAll").click(function () {
                if (this.checked) {
                    checkbox.each(function () {
                        this.checked = true;
                    });
                } else {
                    checkbox.each(function () {
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function () {
                if (!this.checked) {
                    $("#selectAll").prop("checked", false);
                }
            });
        });
    </script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
</body>

</html>