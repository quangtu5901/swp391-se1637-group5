<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <style>
            #subcategory optgroup{
                display:none;
            }
            #subcategory1 optgroup{
                display:none;
            }
        </style>
    </head>

    <body class="g-sidenav-show   bg-gray-100">
        <%@include file="shared/header.jsp" %>
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-header pb-0">
                            <h6>Agency Management</h6> 
                            <form method="post" action="${pageContext.request.contextPath}/productManagement?action=addProduct">
                                <input type="submit" class="btn btn-success" value="Add New Staff">
                            </form>
                        </div>
                        <div class="card-body px-0 pt-0 pb-2">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center mb-0">
                                    <thead>
                                        <tr>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">UserName</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Agency Name</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone Number</th>                                     
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Edit Profile</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Order History</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.UserList}" var="item">
                                            <tr>    
                                                <td>
                                                    <div class="d-flex px-2 py-1">
                                                        <div>
                                                            <img src="${item.avatar}" class="avatar avatar-sm me-3" alt="user1">
                                                        </div>
                                                        <div class="d-flex flex-column justify-content-center">
                                                            <h6 class="mb-0 text-sm">${item.username}</h6>                                                 
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">${item.fullName}</p>
                                                </td>



                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">${item.email}</p>
                                                </td>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">${item.phoneNumber}</p>
                                                </td>
                                                <td class="align-middle text-center text-sm">
                                                    <c:choose>
                                                        <c:when test="${item.status == 1}">
                                                            <span class="badge badge-sm bg-gradient-success" >Active</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="badge badge-sm bg-gradient-secondary">Deactive</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </td>


                                                <td class="align-middle text-center text-sm">
                                                    <form method="post" action="${pageContext.request.contextPath}/userManagement?action=editUser">
                                                        <input type="hidden" name="id" id="id" value="${item.id}">
                                                        <input type="submit" class="text-secondary font-weight-bold text-xs edit" value="Edit">
                                                    </form>
                                                </td>

                                                <td class="align-middle text-center text-sm">
                                                    <form method="post" action="${pageContext.request.contextPath}/historyOrder">
                                                        <input type="hidden" name="id" id="id" value="${item.id}">
                                                        <input type="submit" class="text-secondary font-weight-bold text-xs edit" value="View">
                                                    </form>
                                                </td>
                                            </tr>
                                        </c:forEach> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer pt-3  ">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-lg-between">
                        <div class="col-lg-6 mb-lg-0 mb-4">
                            <div class="copyright text-center text-sm text-muted text-lg-start">
                                Â©
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>,
                                made with <i class="fa fa-heart"></i> by
                                <a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Creative Tim</a>
                                for a better web.
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                                <li class="nav-item">
                                    <a href="https://www.creative-tim.com" class="nav-link text-muted" target="_blank">Creative Tim</a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://www.creative-tim.com/presentation" class="nav-link text-muted" target="_blank">About
                                        Us</a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://www.creative-tim.com/blog" class="nav-link text-muted" target="_blank">Blog</a>
                                </li>
                                <li class="nav-item">
                                    <a href="https://www.creative-tim.com/license" class="nav-link pe-0 text-muted"
                                       target="_blank">License</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </main>
    
    </div>
    <!--   Core JS Files   -->
    <script src="js/core/popper.min.js"></script>
    <script src="js/core/bootstrap.min.js"></script>
    <script src="js/plugins/perfect-scrollbar.min.js"></script>
    <script src="js/plugins/smooth-scrollbar.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script>
                            var win = navigator.platform.indexOf('Win') > -1;
                            if (win && document.querySelector('#sidenav-scrollbar')) {
                                var options = {
                                    damping: '0.5'
                                };
                                Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
                            }
    </script>
    <script>
        var el = document.getElementById('category');
        el.onchange = (function () {
            var category = $('#category :selected').attr('class');
            $('#subcategory optgroup').hide();
            $('#subcategory').val("");
            $('#subcategory .' + category).show();
        });
        var e2 = document.getElementById('category1');
        e2.onchange = (function () {
            var category = $('#category1 :selected').attr('class');
            $('#subcategory1 optgroup').hide();
            $('#subcategory1').val("");
            $('#subcategory1 .' + category).show();
        });
    </script>
    <script>
        $(document).ready(function () {
            // Activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

            // Select/Deselect checkboxes
            var checkbox = $('table tbody input[type="checkbox"]');
            $("#selectAll").click(function () {
                if (this.checked) {
                    checkbox.each(function () {
                        this.checked = true;
                    });
                } else {
                    checkbox.each(function () {
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function () {
                if (!this.checked) {
                    $("#selectAll").prop("checked", false);
                }
            });
        });
    </script>

    <!-- Github buttons -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="js/argon-dashboard.min.js?v=2.0.4"></script>
</body>

</html>