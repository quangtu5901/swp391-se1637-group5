<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="zxx" class="no-js">

    <head>
        <base href="${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>

    </head>

    <body>

        <%@include file="../partial/header.jsp" %>

        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h2>Policy</h2>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="category.html">Policy</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Blog Area =================-->



        <section class="blog_area single-post-area section_gap">
            <div class="row justify-content-center">
                <div class="col-lg-2" ></div>
                <div class="col-lg-8">
                    ${requestScope.policy}
                </div>
                <div class="col-lg-2" ></div>
                
            </div>
        </section>
        <!--================Blog Area =================-->

        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>
    </body>

</html>
