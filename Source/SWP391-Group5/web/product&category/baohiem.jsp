<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="zxx" class="no-js">

    <head>
        <base href="${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
    </head>

    <body id="category">

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Insurance Registration</h1>
                        <nav class="d-flex align-items-center">
                            <a href="${pageContext.request.contextPath}/home">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/productshop">Shop<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/Insurance">Insurance</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
        <div class="container">
            <div class="row justify-content-center" style="margin-bottom: 150px;">
                <div class="col-md-8">
                    <form method="POST" action="${pageContext.request.contextPath}/insurManagement?action=create" id="form">
                        <div class="col-md-12 justify-content-center d-flex">
                            <div class="content-step" style="min-width: 800px;">
                                <div class="form-group">
                                    <label style="color:red !important;font-weight:bold">Lưu ý: Quý khách vui lòng chọn chính xác nhóm loại xe để đảm bảo quyền lợi bảo hiểm</label>
                                    <div class="d-flex justify-content-between">
                                        <label class="col-sm-6">Số năm bảo hiểm (<span>*</span>)</label>
                                        <div class="select-style col-sm-6">
                                            <select id="year" class="form-control motoSoNam" name="year">
                                                <option value="1">1 năm</option>
                                                <option value="2">2 năm</option>
                                                <option value="3">3 năm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <label class="col-sm-6">Thời hạn bảo hiểm: (<span>*</span>)</label>
                                    <div class="col-sm-6">
                                        <table>
                                            <tbody><tr>
                                                    <td>Từ&nbsp;
                                                    </td>
                                                    <td>
                                                        <input type="date" id="txtDate" class="form-control" name="thoihan">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <label for="group-byke" class="col-sm-6">Chọn nhóm loại xe (<span class="start">*</span>)</label>
                                    <div class="select-style col-sm-6">
                                        <select name="loaixe" id="loaixe" class="form-control" required="required">
                                            <option selected="selected" value="2">Xe Mô tô 2 bánh dung tích trên 50cc</option>
                                            <option value="1">Xe Mô tô 2 bánh dung tích từ 50cc trở xuống</option>
                                            <option value="3">Mô tô 3 bánh</option>
                                            <option value="4">Xe máy điện</option>
                                            <option value="5">Các loại xe còn lại</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label class="respon-civil col-sm-6">
                                            <input name="baohiem1" type="checkbox" checked="" disabled="" id="baohiem1">
                                            Bảo hiểm trách nhiệm dân sự bắt buộc
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="respon-civil col-sm-6">
                                            <input name="baohiem2" type="checkbox" id="baohiem2">                                  
                                            Bảo hiểm tai nạn người trên xe
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <label  class="col-sm-6">Tên chủ xe: (<span>*</span>)</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="col-sm-6 form-control" placeholder="Ghi theo đăng ký" required="required" id="ownerName" name="ownerName">                  
                                    </div>
                                </div>
                                <div class="form-group d-flex justify-content-between">
                                    <label  class="col-sm-6">Biển kiểm soát: (<span>*</span>)</label>
                                    <div class="col-sm-12">
                                        <input  type="text" class="col-sm-6 form-control" required="required" id="plate" name="plateNumber">                           
                                    </div>
                                </div>
                                <div class="form-group d-flex float-right">
                                    <input type="submit" value="Submit" class="btn" style="background:  linear-gradient(90deg, #ffba00 0%, #ff6c00 100%);">
                                </div>
                            </div>           
                        </div>
                    </form>
                </div>
                <div class="col-md-4" >
                    <div class="col-md-12">
                        <div class="header-order" style="background: linear-gradient(90deg, #ffba00 0%, #ff6c00 100%);">
                            <div>
                                <h2 style="font-family: be vietnam">Thông tin đơn hàng</h2>
                                <div class="des-order col-md-12">
                                    <div>Sản phẩm: Bảo hiểm xe máy</div>
                                    <div>Thông tin đơn hàng</div>
                                </div>
                            </div>
                        </div>
                        <!-- End .header-order -->
                        <div class="content-order" style="padding: 10px; border: #ff6c00 solid 1px;">
                            <p><i class="fa fa-taxi" aria-hidden="true"></i><span>Biển số xe: <span id="show-plate"></span> </span></p>
                            <p>
                                <i class="fa fa-flickr" aria-hidden="true"></i> <span id="MainContent_thoiHan_message" style="font-size: 14.5px">Thời hạn bảo hiểm: <span id="show-startDate"></span> - <span id="show-endDate"></span></span>
                            </p>
                            <p class="pe-ta"><i class="fa fa-cog" aria-hidden="true"></i><span>Tên chủ xe: <span id="show-ownerName"></span> </span></p>
                            <div class="insurance-fees">

                                <p id="MainContent_TNDS_BB" style="display: block;">
                                    <i class="fa fa-usd" aria-hidden="true"></i><span>Phí bảo hiểm trách nhiệm dân sự: <span id="show-fee1">66,000 VNĐ</span></span>
                                </p>
                                <p id="MainContent_BHTN_NTX" style="display: block;">
                                    <i class="fa fa-usd" aria-hidden="true"></i><span>Phí bảo hiểm người ngồi: <span id="show-fee2">20,000 VNĐ</span></span>
                                </p>                            
                            </div>



                            <p class="sum-su">
                                <i class="fa fa-usd" aria-hidden="true"></i><span>Tổng phí thanh toán: <span id="total">86,000 VNĐ</span></span>
                            </p>
                        </div>
                        <!-- End .content-order -->
                        <!-- End .info-order -->
                    </div>
                </div>


                </form>
            </div>
        </div>
        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>
    </body>
    <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>
    <script>
        function addYears(date, years) {
            var endDate = new Date(date);
            endDate.setFullYear(date.getFullYear() + years);

            return endDate;
        }
        ;
        //a simple date formatting function
        function dateFormat(inputDate, format) {
            //parse the input date

            //extract the parts of the date
            const day = inputDate.getDate();
            const month = inputDate.getMonth() + 1;
            const year = inputDate.getFullYear();

            //replace the month
            format = format.replace("MM", month.toString().padStart(2, "0"));

            //replace the year
            if (format.indexOf("yyyy") > -1) {
                format = format.replace("yyyy", year.toString());
            } else if (format.indexOf("yy") > -1) {
                format = format.replace("yy", year.toString().substr(2, 2));
            }

            //replace the day
            format = format.replace("dd", day.toString().padStart(2, "0"));
            return format;
        }
        ;
        $(function () {
            let date = new Date();
            let dtToday = new Date();
            var dateEnd = addYears(dtToday, parseInt(document.getElementById("year").value));
            var str = date.toString();
            document.getElementById('txtDate').valueAsDate = new Date();
            $('#show-startDate').text(dateFormat(dtToday, 'dd/MM/yyyy'));
            $('#show-endDate').text(dateFormat(dateEnd, 'dd/MM/yyyy'));
            var month = dtToday.getMonth() + 1;
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();

            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();

            var maxDate = year + '-' + month + '-' + day;
            $('#txtDate').attr('min', maxDate);
        });
        $(function () {
            var year = document.getElementById("year");
            var date = document.getElementById("txtDate");
            var loaixe = document.getElementById("loaixe");
            var baohiem2 = document.getElementById("baohiem2");
            var ownerName = document.getElementById("ownerName");
            var plate = document.getElementById("plate");
            var dateTime = new Date(document.getElementById("txtDate").valueAsDate);
            var totalPrice = 0;
            var typePrice = 0;
            var feePrice = 0;
            var yearTime;

            document.getElementById('form').addEventListener('input', function () {
                yearTime = parseInt(year.value);
                dateTime = new Date(date.value);
                var endDate = addYears(dateTime, yearTime);
                var kieuxe = parseInt(loaixe.value);
                if (kieuxe === 1) {
                    typePrice = 60500;
                } else if (kieuxe === 2) {
                    typePrice = 66000;
                } else if (kieuxe === 3) {
                    typePrice = 319000;
                } else if (kieuxe === 4) {
                    typePrice = 60500;
                } else if (kieuxe === 5) {
                    typePrice = 319000;
                }
                if (document.getElementById('baohiem2').checked) {
                    feePrice = 20000;
                } else{
                    feePrice = 0;
                }
                switch (yearTime) {
                    case 1:
                        typePrice = typePrice * 1;
                        if (feePrice !== null)
                            feePrice = feePrice * 1;
                        break;
                    case 2:
                        typePrice = typePrice * 2;
                        if (feePrice !== null)
                            feePrice = feePrice * 2;
                        break;
                    case 3:
                        typePrice = typePrice * 3;
                        if (feePrice !== null)
                            feePrice = feePrice * 3;
                        break;
                }
                
                totalPrice = feePrice + typePrice;
                $('#show-ownerName').text(ownerName.value);
                $('#show-plate').text(plate.value);
                $('#show-startDate').text(dateFormat(dateTime, 'dd/MM/yyyy'));
                $('#show-endDate').text(dateFormat(endDate, 'dd/MM/yyyy'));
                $('#show-fee1').text(typePrice.toLocaleString('it-IT', {style: 'currency', currency: 'VND'}));
                $('#show-fee2').text(feePrice.toLocaleString('it-IT', {style: 'currency', currency: 'VND'}));
                $('#total').text(totalPrice.toLocaleString('it-IT', {style: 'currency', currency: 'VND'}));
            });

        });
    </script>
</html>