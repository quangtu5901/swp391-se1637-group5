<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="zxx" class="no-js">

    <head>
        <base href="${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
        <link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
        <style>
            .paginationPage a{
                color: black;
                background: #e8f0f2;
            }
            .product {
                position: relative;
                margin: 15px 0px;
                -webkit-box-shadow: 0px 0px 0px 0px #E4E7ED, 0px 0px 0px 1px #E4E7ED;
                box-shadow: 0px 0px 0px 0px #E4E7ED, 0px 0px 0px 1px #E4E7ED;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .product:hover {
                -webkit-box-shadow: 0px 0px 6px 0px #E4E7ED, 0px 0px 0px 2px #ffba00;
                box-shadow: 0px 0px 6px 0px #E4E7ED, 0px 0px 0px 2px #ffba00;
            }

            .product .product-img {
                position: relative;
            }

            .product .product-img>img {
                width: 100%;
            }

            .product .product-img .product-label {
                position: absolute;
                top: 15px;
                right: 15px;
            }

            .product .product-img .product-label>span {
                border: 2px solid;
                padding: 2px 10px;
                font-size: 12px;
            }

            .product .product-img .product-label>span.sale {
                background-color: #FFF;
                border-color: #D10024;
                color: #D10024;
            }

            .product .product-img .product-label>span.new {
                background-color: #D10024;
                border-color: #D10024;
                color: #FFF;
            }

            .product .product-body {
                position: relative;
                padding: 15px;
                background-color: #FFF;
                text-align: center;
                z-index: 20;
            }

            .product .product-body .product-category {
                text-transform: uppercase;
                font-size: 12px;
                color: #8D99AE;
            }

            .product .product-body .product-name {
                text-transform: uppercase;
                font-size: 14px;
            }

            .product .product-body .product-name>a {
                font-weight: 100;
                color: black;
            }

            .product .product-body .product-name>a:hover, .product .product-body .product-name>a:focus {
                color: #ffba00;
            }

            .product .product-body .product-price {
                color: #ffba00;
                font-size: 18px;
            }

            .product .product-body .product-price .product-old-price {
                font-size: 70%;
                font-weight: 400;
                color: #8D99AE;
            }

            .product .product-body .product-rating {
                position: relative;
                margin: 15px 0px 10px;
                height: 20px;
            }

            .product .product-body .product-rating>i {
                position: relative;
                width: 14px;
                margin-right: -4px;
                background: #FFF;
                color: #E4E7ED;
                z-index: 10;
            }

            .product .product-body .product-rating>i.fa-star {
                color: #ef233c;
            }

            .product .product-body .product-rating:after {
                content: "";
                position: absolute;
                top: 50%;
                left: 0;
                right: 0;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                height: 1px;
                background-color: #E4E7ED;
            }

            .product .product-body .product-btns>button {
                position: relative;
                width: 40px;
                height: 40px;
                line-height: 40px;
                background: transparent;
                border: none;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .product .product-body .product-btns>button:hover {
                background-color: #E4E7ED;
                color: #ff6c00;
                border-radius: 50%;
            }

            .product .product-body .product-btns>button .tooltipp {
                position: absolute;
                bottom: 100%;
                left: 50%;
                -webkit-transform: translate(-50%, -15px);
                -ms-transform: translate(-50%, -15px);
                transform: translate(-50%, -15px);
                width: 150px;
                padding: 10px;
                font-size: 12px;
                line-height: 10px;
                background: #1e1f29;
                color: #FFF;
                text-transform: uppercase;
                z-index: 10;
                opacity: 0;
                visibility: hidden;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .product .product-body .product-btns>button:hover .tooltipp {
                opacity: 1;
                visibility: visible;
                -webkit-transform: translate(-50%, -5px);
                -ms-transform: translate(-50%, -5px);
                transform: translate(-50%, -5px);
            }

            .product .add-to-cart {
                position: absolute;
                left: 1px;
                right: 1px;
                bottom: 1px;
                padding: 15px;
                background: #1e1f29;
                text-align: center;
                -webkit-transform: translateY(0%);
                -ms-transform: translateY(0%);
                transform: translateY(0%);
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
                z-index: 2;
            }

            .product:hover .add-to-cart {
                -webkit-transform: translateY(100%);
                -ms-transform: translateY(100%);
                transform: translateY(100%);
            }

            .product .add-to-cart .add-to-cart-btn {
                position: relative;
                border: 2px solid transparent;
                height: 40px;
                padding: 0 30px;
                background-color: #ff6c00;
                color: #FFF;
                text-transform: uppercase;
                font-weight: 700;
                border-radius: 40px;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .product .add-to-cart .add-to-cart-btn>i {
                position: absolute;
                left: 0;
                top: 0;
                width: 40px;
                height: 40px;
                line-height: 38px;
                opacity: 0;
                visibility: hidden;
            }

            .product .add-to-cart .add-to-cart-btn:hover {
                background-color: #FFF;
                color: #ffba00;
                border-color: #ff6c00;
                padding: 0px 30px 0px 50px;
            }

            .product .add-to-cart .add-to-cart-btn:hover>i {
                opacity: 1;
                visibility: visible;
            }

        </style>
    </head>

    <body>

        <%@include file="../partial/header.jsp" %>

        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Product Details Page</h1>
                        <nav class="d-flex align-items-center">
                            <a href="${pageContext.request.contextPath}/home">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/productshop">Shop<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/productDetail/${detailP.deAccent()}">${detailP.getProductName()}</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Single Product Area =================-->
        <c:set value="${requestScope.detailP}" var="d"/>
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-6">
                    <div class="fotorama showimage" data-width="100%" data-autoplay="2000" data-thumbwidth="100%" data-nav="thumbs">
                        <img src="${d.image}" alt="productImage">
                        <c:forEach items="${requestScope.ListPLI}" var="pli">
                            <img src="${pli.image}" alt="pListImage">
                        </c:forEach>
                    </div>
                    <div > </div>   
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <form action="${pageContext.request.contextPath}/buy" method="POST">
                        <div class="s_product_text">
                            <h3 style="font-family: monospace">${d.productName}</h3>
                            <h2>${d.formatPrice(Double.parseDouble(d.price))}</h2>
                            <ul class="list">
                                <li><a class="active" href="#"><span>Category</span> : ${d.category.cateName}</a></li>
                                    <c:if test="${d.productStatus == 1}">
                                    <li><a href="#"><span>Status</span> : In Stock</a></li>
                                    </c:if>
                                    <c:if test="${d.productStatus != 1}">
                                    <li><a href="#"><span>Status</span> : Out Of Stock</a></li>
                                    </c:if>
                                <li><a href="#"><span>Total Quantity</span> : ${d.quantity} cái</a></li>
                            </ul>
                            <p>${d.description}</p>
                            <div class="product_count">
                                <label for="num">Quantity:</label>
                                <input type="text" name="num" id="sst" maxlength="12" value="1" title="Quantity:" class="input-text qty">
                                <button onclick="var result = document.getElementById('sst');
                                        var sst = result.value;
                                        if (!isNaN(sst) && sst < ${d.quantity})
                                            result.value++;
                                        return false;"
                                        class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                <button onclick="var result = document.getElementById('sst');
                                        var sst = result.value;
                                        if (!isNaN(sst) && sst > 1)
                                            result.value--;
                                        return false;"
                                        class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                            </div>
                            <div class="card_area d-flex align-items-center">
                                <button class="primary-btn" type="submit">Add to Cart</button>
                                <a class="icon_btn" href="#"><i class="lnr lnr lnr-diamond"></i></a>
                                <a class="icon_btn" href="#"><i class="lnr lnr lnr-heart"></i></a>
                            </div>
                            <input hidden="" name="id" value="${d.productId}">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->

    <!--================Product Description Area =================-->
    <section class="product_description_area">
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
                       aria-selected="false">Reviews</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <p>${d.description}</p>
                </div>
                <c:set value="${requestScope.totalRating}" var="totalrating"/>
                <c:set value="${requestScope.numberReview}" var="numberreview"/>
                <div class="tab-pane fade show active" id="review" role="tabpanel" aria-labelledby="review-tab">
                    <div class="row">
                        <div id="reviewData" class="col-lg-6">
                            <div class="row total_rate">
                                <div class="col-6">
                                    <div class="box_total">
                                        <h5>Overall</h5>
                                        <c:if test="${totalrating == null}">
                                            <h4>0.0</h4>
                                        </c:if>
                                        <c:if test="${totalrating != null}">
                                            <h4>${totalrating}</h4>
                                        </c:if>
                                        <c:if test="${numberreview != 0}">
                                            <h6>(0${numberreview} Reviews)</h6>
                                        </c:if>
                                        <c:if test="${numberreview == 0}">
                                            <h6>(0 Reviews)</h6>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="rating_list">
                                        <h3>Based on ${numberreview} Reviews</h3>
                                        <c:forEach begin="0" end="${listRating.size()-1}" step="1" var="i">
                                            <ul class="list">
                                                <li><a>${i+1} Star <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                                            class="fa fa-star"></i><i class="fa fa-star"></i> ${listRating.get(i)}</a></li>
                                            </ul>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                            <div class="review_list" style="min-height: 720px">
                                <c:forEach items="${requestScope.listPC}" var="pc">
                                    <div class="review_item">
                                        <div class="media">
                                            <div class="d-flex">
                                                <img src="${pc.user.avatar}" alt="userAvt">
                                            </div>
                                            <div class="media-body">
                                                <h4>${pc.user.username}</h4>
                                                <c:forEach begin="1" end="${pc.rating}">
                                                    <i class="fa fa-star"></i>
                                                </c:forEach>
                                                <c:forEach begin="${pc.rating}" end="4">
                                                    <i style="font-family: FontAwesome;" class="fa fa-star-o"></i>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <p>${pc.content}</p>
                                        <c:if test="${pc.listImage.size() != null}">
                                            <div class="review-comment">
                                                <div class="row-review">
                                                    <c:forEach items="${pc.listImage}" var="pc1">
                                                        <div class="column-review">
                                                            <img src="${pc1.image}" alt="listImg"  onclick="myFunction(this, '${pc1.id}');">
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                                <c:forEach items="${pc.listImage}" var="pc2">
                                                    <div class="container-review">
                                                        <span onclick="this.parentElement.style.display = 'none'" class="closebtn-review">&times;</span>
                                                        <img id="expandedImg${pc2.id}" style="width:80%" alt="commentimg">
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </c:if>
                                    </div>
                                </c:forEach>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                                <div class="paginationPage pagination">
                                    <c:if test="${tag > 1 }">
                                        <a onclick="loadDataPage('${tag - 1}', '${d.productId}')" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:forEach begin="1" end="${endPage}" var="i">
                                        <a onclick="loadDataPage('${i}', '${d.productId}')" ${(tag==i)?'class="active"':''}>${i}</a>
                                    </c:forEach>
                                    <c:if test="${tag < endPage}">
                                        <a onclick="loadDataPage('${tag + 1}', '${d.productId}')" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                        </c:if>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <form action="${pageContext.request.contextPath}/productDetail" method="post" enctype="multipart/form-data" id="my-form">
                                <input name="productId" type="hidden" value="${d.productId}">
                                <div class="review_box">
                                    <h4>Add a Review</h4>
                                    <span>Upload image: </span>
                                    <div class="multiple-uploader" id="multiple-uploader">
                                        <div class="mup-msg">
                                            <span class="mup-main-msg">Click to upload images.</span>
                                            <span class="mup-msg" id="max-upload-number">Upload up to 10 images</span>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 style="color: red;" class="modal-title" id="exampleModalLabel"><i style="font-size: 35px" class="fa fa-exclamation-triangle" aria-hidden="true"></i> Warning!!!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div id="notImage" class="modal-body">
                                                    <p>Not Image!!!</p>
                                                </div>
                                                <p style="color: red;padding-top: 0px" class="modal-body"><i style="font-size: 18px" class="fa fa-exclamation-circle" aria-hidden="true"></i> This is not Image!!!</p>
                                                <div class="modal-footer">
                                                    <button style="background-color: #ff6c00" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-rating">
                                        <span>Your Rating: </span>
                                        <div class="stars">
                                            <input id="star5" name="rating" required="" value="5" type="radio"><label
                                                for="star5"></label>
                                            <input id="star4" name="rating" required="" value="4" type="radio"><label
                                                for="star4"></label>
                                            <input id="star3" name="rating" required="" value="3" type="radio"><label
                                                for="star3"></label>
                                            <input id="star2" name="rating" required="" value="2" type="radio"><label
                                                for="star2"></label>
                                            <input id="star1" name="rating" required="" value="1" type="radio"><label
                                                for="star1"></label>
                                        </div>
                                        <p>Outstanding</p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea id="editor1" required="" name="review1" placeholder="Review"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="button" onclick="loadData()" class="primary-btn">Submit Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Product Description Area =================-->

    <!-- Start related-product Area -->
    <section class="related-product-area section_gap_bottom">
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">

                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h3 class="title">Related Products</h3>
                        </div>
                    </div>
                    <div style="display: flex">
                        <c:forEach items="${requestScope.listR}" var="r">
                            <!-- product -->
                            <div style="line-height: 42px" class="col-md-3 col-xs-6">
                                <div class="product">
                                    <div class="product-img">
                                        <img style="height: 294px;width: 294px" src="${r.image}" alt="productImg">
                                    </div>
                                    <div class="product-body">
                                        <p class="product-category">${r.category.cateName}</p>
                                        <h3 style="font-family: monospace;font-size: 15px;height: 36px" class="product-name"><a href="${pageContext.request.contextPath}/productDetail/${r.deAccent()}">${r.productName}</a></h3>
                                        <h4 class="product-price">${r.formatPrice(Double.parseDouble(r.price))}</h4>
                                        <div class="product-rating">
                                        </div>
                                        <div class="product-btns">
                                            <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">add to wishlist</span></button>
                                            <button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">add to compare</span></button>
                                            <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
                                        </div>
                                    </div>
                                    <div class="add-to-cart">
                                        <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /product -->
                        </c:forEach>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /Section -->
    </section>
    <!-- End related-product Area -->

    <%@include file="../partial/footer.jsp" %>
    <%@include file="../partial/script.jsp" %>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor 4
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
    </script>
    <script src="../public/js/multiple-uploader.js"></script>
    <script>
        let multipleUploader = new MultipleUploader('#multiple-uploader').init({
            maxUpload: 5, // maximum number of uploaded images
            maxSize: 5, // in size in mb
            filesInpName: 'images', // input name sent to backend
            formSelector: '#my-form' // form selector
        });
    </script>
    <script>
        function loadData() {
            var description = CKEDITOR.instances['editor1'].getData();
            var form = $('#my-form')[0];
            var data = new FormData(form);
            data.append("review", description);
            $('#my-form')[0].reset();
            CKEDITOR.instances['editor1'].setData('');
            $.ajax({
                url: "/SWP391-Group5/productDetail?action=insertimage",
                type: 'POST',
                enctype: 'multipart/form-data',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var reviewData = document.getElementById("reviewData");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function loadDataPage(index, id) {
            $.ajax({
                url: "/SWP391-Group5/productDetail?action=pagecomment",
                type: 'POST',
                data: {
                    indexPage: index,
                    productId: id
                },
                success: function (data) {
                    var reviewData = document.getElementById("reviewData");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
    </script>
</body>

</html>