<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <base href="${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
    </head>

    <body id="category">

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Shop Page</h1>
                        <nav class="d-flex align-items-center">
                            <a href="${pageContext.request.contextPath}/home">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/productshop">Shop<span></span></a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-5">
                    <div class="sidebar-categories">
                        <div class="head">Browse Categories</div>
                        <ul class="main-categories">
                            <input type="hidden" name="priceCate" id="catePriceMin"/>
                            <input type="hidden" name="priceCate" id="catePriceMax"/>
                            <li class="main-nav-list">
                                <div style="display: flex;justify-content: space-between;width: 100%">
                                    <a style="width: 100%" type="button" onclick="searchListCateIDAll('0')"><span class="lnr lnr-arrow-right"></span>All</a>
                                    <a type="button"><i class="fa fa-angle-down"></i></a>
                                </div>
                            </li> 
                            <c:forEach items="${requestScope.listC}" var="listC">
                                <li class="main-nav-list">
                                    <div style="display: flex;justify-content: space-between;width: 100%">
                                        <a type="button" onclick="searchListCateID('${listC.cateId}')"><span class="lnr lnr-arrow-right"></span>${listC.cateName}</a>
                                        <a data-toggle="collapse" href="#category${listC.cateId}" aria-expanded="false" aria-controls="category${listC.cateId}"><i class="fa fa-angle-down"></i></a>
                                    </div>
                                    <ul class="collapse" id="category${listC.cateId}" data-toggle="collapse" aria-expanded="false" aria-controls="category${listC.cateId}">
                                        <c:forEach items="${listC.subCategory}" var="sublistC">
                                            <li class="main-nav-list child"><a type="button" onclick="searchCateID('${sublistC.cateId}')">${sublistC.cateName}</a></li>
                                            </c:forEach>
                                    </ul>
                                </li>
                            </c:forEach>
                            <!--
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#meatFish" aria-expanded="false" aria-controls="meatFish"><span
                                                                    class="lnr lnr-arrow-right"></span>Meat and Fish<span class="number">(53)</span></a>
                                                            <ul class="collapse" id="meatFish" data-toggle="collapse" aria-expanded="false" aria-controls="meatFish">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#cooking" aria-expanded="false" aria-controls="cooking"><span
                                                                    class="lnr lnr-arrow-right"></span>Cooking<span class="number">(53)</span></a>
                                                            <ul class="collapse" id="cooking" data-toggle="collapse" aria-expanded="false" aria-controls="cooking">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#beverages" aria-expanded="false" aria-controls="beverages"><span
                                                                    class="lnr lnr-arrow-right"></span>Beverages<span class="number">(24)</span></a>
                                                            <ul class="collapse" id="beverages" data-toggle="collapse" aria-expanded="false" aria-controls="beverages">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#homeClean" aria-expanded="false" aria-controls="homeClean"><span
                                                                    class="lnr lnr-arrow-right"></span>Home and Cleaning<span class="number">(53)</span></a>
                                                            <ul class="collapse" id="homeClean" data-toggle="collapse" aria-expanded="false" aria-controls="homeClean">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a href="#">Pest Control<span class="number">(24)</span></a></li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#officeProduct" aria-expanded="false" aria-controls="officeProduct"><span
                                                                    class="lnr lnr-arrow-right"></span>Office Products<span class="number">(77)</span></a>
                                                            <ul class="collapse" id="officeProduct" data-toggle="collapse" aria-expanded="false" aria-controls="officeProduct">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#beauttyProduct" aria-expanded="false" aria-controls="beauttyProduct"><span
                                                                    class="lnr lnr-arrow-right"></span>Beauty Products<span class="number">(65)</span></a>
                                                            <ul class="collapse" id="beauttyProduct" data-toggle="collapse" aria-expanded="false" aria-controls="beauttyProduct">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#healthProduct" aria-expanded="false" aria-controls="healthProduct"><span
                                                                    class="lnr lnr-arrow-right"></span>Health Products<span class="number">(29)</span></a>
                                                            <ul class="collapse" id="healthProduct" data-toggle="collapse" aria-expanded="false" aria-controls="healthProduct">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a href="#">Pet Care<span class="number">(29)</span></a></li>
                                                        <li class="main-nav-list"><a data-toggle="collapse" href="#homeAppliance" aria-expanded="false" aria-controls="homeAppliance"><span
                                                                    class="lnr lnr-arrow-right"></span>Home Appliances<span class="number">(15)</span></a>
                                                            <ul class="collapse" id="homeAppliance" data-toggle="collapse" aria-expanded="false" aria-controls="homeAppliance">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="main-nav-list"><a class="border-bottom-0" data-toggle="collapse" href="#babyCare" aria-expanded="false"
                                                                                     aria-controls="babyCare"><span class="lnr lnr-arrow-right"></span>Baby Care<span class="number">(48)</span></a>
                                                            <ul class="collapse" id="babyCare" data-toggle="collapse" aria-expanded="false" aria-controls="babyCare">
                                                                <li class="main-nav-list child"><a href="#">Frozen Fish<span class="number">(13)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Dried Fish<span class="number">(09)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Fresh Fish<span class="number">(17)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#">Meat Alternatives<span class="number">(01)</span></a></li>
                                                                <li class="main-nav-list child"><a href="#" class="border-bottom-0">Meat<span class="number">(11)</span></a></li>
                                                            </ul>
                                                        </li>-->
                        </ul>
                    </div>
                    <div class="sidebar-filter mt-50">
                        <div class="top-filter-head">Product Filters</div>
                        <div class="common-filter">
                            <div class="head">Brands</div>
                            <c:forEach items="${requestScope.listB}" var="listb">
                                <ul>
                                    <li class="filter-list">
                                        <input class="pixel-radio" type="checkbox" id="brand" value="${listb.brandId}" onclick="searchByBrand()" name="brand">
                                        <label>${listb.brandName}</label>
                                    </li>
                                </ul>
                            </c:forEach>
                        </div>
                        <div class="common-filter">
                            <div class="head">Search</div>
                            <input type="hidden" name="action" value="searchProduct">
                            <div style="display: flex;justify-content: space-around">
                                <input oninput="searchProduct(this)" style="transform: translateX(30px)" type="text" name="txt" id="txtsearch" placeholder="search product">
                                <button style="background: #ffba00;border: 0px;color: white" type="button"><i class="fa fa-search"></i></span></button>
                            </div>
                        </div>
                        <div class="common-filter">
                            <div class="head">Price</div>
                            <div class="price-range-area">
                                <div id="price-range"></div>
                                <div class="value-wrapper d-flex">
                                    <div class="price">Price:</div>
                                    <div id="lower-value"></div>
                                    <div class="to">to</div>
                                    <div id="upper-value"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-7">
                    <!-- Start Filter Bar -->
                    <div class="filter-bar d-flex flex-wrap align-items-center">
                        <div class="sorting">
                            <select id="mySelect" name="mySelect" onchange="softByAll(this.value)" class="nice-select">
                                <option selected value="0">Please select option</option>
                                <option value="sort1">By Price asc</option>
                                <option value="sort2">By Price desc</option>
                                <option value="sort3">A-Z</option>
                                <option value="sort4">Z-A</option>
                            </select>
                        </div>
                    </div>
                    <!-- End Filter Bar -->
                    <!-- Start Best Seller -->
                    <div id="searchProduct">
                        <section style="min-height: 1615px" class="lattest-product-area pb-40 category-list">
                            <div class="row">
                                <!-- single product -->
                                <c:forEach items="${requestScope.listP}" var="listP">
                                    <div class="col-lg-4 col-md-6">
                                        <div class="single-product">
                                            <div class="img-product">
                                                <a href="${pageContext.request.contextPath}/productDetail/${listP.deAccent()}">
                                                    <img style="height: 100%" class="img-fluid" src="${listP.image}" alt="Error">
                                                </a>
                                            </div>
                                            <div class="product-details">
                                                <a href="${pageContext.request.contextPath}/productDetail/${listP.deAccent()}"><h6 style="font-family: monospace;height: 40px"">${listP.productName}</h6></a>
                                                <div class="price">
                                                    <h6>${listP.formatPrice(Double.parseDouble(listP.price))}</h6>
                                                    <!--<h6 class="l-through">$210.00</h6>-->
                                                </div>
                                                <div class="prd-bottom">

                                                    <a href="" class="social-info">
                                                        <span class="ti-bag"></span>
                                                        <p class="hover-text">add to bag</p>
                                                    </a>
                                                    <a href="" class="social-info">
                                                        <span class="lnr lnr-heart"></span>
                                                        <p class="hover-text">Wishlist</p>
                                                    </a>
                                                    <a href="" class="social-info">
                                                        <span class="lnr lnr-sync"></span>
                                                        <p class="hover-text">compare</p>
                                                    </a>
                                                    <a href="${pageContext.request.contextPath}/productDetail/${listP.deAccent()}" class="social-info">
                                                        <span class="lnr lnr-move"></span>
                                                        <p class="hover-text">view more</p>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </section>
                        <!-- End Best Seller -->
                        <!-- Start Filter Bar -->
                        <div class="filter-bar d-flex flex-wrap align-items-center">
                            <div class="sorting mr-auto"></div>
                            <div class="pagination" style="cursor: pointer">
                                <c:if test="${tag > 1 }">
                                    <a onclick="loadDataPageShop('${tag - 1}')" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                                    </c:if>
                                    <c:forEach begin="1" end="${endPage}" var="i">
                                    <a onclick="loadDataPageShop('${i}')" ${(tag==i)?'class="active"':''}>${i}</a>
                                </c:forEach>
                                <c:if test="${tag < endPage}">
                                    <a onclick="loadDataPageShop('${tag + 1}')" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                    </c:if>
                            </div>
                        </div>
                    </div>
                    <!-- End Filter Bar -->
                </div>
            </div>
        </div>

        <!-- Start related-product Area -->
        <section class="related-product-area section_gap">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Featured Products</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row">
                            <c:forEach items="${requestScope.listP1}" var="p1">
                                <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                                    <div class="single-related-product d-flex">
                                        <a href="${pageContext.request.contextPath}/productDetail/${p1.deAccent()}"><img style="width: 70px;height: 70px" src="${p1.image}" alt=""></a>
                                        <div style="margin-top: 0px" class="desc">
                                            <a href="${pageContext.request.contextPath}/productDetail/${p1.deAccent()}" style="font-size: 12px" class="title">${p1.productName}</a>
                                            <div class="price">
                                                <h6 style="font-size: 10px">${p1.formatPrice(Double.parseDouble(p1.price))}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ctg-right">
                            <a href="#" target="_blank">
                                <img class="img-fluid d-block mx-auto" src="img/category/c5.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End related-product Area -->

        <!-- Modal Quick Product View -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="container relative">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="product-quick-view">
                        <div class="row align-items-center">
                            <div class="col-lg-6">
                                <div class="quick-view-carousel">
                                    <div class="item" style="background: url(img/organic-food/q1.jpg);">

                                    </div>
                                    <div class="item" style="background: url(img/organic-food/q1.jpg);">

                                    </div>
                                    <div class="item" style="background: url(img/organic-food/q1.jpg);">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="quick-view-content">
                                    <div class="top">
                                        <h3 class="head">Mill Oil 1000W Heater, White</h3>
                                        <div class="price d-flex align-items-center"><span class="lnr lnr-tag"></span> <span class="ml-10">$149.99</span></div>
                                        <div class="category">Category: <span>Household</span></div>
                                        <div class="available">Availibility: <span>In Stock</span></div>
                                    </div>
                                    <div class="middle">
                                        <p class="content">Mill Oil is an innovative oil filled radiator with the most modern technology. If you are
                                            looking for something that can make your interior look awesome, and at the same time give you the pleasant
                                            warm feeling during the winter.</p>
                                        <a href="#" class="view-full">View full Details <span class="lnr lnr-arrow-right"></span></a>
                                    </div>
                                    <div class="bottom">
                                        <div class="color-picker d-flex align-items-center">Color:
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                        </div>
                                        <div class="quantity-container d-flex align-items-center mt-15">
                                            Quantity:
                                            <input type="text" class="quantity-amount ml-15" value="1" />
                                            <div class="arrow-btn d-inline-flex flex-column">
                                                <button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
                                                <button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
                                            </div>

                                        </div>
                                        <div class="d-flex mt-20">
                                            <a href="#" class="view-btn color-2"><span>Add to Cart</span></a>
                                            <a href="#" class="like-btn"><span class="lnr lnr-layers"></span></a>
                                            <a href="#" class="like-btn"><span class="lnr lnr-heart"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>
    </body>
    <script>
        function searchProduct(param) {
            var nonLinearSlider = document.getElementById('price-range');
            nonLinearSlider.noUiSlider.reset();
            var sortAll = document.getElementById("mySelect").value;
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            var text = param.value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    txt: text,
                    listcateIdAll: 0,
                    sort: sortAll,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function searchListCateID(listCate) {
            window.history.pushState(null, null, "/SWP391-Group5/productshop");
            $('#mySelect option').prop('selected', function () {
                return this.defaultSelected;
            });
            $("input:checkbox[name=brand]:checked").removeAttr('checked');
            document.getElementById("txtsearch").value = null;
            var nonLinearSlider = document.getElementById('price-range');
            nonLinearSlider.noUiSlider.reset();
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    listCateId: listCate,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {

                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function searchListCateIDAll(listCate) {
            window.history.pushState(null, null, "/SWP391-Group5/productshop");
            $("input:checkbox[name=brand]:checked").removeAttr('checked');
            $('#mySelect option').prop('selected', function () {
                return this.defaultSelected;
            });
            document.getElementById("txtsearch").value = null;
            var nonLinearSlider = document.getElementById('price-range');
            nonLinearSlider.noUiSlider.reset();
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    listcateIdAll: listCate,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function searchCateID(CateID) {
            window.history.pushState(null, null, "/SWP391-Group5/productshop");
            $('#mySelect option').prop('selected', function () {
                return this.defaultSelected;
            });
            $("input:checkbox[name=brand]:checked").removeAttr('checked');
            document.getElementById("txtsearch").value = null;
            var nonLinearSlider = document.getElementById('price-range');
            nonLinearSlider.noUiSlider.reset();
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            console.log(CateID);
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    cateId: CateID,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function softByAll(param) {
            window.history.pushState(null, null, "/SWP391-Group5/productshop");
            var arrayBrand = [];
            $("input:checkbox[name=brand]:checked").each(function () {
                arrayBrand.push($(this).val());
            });
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            var text = document.getElementById("txtsearch").value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    sort: param,
                    brandId: arrayBrand,
                    txt: text,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function loadDataPageShop(param) {
            window.history.pushState(null, null, "/SWP391-Group5/productshop");
            var arrayBrand = [];
            $("input:checkbox[name=brand]:checked").each(function () {
                arrayBrand.push($(this).val());
            });
            var sortAll = document.getElementById("mySelect").value;
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            var text = document.getElementById("txtsearch").value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    brandId: arrayBrand,
                    index: param,
                    txt: text,
                    sort: sortAll,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
        function searchByBrand() {
            var arrayBrand = [];
            $("input:checkbox[name=brand]:checked").each(function () {
                arrayBrand.push($(this).val());
            });
            var nonLinearSlider = document.getElementById('price-range');
            nonLinearSlider.noUiSlider.reset();
            var sortAll = document.getElementById("mySelect").value;
            var priceMin = document.getElementsByName("priceCate")[0].value;
            var priceMax = document.getElementsByName("priceCate")[1].value;
            $.ajax({
                url: "/SWP391-Group5/productshopdetail",
                type: 'GET',
                data: {
                    listcateIdAll: 0,
                    brandId: arrayBrand,
                    sort: sortAll,
                    minPrice: priceMin,
                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchProduct");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }
    </script>

</html>