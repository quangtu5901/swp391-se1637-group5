<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="vi" class="no-js" >

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
        <meta charset="utf-8"/>
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="assets/vendor/aos/aos.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
        <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">

    </head>

    <body>

        <%@include file="../partial/header.jsp" %>

        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h2>About us</h2>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="category.html">About us</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Blog Area =================-->

        <main id="main">

            <!-- ======= About Us Section ======= -->
            <!-- End About Us Section -->

            <!-- ======= About Section ======= -->
            <section class="about" data-aos="fade-up">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-6">
                            <img src="https://images-platform.99static.com/tkL3tNBXQgg6Syh_u0BcH1uhL0o=/500x500/top/smart/99designs-contests-attachments/7/7941/attachment_7941285" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0">
                            <h1>Introduction our company</h1>
                            <p>Welcome to our motobike accessories website! We are a team of passionate motorcycle enthusiasts who understand the importance of having the right gear and accessories to enhance your riding experience. Our mission is to provide high-quality and affordable accessories that cater to all your needs as a rider.


                            </p>
                            <p>Our journey started with a simple idea: to make riding safer and more comfortable for every rider. We realized that finding the right accessories could be a challenge, and that's why we decided to create a platform where riders could easily find everything they need in one place.
                            </p>
                            <p>
                                We are constantly searching for the latest and most innovative products to add to our collection, from protective gear such as helmets, jackets and gloves, to accessories that add style and functionality to your bike. We carefully select our products to ensure they meet our high standards of quality and safety.</p>

                        </div>
                    </div>
                    </br>
                                        </br>

                                                            </br>

                                                                                </br>

                    <div class="row">
                        <p>
                            At our motobike accessories website, we believe that every rider deserves the best, which is why we offer a wide range of products to suit all budgets. We pride ourselves on our excellent customer service and are always here to help you with any queries or concerns you may have.
                        </p>
                        <p>We are committed to making your shopping experience with us as smooth and enjoyable as possible. We offer secure payment options, fast shipping, and hassle-free returns. We also provide detailed product descriptions and specifications, as well as customer reviews, to help you make informed decisions.
                        <p class="fst-italic">
                            Thank you for choosing our motobike accessories website. We look forward to serving you and helping you ride with confidence and style.
                        </p>


                    </div>

                </div>
            </section>
            <!-- End About Section -->

            <!-- ======= Facts Section ======= -->
            <!--            <section class="facts section-bg" data-aos="fade-up">
                            <div class="container">
            
                                <div class="row counters">
            
                                    <div class="col-lg-3 col-6 text-center">
                                        <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
                                        <p>Clients</p>
                                    </div>
            
                                    <div class="col-lg-3 col-6 text-center">
                                        <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" class="purecounter"></span>
                                        <p>Projects</p>
                                    </div>
            
                                    <div class="col-lg-3 col-6 text-center">
                                        <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter"></span>
                                        <p>Hours Of Support</p>
                                    </div>
            
                                    <div class="col-lg-3 col-6 text-center">
                                        <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1" class="purecounter"></span>
                                        <p>Hard Workers</p>
                                    </div>
            
                                </div>
            
                            </div>
                        </section> End Facts Section 
            
                         ======= Our Skills Section ======= 
                        <section class="skills" data-aos="fade-up">
                            <div class="container">
            
                                <div class="section-title">
                                    <h2>Our Skills</h2>
                                    <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                                </div>
            
                                <div class="skills-content">
            
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                                            <span class="skill">HTML <i class="val">100%</i></span>
                                        </div>
                                    </div>
            
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                            <span class="skill">CSS <i class="val">90%</i></span>
                                        </div>
                                    </div>
            
                                    <div class="progress">
                                        <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                                            <span class="skill">JavaScript <i class="val">75%</i></span>
                                        </div>
                                    </div>
            
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
                                            <span class="skill">Photoshop <i class="val">55%</i></span>
                                        </div>
                                    </div>
            
                                </div>
            
                            </div>
                        </section> End Our Skills Section 
            
                         ======= Tetstimonials Section ======= 
                        <section class="testimonials" data-aos="fade-up">
                            <div class="container">
            
                                <div class="section-title">
                                    <h2>Tetstimonials</h2>
                                    <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                                </div>
            
                                <div class="testimonials-carousel swiper">
                                    <div class="swiper-wrapper">
                                        <div class="testimonial-item swiper-slide">
                                            <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                                            <h3>Saul Goodman</h3>
                                            <h4>Ceo &amp; Founder</h4>
                                            <p>
                                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                                Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                            </p>
                                        </div>
            
                                        <div class="testimonial-item swiper-slide">
                                            <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                                            <h3>Sara Wilsson</h3>
                                            <h4>Designer</h4>
                                            <p>
                                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                                Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                            </p>
                                        </div>
            
                                        <div class="testimonial-item swiper-slide">
                                            <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                                            <h3>Jena Karlis</h3>
                                            <h4>Store Owner</h4>
                                            <p>
                                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                                Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                            </p>
                                        </div>
            
                                        <div class="testimonial-item swiper-slide">
                                            <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                                            <h3>Matt Brandon</h3>
                                            <h4>Freelancer</h4>
                                            <p>
                                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                                Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                            </p>
                                        </div>
            
                                        <div class="testimonial-item swiper-slide">
                                            <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                                            <h3>John Larson</h3>
                                            <h4>Entrepreneur</h4>
                                            <p>
                                                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                                                Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                                                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="swiper-pagination"></div>
                                </div>
            
                            </div>-->
        </section><!-- End Ttstimonials Section -->

    </main>

    <section class="blog_area single-post-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 posts-list">




                </div>
            </div>
        </div>
    </section>
    <!--================Blog Area =================-->

    <%@include file="../partial/footer.jsp" %>
    <%@include file="../partial/script.jsp" %>
</body>
<script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</html>