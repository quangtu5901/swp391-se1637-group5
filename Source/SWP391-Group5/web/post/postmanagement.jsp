<!DOCTYPE html>
<%-- 
    Document   : blogmanagement
    Created on : Feb 22, 2023, 6:40:37 PM
    Author     : asus
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="zxx" class="no-js">

    <header>
        <base href="../${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
        <link rel="stylesheet" href="post/style.css">
        <script src="js/core/popper.min.js"></script>
        <script src="js/core/bootstrap.min.js"></script>
        <script src="js/plugins/perfect-scrollbar.min.js"></script>
        <script src="js/plugins/smooth-scrollbar.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.table').DataTable();
            });
        </script>
        <script>
            var win = navigator.platform.indexOf('Win') > -1;
            if (win && document.querySelector('#sidenav-scrollbar')) {
                var options = {
                    damping: '0.5'
                }
                Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
            }
        </script>
        <!-- Github buttons -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="js/argon-dashboard.min.js?v=2.0.4"></script>


        <link rel="apple-touch-icon" sizes="76x76" href="img/dashboard/apple-icon.png">
        <link rel="icon" type="image/png" href="img/dashboard/favicon.png">
        <title>
            Argon Dashboard 2 by Creative Tim
        </title>
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <!-- Nucleo Icons -->
        <link href="css/nucleo-icons.css" rel="stylesheet" />
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- Font Awesome Icons -->
        <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
        <link href="css/nucleo-svg.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link id="pagestyle" href="css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
        <link href="//cdn.datatables.net/1.13.2/css/jquery.dataTables.min.css" rel="stylesheet"/>
        <style>
            .edit_btn_management{
                font-size: var(--bs-badge-font-size);
                font-weight: var(--bs-badge-font-weight);
                text-transform: uppercase;
                padding:0px;
                border:0px;
                background-image: linear-gradient(310deg, #627594 0%, #a8b8d8 100%);
                color: white;
                width: 100%;
            }
        </style>

    </header>
    <style>
        table {

        }

        /*        tr, td, tbody, tfoot {
                display: block;
                }*/

        thead {
            display: none;
        }

        tr {
            padding-bottom: 10px;
        }

        td {
            padding: 10px 10px 0;
            text-align: center;
        }
        td:before {
            content: attr(data-title);
            color: #7a91aa;
            text-transform: uppercase;
            font-size: 1.4rem;
            padding-right: 10px;
            display: block;
        }

        table {
            width: 100%;
        }

        th {

            text-align: left;
            font-weight: 700;
        }

        thead th {
            background-color: #202932;
            color: #fff;
            border: 1px solid #202932;
        }

        tfoot th {
            display: block;
            padding: 10px;
            text-align: center;
            color: #b8c4d2;
        }

        .button {
            line-height: 1;
            display: inline-block;
            font-size: 1.2rem;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            padding: 8px;
            background-color: #4b908f;
        }
        le>


    </style>




    <body>

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Blog Page</h1>
                        <nav class="d-flex align-items-center">
                            <a href="${pageContext.request.contextPath}/home">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/post">Blog</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Blog Categorie Area =================-->
        <section class="blog_categorie_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/cat-post/cat-post-3.jpg" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Bao</h2>
                                    </a>
                                    <div class="border_line"></div>
                                    <p>Enjoy your social life together</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/cat-post/cat-post-2.jpg" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Motobike accessories</h2>
                                    </a>
                                    <div class="border_line"></div>
                                    <p>Be a part of politics</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/cat-post/cat-post-1.jpg" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Food</h2>
                                    </a>
                                    <div class="border_line"></div>
                                    <p>Let the food be finished</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
                <main>
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Post Title</th>

                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Post Title</th>

                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                            </tr>
                            <c:forEach items="${requestScope.list}" var="p">   
                                <tr>
                                    <td>
                                        <div class="d-flex px-2 py-1 ">                                         
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm ">${p.postTitle}</h6>
                                            </div>
                                        </div>
                                    </td>

                                    <td class="align-middle text-center text-sm">
                                        <c:choose>
                                            <c:when test="${p.postStatus == 0}">
                                                <span class="badge badge-sm bg-gradient-faded-primary" >Processed</span>                                                           
                                            </c:when>
                                            <c:when test="${p.postStatus == 1}">
                                                <span class="badge badge-sm bg-gradient-success" >Approved</span>                                                              
                                            </c:when>
                                            <c:otherwise>
                                                <span class="badge badge-sm bg-gradient-secondary">Declined</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="align-middle">
                                        <a class='button' href='${pageContext.request.contextPath}/updatepost?postId=${p.postId}''>
                                            Update
                                        </a>
                                        <a class='button' href='${pageContext.request.contextPath}/deletepost?postId=${p.postId}' style="background-color:red">
                                            Delete
                                        </a>
                                    </td>
                                </tr> 
                            </c:forEach>

                        </tbody>
                    </table>
                </main>
            </div>
        </div>


        <!--        <table>
                    <thead>
                        <tr>
                            <td>
                                Post Title
                            </td>
                            <td>
                                Post Status
                            </td>
        
                            <td>
                                Action
        
                            </td>
                        </tr>
                    </thead>
        
                    <tbody>
        <c:forEach var="p" items="${requestScope.list}">
            <tr>
                <td >
            ${p.postTitle}
        </td>
        <td>
            ${p.postStatus}
            <c:choose>
                <c:when test="${p.postStatus == 0}">
                    <form method="post" action="${pageContext.request.contextPath}/orderManagement?action=update&id=${item.id}">
                        <input type="submit" class="btn btn-success" value="Approve" name="submit" style="padding: 2px 15px; margin: 0;">
                        <input type="submit" class="btn btn-primary" value="Decline" name="submit" style="padding: 2px 15px; margin: 0;">
                    </form>                                                                
                </c:when>
                <c:when test="${post.status == 1}">
                    <span class="badge badge-sm bg-gradient-success" >Approved</span>                                                              
                </c:when>
                <c:otherwise>
                    <span class="badge badge-sm bg-gradient-secondary">Declined</span>
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <a class='button' href='${pageContext.request.contextPath}/updatepost?postId=${p.postId}''>
                Update
            </a>
            <a class='button' href='${pageContext.request.contextPath}/deletepost?postId=${p.postId}'>
                Delete
            </a>
        </td>
    </tr>
        </c:forEach>
    </tbody>
</table>-->




        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>   
    </body>
</html>
