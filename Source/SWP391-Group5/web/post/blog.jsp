<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="zxx" class="no-js">

    <header>
        <base href="${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>

    </header>

    <body>

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Blog Page</h1>
                        <nav class="d-flex align-items-center">
                            <a href="${pageContext.request.contextPath}/home">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="${pageContext.request.contextPath}/post">Blog</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Blog Categorie Area =================-->
        <section class="blog_categorie_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/phukien.jfif" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Phụ kiện</h2>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/phutung.jfif" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Phụ tùng xe máy</h2>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="categories_post">
                            <img src="img/blog/baohiem.jfif" alt="post">
                            <div class="categories_details">
                                <div class="categories_text">
                                    <a href="blog-details.html">
                                        <h2>Bảo hiểm xe máy</h2>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Categorie Area =================-->

        <!--================Blog Area =================-->


        <section class="blog_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="blog_left_sidebar" id="searchPost">
                            
                            <c:forEach  items="${requestScope.list}" var="post">
                                <article class="row blog_item">
                                    <div class="col-md-3">
                                        <div class="blog_info text-right">
                                            <div class="post_tag">
                                                <a href="#">Motorcycle insurance</a>
                                                <a href="#">Motobike accessories</a>



                                            </div>
                                            <ul class="blog_meta list">

                                                <li><a href="#">${post.dateCreated}<i class="lnr lnr-calendar-full"></i></a></li>
                                                <li><a href="#">${post.viewNumber}<i class="lnr lnr-eye"></i></a></li>
                                                        <jsp:useBean id="postModel" class="model.PostModel"/>
                                                <li><a href="#">${postModel.getAllPostComment($post.postId).size()}<i class="lnr lnr-bubble"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="blog_post">
                                            <img src="${post.imageBanner}" alt="imgBanner" style="width:100%;">
                                            <div class="blog_details">

                                                <a href="${pageContext.request.contextPath}/single-blog/${post.deAccent()}">


                                                    <h2>${post.postTitle} </h2>
                                                </a>
                                                <p>${post.seoContent}</p>

                                                <a href="${pageContext.request.contextPath}/single-blog/${post.deAccent()}" class="white_bg_btn">View More</a>



                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </c:forEach>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget search_widget">
                                <div class="input-group">
<!--                                    <form action="${pageContext.request.contextPath}/searchpost" method="post">-->
                                    <input type="hidden" name="action" value="searchProduct">
                                    <div style="display: flex;justify-content: space-around">
<!--                                        <input class="form-control" oninput="searchPost(this)" style="transform: translateX(30px)" type="text" name="txt" id="txtsearch" placeholder="search product">-->
                                        
                                        <input class="form-control" placeholder="Search Posts" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'" oninput="searchPost(this)" style="transform: translateX(30px)" type="text" name="txt" id="txtsearch" >
                                        <!--<input type="text" class="form-control" placeholder="Search Posts" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Posts'" name="search">-->

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="lnr lnr-magnifier"></i></button>
                                        </span>
                                    </div>
                                    <!--</form>-->
                                </div><!-- /input-group -->
                                <div class="br"></div>
                            </aside>
                            <!--                            <aside class="single_sidebar_widget author_widget">
                                                            <img class="author_img rounded-circle" src="img/blog/author.png" alt="">
                                                            <h4>Charlie Barber</h4>
                                                            <p>Senior blog writer</p>
                                                            <div class="social_icon">
                                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                                <a href="#"><i class="fa fa-github"></i></a>
                                                                <a href="#"><i class="fa fa-behance"></i></a>
                                                            </div>
                                                            <p>Boot camps have its supporters andit sdetractors. Some people do not understand why you
                                                                should have to spend money on boot camp when you can get. Boot camps have itssuppor
                                                                ters andits detractors.</p>
                                                            <div class="br"></div>
                                                        </aside>-->
                            <aside class="single_sidebar_widget popular_post_widget">
                                <h3 class="widget_title">Popular Posts</h3>
                                <c:forEach var="pp" items="${requestScope.listPopular}" >
                                    <div class="media post_item">
                                        <img src="${pp.imageBanner}" alt="post" style="width: 100px;height:60px ">
                                        <div class="media-body">
                                            <a href="${pageContext.request.contextPath}/single-blog?id=${pp.postId}">
                                                <h3>${pp.postTitle}</h3>
                                            </a>
                                            <p>${pp.dateCreated}</p>
                                        </div>
                                    </div>
                                </c:forEach>

                                <div class="br"></div>
                            </aside>
                            <aside class="single_sidebar_widget ads_widget">
                                <a href="#"><img class="img-fluid" src="https://th.bing.com/th/id/OIP.vPvFTY4Z1XHhyvhdX2QTWQHaNK?pid=ImgDet&rs=1" alt="fluid"></a>
                                <div class="br"></div>
                            </aside>
                            <!--                            <aside class="single_sidebar_widget post_category_widget">
                                                            <h4 class="widget_title">Post Catgories</h4>
                                                            <ul class="list cat-list">
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Technology</p>
                                                                        <p>37</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Lifestyle</p>
                                                                        <p>24</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Fashion</p>
                                                                        <p>59</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Art</p>
                                                                        <p>29</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Food</p>
                                                                        <p>15</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Architecture</p>
                                                                        <p>09</p>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="d-flex justify-content-between">
                                                                        <p>Adventure</p>
                                                                        <p>44</p>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <div class="br"></div>
                                                        </aside>-->
                            <aside class="single-sidebar-widget newsletter_widget">
                                <h4 class="widget_title">Newsletter</h4>
                                <p>
                                    Here, I focus on a range of items and features that we use in life without
                                    giving them a second thought.
                                </p>
                                <div class="form-group d-flex flex-row">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                                        </div>
                                        <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Enter email"
                                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email'">
                                    </div>
                                    <a href="#" class="bbtns">Subcribe</a>
                                </div>
                                <p class="text-bottom">You can unsubscribe at any time</p>
                                <div class="br"></div>
                            </aside>
                            <aside class="single-sidebar-widget tag_cloud_widget">
                                <h4 class="widget_title">Tag Clouds</h4>
                                <ul class="list">
                                    <li><a href="#">Bao hiem</a></li>
                                    <li><a href="#">Phu tung xe may </a></li>
                                    <li><a href="#">Lop xe may </a></li>
                                    <li><a href="#">Nhot xe may </a></li>

                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function searchPost(param) {
            
            var text = param.value;
            $.ajax({
                url: "/SWP391-Group5/searchPost",
                type: 'GET',
                data: {
                    txt: text,
//                    listcateIdAll: 0,
//                    sort: sortAll,
//                    minPrice: priceMin,
//                    maxPrice: priceMax
                },
                success: function (data) {
                    var reviewData = document.getElementById("searchPost");
                    reviewData.innerHTML = data;
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        </script>
        <!--================Blog Area =================-->

        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>
    </body>

</html>