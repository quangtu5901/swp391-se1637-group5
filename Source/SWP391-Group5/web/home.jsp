<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="zxx" class="no-js">

    <head>
        
        <base href="public/">
        <%@include file="partial/head.jsp" %>
    </head>

    <body>

        <%@include file="partial/header.jsp" %>

        <!-- start banner Area -->
        <section style="height: 700px" class="banner-area">
            <div class="container">
                <div style="transform: translateY(-100px)" class="row align-items-center justify-content-start">
                    <div class="col-lg-3">
                        <div class="sidebar-categories">
                            <div class="head">Browse Categories</div>
                            <ul style="padding: 0;" id="main-categories" class="main-categories">
                                <c:forEach items="${requestScope.listC}" var="c">
                                    <li class="main-nav-list dropdown side-dropdown open">
                                        <a style="position: relative; line-height: 70px" href="${pageContext.request.contextPath}/productshop/category/${c.deAccent()}" onclick="return catelist()" class="border-bottom-1">${c.cateName} <i class="fa fa-angle-right"></i></a>
                                            <c:if test="${c.subCategory.size() != 0}">
                                            <div class="custom-menu">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <ul class="list-links">
                                                            <li>
                                                                <h3 class="list-links-title">${c.cateName}</h3>
                                                            </li>
                                                            <c:if test="${c.subCategory.size() >= 5}">
                                                                <c:forEach items="${c.subCategory.subList(0,5)}" var="c1">
                                                                    <li><a href="${pageContext.request.contextPath}/productshop/subcategory/${c1.deAccent()}">${c1.cateName}</a></li>
                                                                    </c:forEach>
                                                                </c:if>
                                                                <c:if test="${c.subCategory.size() < 5}">
                                                                    <c:forEach items="${c.subCategory.subList(0,c.subCategory.size())}" var="c1">
                                                                    <li><a href="${pageContext.request.contextPath}/productshop/subcategory/${c1.deAccent()}">${c1.cateName}</a></li>
                                                                    </c:forEach>
                                                                </c:if>
                                                        </ul>
                                                    </div>
                                                    <c:if test="${c.subCategory.size() > 5}">
                                                        <div class="col-md-4">
                                                            <ul class="list-links">
                                                                <li>
                                                                    <h3 class="list-links-title">${c.cateName}</h3>
                                                                </li>
                                                                <c:if test="${c.subCategory.size() >= 10}">
                                                                    <c:forEach items="${c.subCategory.subList(5,10)}" var="c2">
                                                                        <li><a href="${pageContext.request.contextPath}/productshop/subcategory/${c2.deAccent()}">${c2.cateName}</a></li>
                                                                        </c:forEach>
                                                                    </c:if>
                                                                    <c:if test="${c.subCategory.size() < 10}">
                                                                        <c:forEach items="${c.subCategory.subList(5,c.subCategory.size())}" var="c2">
                                                                        <li><a href="${pageContext.request.contextPath}/productshop/subcategory/${c2.deAccent()}">${c2.cateName}</a></li>
                                                                        </c:forEach>
                                                                    </c:if>
                                                            </ul>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${c.subCategory.size() > 10}">
                                                        <div class="col-md-4">
                                                            <ul class="list-links">
                                                                <li>
                                                                    <h3 class="list-links-title">${c.cateName}</h3>
                                                                </li>
                                                                <c:forEach items="${c.subCategory.subList(10,c.subCategory.size())}" var="c3">
                                                                    <li><a href="${pageContext.request.contextPath}/productshop/subcategory/${c3.deAccent()}">${c3.cateName}</a></li>
                                                                    </c:forEach>
                                                            </ul>
                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </c:if>
                                    </li>
                                </c:forEach>

                                <!--                                <li class="main-nav-list dropdown side-dropdown open">
                                                                    <a style="position: relative; line-height: 70px" href="#" class="border-bottom-1">Item 2<i class="fa fa-angle-right"></i></a>
                                                                    <div class="custom-menu">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li class="main-nav-list dropdown side-dropdown open">
                                                                    <a style="position: relative; line-height: 70px" href="#" class="border-bottom-1">Item 3<i class="fa fa-angle-right"></i></a>
                                                                    <div class="custom-menu">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <ul class="list-links">
                                                                                    <li>
                                                                                        <h3 class="list-links-title">Categories</h3>
                                                                                    </li>
                                                                                    <li><a href="#">Women’s Clothing</a></li>
                                                                                    <li><a href="#">Men’s Clothing</a></li>
                                                                                    <li><a href="#">Phones & Accessories</a></li>
                                                                                    <li><a href="#">Jewelry & Watches</a></li>
                                                                                    <li><a href="#">Bags & Shoes</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>-->
                                <li class="main-nav-list dropdown side-dropdown open">
                                    <a style="position: relative;line-height: 70px" href="#" class="border-bottom-1">Item 4<i class="fa fa-angle-right"></i></a>
                                </li>
                                <li class="main-nav-list dropdown side-dropdown open">
                                    <a style="position: relative;line-height: 70px" href="#" class="border-bottom-1">Item 5<i class="fa fa-angle-right"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-9">
                        <div class="row fullscreen align-items-center justify-content-start">
                            <div class="col-lg-12">
                                <div class="active-banner-slider owl-carousel">
                                    <!-- single-slide -->
                                    <c:forEach items="${requestScope.listB}" var="item">
                                        <div class="row single-slide align-items-center d-flex">
                                            <div class="col-lg-5 col-md-6">
                                                <div class="banner-content">
                                                    <h2 style="font-family: 'Be Vietnam'; font-size: 4rem; font-weight: bold">${item.title}</h2>
                                                    <p>${item.description}</p>
                                                    <div class="add-bag d-flex align-items-center">
                                                        <a class="add-btn" href="${pageContext.request.contextPath}/productshop/category/${item.link}"><span class="lnr lnr-cross"></span></a>
                                                        <span class="add-text text-uppercase">Browse</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7">
                                                <div class="banner-img">
                                                    <img class="img-fluid" style="height: 380px; width: auto; float: right;" src="${item.image}" alt="">
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <!-- single-slide -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End banner Area -->
        <!-- Start exclusive deal Area -->
        <section class="exclusive-deal-area">
            <div class="container-fluid">
                <div class="row justify-content-center align-items-center ">
                    <div class="col-lg-12 no-padding" style="background: 0; text-align: center;padding-top: 295px;padding-bottom: 295px">
                        <video autoplay="true" muted loop style="position: absolute; top: 0; left: 0; width: 100%"> <source src="video/video_banner_1.mp4"> </video>
                        <div class="row clock_sec clockdiv overlay" id="clockdiv">
                            <div class="col-lg-12">
                                <h2 style="color: silver; font-family: 'Be Vietnam'">Thỏa Sức Sáng Tạo!</h2>
                                <p style="color: white">Tự tay lựa chọn thay thế, nâng cấp Motor của bạn!</p>
                            </div>
                            <div class="col-lg-12">
                                <h1 style="color: #ffffff; -webkit-text-stroke: 0.2px black;">
                                    MotorGear
                                </h1>
                            </div>
                        </div>
                        <a href="http://localhost:9999/SWP391-Group5/productshop" class="primary-btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- End exclusive deal Area -->
        <!-- start features Area -->
        <section class="features-area section_gap">
            <div class="container">
                <div class="row features-inner">
                    <!-- single features -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-features">
                            <div class="f-icon">
                                <img src="img/features/f-icon1.png" alt="">
                            </div>
                            <h6>Free Delivery</h6>
                            <p>Free Shipping on all order</p>
                        </div>
                    </div>
                    <!-- single features -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-features">
                            <div class="f-icon">
                                <img src="img/features/f-icon2.png" alt="">
                            </div>
                            <h6>Return Policy</h6>
                            <p>Free Shipping on all order</p>
                        </div>
                    </div>
                    <!-- single features -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-features">
                            <div class="f-icon">
                                <img src="img/features/f-icon3.png" alt="">
                            </div>
                            <h6>24/7 Support</h6>
                            <p>Free Shipping on all order</p>
                        </div>
                    </div>
                    <!-- single features -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-features">
                            <div class="f-icon">
                                <img src="img/features/f-icon4.png" alt="">
                            </div>
                            <h6>Secure Payment</h6>
                            <p>Free Shipping on all order</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end features Area -->

        <!-- Start category Area -->
        <section class="category-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-md-12">
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <div class="single-deal">
                                    <div class="overlay"></div>
                                    <img class="img-fluid w-100" src="${A1}" alt="">
                                    <a href="${A1}" class="img-pop-up" target="_blank">
                                        <div class="deal-details">
                                            <h6 class="deal-title">MotorGear Shop</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="single-deal">
                                    <div class="overlay"></div>
                                    <img class="img-fluid w-100" src="${A2}" alt="">
                                    <a href="${A2}" class="img-pop-up" target="_blank">
                                        <div class="deal-details">
                                            <h6 class="deal-title">MotorGear Shop</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="single-deal">
                                    <div class="overlay"></div>
                                    <img class="img-fluid w-100" src="${A3}" alt="">
                                    <a href="${A3}" class="img-pop-up" target="_blank">
                                        <div class="deal-details">
                                            <h6 class="deal-title">MotorGear Shop</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8">
                                <div class="single-deal">
                                    <div class="overlay"></div>
                                    <img class="img-fluid w-100" src="${A4}" alt="">
                                    <a href="${A4}" class="img-pop-up" target="_blank">
                                        <div class="deal-details">
                                            <h6 class="deal-title">MotorGear Shop</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-deal">
                            <div class="overlay"></div>
                            <img class="img-fluid w-100" src="${A5}" alt="">
                            <a href="${A5}" class="img-pop-up" target="_blank">
                                <div class="deal-details">
                                    <h6 class="deal-title">MotorGear Shop</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End category Area -->

        <!-- start product Area -->
        <section class="owl-carousel active-product-area section_gap">
            <!-- single product slide -->
            <c:forEach items="${requestScope.listP}" var="p">
                <div class="single-product-slider">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-6 text-center">
                                <div class="section-title">
                                    <h2 style="font-family: 'Be Vietnam'">${p.cateName}</h2>
                                    <p>Cung cấp những sản phẩm chất lượng, giá cả phải chăng và luôn đáp ứng được nhu cầu của khách hàng.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <c:forEach items="${p.listProduct}" var="p1">
                                <!-- single product -->
                                <div class="col-lg-3 col-md-6">
                                    <div class="single-product">
                                        <div class="img-product">
                                            <a href="${pageContext.request.contextPath}/productDetail/${p1.deAccent()}">
                                                <img style="height: 100%" class="img-fluid" src="${p1.image}" alt="Error">
                                            </a>
                                        </div>
                                        <div class="product-details">
                                            <a href="${pageContext.request.contextPath}/productDetail/${p1.deAccent()}"><h6 style="font-family: monospace;height: 40px"">${p1.productName}</h6></a>
                                            <div class="price">
                                                <h6>${p1.formatPrice(Double.parseDouble(p1.price))}</h6>
                                                <!--<h6 class="l-through">$210.00</h6>-->
                                            </div>
                                            <div class="prd-bottom">

                                                <a href="" class="social-info">
                                                    <span class="ti-bag"></span>
                                                    <p class="hover-text">add to bag</p>
                                                </a>
                                                <a href="" class="social-info">
                                                    <span class="lnr lnr-heart"></span>
                                                    <p class="hover-text">Wishlist</p>
                                                </a>
                                                <a href="" class="social-info">
                                                    <span class="lnr lnr-sync"></span>
                                                    <p class="hover-text">compare</p>
                                                </a>
                                                <a href="${pageContext.request.contextPath}/productDetail/${p1.deAccent()}" class="social-info">
                                                    <span class="lnr lnr-move"></span>
                                                    <p class="hover-text">view more</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                            <!-- single product -->
                        </div>
                    </div>
                </div>
            </c:forEach>
            <!-- single product slide -->
        </section>
        <!-- end product Area -->



        <!-- Start brand Area -->
        <section class="brand-area section_gap">
            <div class="container">
                <div class="row">
                    <c:forEach items="${requestScope.listB1}" var="b1">
                        <a class="col single-img">
                            <img style="height: 100px;width: 100px" class="img-fluid d-block mx-auto" src="${b1.image}" alt="">
                        </a>
                    </c:forEach>
                    <!--                    <a class="col single-img" href="#">
                                            <img class="img-fluid d-block mx-auto" src="img/brand/2.png" alt="">
                                        </a>
                                        <a class="col single-img" href="#">
                                            <img class="img-fluid d-block mx-auto" src="img/brand/3.png" alt="">
                                        </a>
                                        <a class="col single-img" href="#">
                                            <img class="img-fluid d-block mx-auto" src="img/brand/4.png" alt="">
                                        </a>
                                        <a class="col single-img" href="#">
                                            <img class="img-fluid d-block mx-auto" src="img/brand/5.png" alt="">
                                        </a>-->
                </div>
            </div>
        </section>
        <!-- End brand Area -->

        <!-- Start related-product Area -->
        <section class="related-product-area section_gap_bottom">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h2>Featured Products</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row">
                            <c:forEach items="${requestScope.listP1}" var="p2">
                                <div class="col-lg-4 col-md-4 col-sm-6 mb-20">
                                    <div class="single-related-product d-flex">
                                        <a href="${pageContext.request.contextPath}/productDetail/${p2.deAccent()}"><img style="width: 70px;height: 70px" src="${p2.image}" alt=""></a>
                                        <div style="margin-top: 0px" class="desc">
                                            <a href="${pageContext.request.contextPath}/productDetail/${p2.deAccent()}" style="font-size: 12px" class="title">${p2.productName}</a>
                                            <div class="price">
                                                <h6 style="font-size: 10px">${p2.formatPrice(Double.parseDouble(p2.price))}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ctg-right">
                            <a href="#" target="_blank">
                                <img class="img-fluid d-block mx-auto" src="img/product/bannerfotter.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End related-product Area -->


        <%@include file="partial/footer.jsp" %>
        <%@include file="partial/script.jsp" %>
    </body>

</html>