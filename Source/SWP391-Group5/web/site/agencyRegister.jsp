<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
    </head>

    <body>

        <%@include file="../partial/header.jsp" %>

        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Register Agency</h1>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="category.html">Contact</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <div class="align-items-center">
            <img src="img/agency.png" style="align-self: center"/>
        </div>
        <!--================Contact Area =================-->
        <hr class="forgotpass-hr">
        <section class="forgotpass-body login_box_area section_gap">
            <div class="forgotpass-responsive-form">
                <div class="container">
                    <div class="card text-black" style="border-radius: 25px;">
                        <div class="card-body p-md-5">
                            <!--        <section class="contact_area section_gap_bottom">
                                        <div class="container">      -->

                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 style="text-align: center">Register for become Agency</h1>
                                </div>
                                <div class="col-lg-12">
                                </div>
                                <br><br>
                                <div class="col-lg-12">
                                    <form class="row contact_form" action="${pageContext.request.contextPath}/agencyRegister" method="post" id="contactForm" novalidate="novalidate" enctype="multipart/form-data">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="Shopname" name="Shopname" placeholder="Enter Shop name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea class="form-control" name="message" id="message" rows="1" placeholder="Enter Message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <h6>Upload CV: </h6>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <input type="file" name="file" />
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <button type="submit" value="submit" class="primary-btn">Send Request</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================Contact Area =================-->

    <%@include file="../partial/footer.jsp" %>


    <!--================Contact Success and Error message Area =================-->
    <div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <h2>Thank you</h2>
                    <p>Your message is successfully sent...</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-close"></i>
                    </button>
                    <h2>Sorry !</h2>
                    <p> Something went wrong </p>
                </div>
            </div>
        </div>
    </div>
    <!--================End Contact Success and Error message Area =================-->


    <%@include file="../partial/script.jsp" %>
</body>

</html>