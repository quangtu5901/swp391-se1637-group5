<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
    </head>

    <body>

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Checkout</h1>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="single-product.html">Checkout</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Checkout Area =================-->
        <section class="checkout_area section_gap">
            <div class="container">
                <form class="row contact_form" action="${pageContext.request.contextPath}/checkout" method="post" novalidate="novalidate">
                    <div class="billing_details">
                        <div class="row">
                            <div class="col-lg-6 central-body">
                                <div class="order_box">
                                    <h2>Your Order</h2>
                                    <ul class="list">
                                        <li><a href="#">Product <span>Total</span></a></li>
                                            <c:set var="o" value="${sessionScope.cart}"/>
                                            <c:set var="tt" value="0"/>
                                            <c:forEach items="${o.items}" var="i">
                                                <%--<c:set var="name" value="${i.product.productName}"/>--%>
                                            <li><a href="${pageContext.request.contextPath}/productDetail?productId=${i.product.productId}"> ${fn:substring(i.product.productName, 0, 10)} <span class="middle">${i.quantity}</span> <span class="last">${i.price * i.quantity}</span></a></li>
                                                </c:forEach>  
                                    </ul>
                                    <ul class="list list_2">
                                        <li><a href="#">Subtotal <span>${o.getTotalMoney()}</span></a></li>
                                        <li><a href="#">Shipping <span>Thoa thuan</span></a></li>
                                        <li><a href="#">Total <span>${o.getTotalMoney()}</span></a></li>
                                    </ul>
                                    <div class="payment_item" hidden="">
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option5" name="selectorpayment" value="1">
                                            <label for="f-option5">Check payments</label>
                                            <div class="check"></div>
                                        </div>
                                        <p>Payment when shipping</p>
                                    </div>
                                    <div class="payment_item active" >
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option6" name="selectorpayment" value="0">
                                            <label for="f-option6">Banking </label>
                                            <img src="img/product/card.jpg" alt="">
                                            <div class="check"></div>
                                        </div>
                                        <p>You can have banking wallet</p>
                                    </div>
                                    <div class="creat_account">
                                        <input type="checkbox" id="f-option4" name="selector">
                                        <label for="f-option4">I’ve read and accept the </label>
                                        <a href="#">terms & conditions*</a>
                                    </div>
                                    <button style="width: 100%" type="submit" value="submit" class="primary-btn">Proceed to Payment</button>
                                </div>
                            </div>
                            <div class="col-lg-6 central-body">
                                <div>
                                    <img src="https://img.vietqr.io/image/BIDV-21710000605431-compact2.jpg?amount=${o.getTotalMoney()}}\&addInfo=SHOP2BANH%20${paymentBanking}%20${account.username}\&accountName=CHU%20HUU%20LONG%20" alt="alt" width="700" height="700"/>                                 
                                </div>
                            </div>   s
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!--================End Checkout Area =================-->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.1/axios.min.js" integrity="sha512-bPh3uwgU5qEMipS/VOmRqynnMXGGSRv+72H/N260MQeXZIK4PG48401Bsby9Nq5P5fz7hy5UGNmC/W1Z51h2GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>-->
        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>

    </body>

</html>
