<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <%@include file="../partial/head.jsp" %>
    </head>

    <body>
        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Shopping Cart</h1>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="category.html">Cart</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Cart Area =================-->
        <section class="cart_area">
            <div class="container">
                <div class="cart_inner">
                    <div class="table-responsive">
                        <c:set var="o" value="${sessionScope.cart}"/>
                        <c:set var="tt" value="0"/>
                        <c:if test="${o.items.size() == 0 || o == null}">
                            <div class="container-fluid  mt-100">
                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="card">
                                            <div class="card-body cart" style="padding: 100px">
                                                <div class="col-sm-12 empty-cart-cls text-center">
                                                    <img src="https://i.imgur.com/dCdflKN.png" width="130" height="130" class="img-fluid mb-4 mr-3">
                                                    <h3><strong>Your Cart is Empty</strong></h3>
                                                    <h4>Add something to make me happy :)</h4>
                                                    <a href="${pageContext.request.contextPath}/home" class="btn btn-primary cart-btn-transform m-3" data-abc="true">continue shopping</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${o.items.size() != 0 && o != null}">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Product</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <c:forEach items="${o.items}" var="i">
                                        <tr>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img src="${i.product.image}" alt="" width="80px" height="80px">
                                                    </div>
                                                    <div class="media-body">
                                                        <p>${i.product.productName}</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <h5>${i.product.price}</h5>
                                            </td>
                                            <td>
                                                <div class="product_count">
                                                    <input type="text" name="num" id="sst" maxlength="12" value="${i.quantity}" title="Quantity:"
                                                           class="input-text qty">
                                                    <button class="increase items-count" type="button">
                                                        <a href="${pageContext.request.contextPath}/process?num=1&id=${i.product.productId}"><i class="lnr lnr-chevron-up"></i></a>
                                                    </button>
                                                    <button class="reduced items-count" type="button">
                                                        <a href="${pageContext.request.contextPath}/process?num=-1&id=${i.product.productId}"><i class="lnr lnr-chevron-down"></i></a>
                                                    </button>
                                                </div>
                                            </td>
                                            <td>
                                                <h5>${i.price * i.quantity}</h5>
                                            </td>
                                        </tr>
                                    </c:forEach>

                                    <tr>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <h5>Subtotal</h5>
                                        </td>
                                        <td>
                                            <h5>${o.getTotalMoney()}</h5>
                                        </td>
                                    </tr>
                                    <tr class="out_button_area">
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="checkout_btn_inner d-flex align-items-center">
                                                <a class="gray_btn" href="${pageContext.request.contextPath}/home">Continue Shopping</a>
                                                <a class="primary-btn" href="${pageContext.request.contextPath}/checkout">Proceed to checkout</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </c:if>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Cart Area =================-->

        <%@include file="../partial/footer.jsp" %>
        <%@include file="../partial/script.jsp" %>



    </body>

</html>