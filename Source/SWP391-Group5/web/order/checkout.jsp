<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <base href="../${pageContext.request.contextPath}/public/">
        <%--<%@include file="../partial/head.jsp" %>--%>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Karma Shop</title>
        <!--
                CSS
                ============================================= -->
        <link rel="stylesheet" href="css/linearicons.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/nouislider.min.css">
        <link rel="stylesheet" href="css/ion.rangeSlider.css" />
        <link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/fotorama.css">
    </head>

    <body>

        <%@include file="../partial/header.jsp" %>


        <!-- Start Banner Area -->
        <section class="banner-area organic-breadcrumb">
            <div class="container">
                <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                    <div class="col-first">
                        <h1>Checkout</h1>
                        <nav class="d-flex align-items-center">
                            <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                            <a href="single-product.html">Checkout</a>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->

        <!--================Checkout Area =================-->
        <section class="checkout_area section_gap">
            <div class="container">
                <form class="row contact_form" action="${pageContext.request.contextPath}/checkout" method="post" novalidate="novalidate">
                    <div class="billing_details">
                        <div class="row">
                            <div class="col-lg-8">
                                <h3>Billing Details</h3>
                                <div class="row">
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="fullName" name="name" placeholder="Full Name" value="${sessionScope.account.fullName}">
                                    </div>
                                    <div class="col-md-6 form-group p_star">
                                        <input type="text" class="form-control" id="number" name="phoneNumber" placeholder="Phone Number" value="${sessionScope.account.phoneNumber}" >
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="${sessionScope.account.email}" >
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <input type="email" class="form-control" id="old_add" name="old_add" value="${sessionScope.account.city}, ${sessionScope.account.district}, ${sessionScope.account.ward}, ${sessionScope.account.address}" readonly="">
                                    </div>

                                    <div class="col-md-12 form-group">
                                        <div class="creat_account">
                                            <h3>Shipping Details</h3>
                                            <input type="checkbox" id="f-option3" name="selectorAddress" value="1">
                                            <label for="f-option3">Ship to a different address?</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6 form-group p_star">
                                        <select style="padding-bottom: 0px; padding-top: 0px;line-height: normal" id="province" class="form-control" >
                                            <option value="">Select a city</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 form-group p_star">
                                        <select style="padding-bottom: 0px; padding-top: 0px;line-height: normal" id="district" class="form-control" >
                                            <option value="">Select a district</option>
                                        </select>
                                    </div>
                                    <div class="col-md-12 form-group p_star">
                                        <select style="padding-bottom: 0px; padding-top: 0px;line-height: normal" id="ward" class="form-control" >
                                            <option value="">Select a ward</option>
                                        </select>
                                    </div>
                                    <input hidden="" name="province1" id="province1">
                                    <input hidden="" name="district1" id="district1">
                                    <input hidden="" name="ward1" id="ward1">
                                    <div class="col-md-12 form-group p_star">
                                        <input type="text" class="form-control" id="add1" name="add_new" placeholder="New Address">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <textarea class="form-control" name="message" id="message" rows="1" placeholder="Order Notes"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="order_box">
                                    <h2>Your Order</h2>
                                    <ul class="list">
                                        <li><a href="#">Product <span>Total</span></a></li>
                                            <c:set var="o" value="${sessionScope.cart}"/>
                                            <c:set var="tt" value="0"/>
                                            <c:forEach items="${o.items}" var="i">
                                            <li><a href="${pageContext.request.contextPath}/productDetail?productId=${i.product.productId}"> ${fn:substring(i.product.productName, 0, 10)} <span class="middle">${i.quantity}</span> <span class="last">${i.price * i.quantity}</span></a></li>
                                                </c:forEach>  
                                    </ul>
                                    <ul class="list list_2">
                                        <li><a href="#">Subtotal <span>${o.getTotalMoney()}</span></a></li>
                                        <li><a href="#">Shipping <span>Thoa thuan</span></a></li>
                                        <li><a href="#">Total <span>${o.getTotalMoney()}</span></a></li>
                                    </ul>
                                    <div class="payment_item">
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option5" name="selectorpayment" value="1" checked="">
                                            <label for="f-option5">Check payments</label>
                                            <div class="check"></div>
                                        </div>
                                        <p>Payment when shipping</p>
                                    </div>
                                    <div class="payment_item active">
                                        <div class="radion_btn">
                                            <input type="radio" id="f-option6" name="selectorpayment" value="0">
                                            <label for="f-option6">Banking </label>
                                            <img src="img/product/card.jpg" alt="">
                                            <div class="check"></div>
                                        </div>
                                        <p>You can have banking wallet</p>
                                    </div>
                                    <div class="creat_account">
                                        <input type="checkbox" id="f-option4" name="selector">
                                        <label for="f-option4">I’ve read and accept the </label>
                                        <a href="#">terms & conditions*</a>
                                    </div>
                                    <button style="width: 100%" type="submit" value="submit" class="primary-btn">Proceed to Payment</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!--================End Checkout Area =================-->


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.26.1/axios.min.js" integrity="sha512-bPh3uwgU5qEMipS/VOmRqynnMXGGSRv+72H/N260MQeXZIK4PG48401Bsby9Nq5P5fz7hy5UGNmC/W1Z51h2GQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>-->
        <%@include file="../partial/footer.jsp" %>
        <%--<%@include file="../partial/script.jsp" %>--%>

        <script src="js/address.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/countdown.js"></script>
        <!--<script src="js/vendor/jquery-2.2.4.min.js"></script>-->

        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
    </body>

</html>
