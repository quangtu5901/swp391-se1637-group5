
create database SWP391_Group5
go
use SWP391_Group5
go
create table [Role] (
	roleID int identity(1,1),
	roleName nvarchar(50),
	Primary Key(roleID)
)

create table [Admin] (
	id int identity(1,1),
	fullname nvarchar(50),
	username nvarchar(50),
	password nvarchar(300),
	email nvarchar(50),
	phone nvarchar(20),
	avartar nvarchar(Max),
	status int,
	roleId int,
	primary key(id),
    FOREIGN KEY (roleID) REFERENCES Role(roleID)
)

create table [User] (
	id int identity(1,1),
	roleID int,
	username nvarchar(50),
	password varchar(50),
	email varchar(50),
	status int,
	phoneNumber varchar(50),
	address varchar(200),
	fullName nvarchar(50),
	city nvarchar(50),
	district nvarchar(50),
	ward nvarchar(50),
	avatar varchar(2000),
	PRIMARY KEY (id),
    FOREIGN KEY (roleID) REFERENCES Role(roleID)
);

create table [Brand] (
	brandId int identity(1,1),
	brandName nvarchar(100),
	image nvarchar(MAX),
	primary key(brandId)
);


create table [Category] (
	cateId int identity(1,1),
	cateName nvarchar(100),
	image nvarchar(1000),
	parentId int,
	cateStatus int,
	primary key(cateId),
);

create table [Product] (
	productId int identity(1,1),
	cateId int,
	brandId int,
	productName nvarchar(200),
	price decimal(10, 2),
	discount decimal(2, 2),
	description nvarchar(1000),
	image varchar(1000),
	quantity int,
	buyNumber bigint,
	userCreatedId int,
	userModifiedId int,
	dateCreated datetime,
	dateModified datetime,
	productStatus int,
	primary key(productId),
	FOREIGN KEY (brandId) REFERENCES Brand(brandId),
	FOREIGN KEY (cateId) REFERENCES Category(cateId),
); 


create table [Pro list image] (
	id int identity(1,1),
	image nvarchar(1000),
	productId int,
	primary key(id),
	FOREIGN KEY (productId) REFERENCES [Product](productId)
);



create table [Order] (
	orderID int identity(1,1),
	orderName nvarchar(50),
	userId int,
	dateCreated datetime,
	totalPrice decimal(10,2),
	phoneNumber nvarchar(20),
	status int,
	payment_status int,
	note nvarchar(1000),
	address nvarchar(1000),
	primary key(orderID),
	FOREIGN KEY (userId) REFERENCES [User](id)
);

create table [Oder Detail] (
	oderDetailId int identity(1,1),
	orderId int,
	productId int,
	price decimal(10,2),
	quantity int,
	primary key(oderDetailId),
	FOREIGN KEY (orderId) REFERENCES [Order](orderID),
	FOREIGN KEY (productId) REFERENCES [Product](productId)
);

create table [Post] (
	postId int identity(1,1),
	postTitle nvarchar(2000),
	userCreatedId int,
	dateCreated datetime,
	dateModified datetime,
	postContent nvarchar(MAX),
	seoContent nvarchar(MAX),
	imageBanner varchar(MAX),
	viewNumber bigint,
	postStatus int,
	primary key(postId),
	FOREIGN KEY (userCreatedId) REFERENCES [User](id),
);


create table [Post list image] (
	id int identity(1,1),
	image nvarchar(MAX),
	postId int,
	primary key(id),
	FOREIGN KEY (postId) REFERENCES [Post](postId)
);


create table [Post Comment] (
	commentId int identity(1,1),
	userId int,
	postId int,
	content ntext,
	datePosted datetime,
	primary key(commentId),
	FOREIGN KEY (userId) REFERENCES [User](id),
	FOREIGN KEY (postId) REFERENCES Post(postId),
);

create table [Product Comment] (
	commentId int identity(1,1),
	userId int,
	productId int,
	content ntext,
	rating int,
	primary key(commentId),
	FOREIGN KEY (userId) REFERENCES [User](id),
	FOREIGN KEY (productId) REFERENCES [Product](productId)
);

create table [Product comment List Image] (
	id int identity(1,1),
	image nvarchar(MAX),
	commentId int,
	FOREIGN KEY (commentId) REFERENCES [Product Comment](commentId)
)

create table Contact(
	id int identity(1,1) primary key,
	userID int foreign key references [User](id),
	content nvarchar(max),
	dateCreated date,
	subject nvarchar(1000),
)
create table ContactImage(
	id int identity(1,1) primary key,
	image nvarchar(MAX),
	idContact int foreign key references Contact(id)
)

create table [Banner] (
		id int identity(1,1) primary key,
		title nvarchar(200),
		description nvarchar(MAX),
		image nvarchar(max),
		link nvarchar(max)
)


create table [advertiser] (
	id int identity(1,1) primary key,
	image nvarchar(max),
	link nvarchar(max)
)


create table [Banner] (
		id int identity(1,1) primary key,
		title nvarchar(200),
		description nvarchar(MAX),
		image nvarchar(max),
		link nvarchar(max)
)


create table [advertiser] (
	id int identity(1,1) primary key,
	image nvarchar(max),
	link nvarchar(max)
)

create table [Insurance](
  id int identity(1,1) primary key,
  userId int,
  ownerName nvarchar(30),
  plateNumber nvarchar(30),
  insurYear int,
  dateFrom date,
  dateTo date,
  mandatoryInsur bit,
  oprionInsur bit,
  vehicleType nvarchar(100),
  totalPrice decimal(10, 0)
  FOREIGN KEY (userId) REFERENCES [User](id)
  )


